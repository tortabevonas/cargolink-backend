/**
 * Generates a GUID string.
 * @returns {String} The generated GUID.
 * @example af8a8416-6e18-a307-bd9c-f2c947bbb3aa
 * @author Slavik Meltser (slavik@meltser.info).
 * @link http://slavik.meltser.info/?p=142
 */
function randomUUID() {
    function _p8(s) {
        var p = (Math.random().toString(16)+"000000000").substr(2,8);
        return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}

function extend(destination, source) {   
          for (var property in source) {
            destination[property] = source[property];
          }
          return destination;
    };

function intInString(str) {
  var digits="";
  var chars=str.split("");
  var DIGITS="0123456789";
  for(var i=0;i<chars.length;i++) {
    var ch=chars[i];
    if(DIGITS.indexOf(ch)>-1) {
       digits+=ch;
    }
  }
  if(digits.length>0) {
    return parseInt(digits);
  }else{
    return -1; 
  }
}

function validString(str) {
  if(str==null || typeof(str) !="string") {
    return false;
  }
  return str.trim().length>0;
}

function ensureUUID(sheet, row, uuidColumn) {
  var lock=LockService.getScriptLock();
  lock.waitLock(5000);
  var uuidCell=sheet.getRange(uuidColumn+row);
  if(!validString(uuidCell.getValue())) {
    uuidCell.setValue(randomUUID());
  }
  lock.releaseLock();
}

function enforceAllUuids(sheet, row, lastRow, uuidColumn) {
  var uuids = [];
  for(var r=0;r<lastRow-row+1;r++) {
    uuids.push([randomUUID()]);
  }
  var uuidRange=sheet.getRange(uuidColumn+row+":"+uuidColumn+lastRow);
  uuidRange.setValues(uuids);
}

function UuidRangeSync(id, url, uuidColumn, acceptSheetFunc, minColumn, maxColumn, minRow, maxRow) {
  var rs=new RangeSync(
    id,
    function sync(sheet, row, lastRow, edited) {
      if(edited) {
        for(var r=row;r<=lastRow;r++) {
          ensureUUID(sheet, r, uuidColumn);
        }
      }else{
        enforceAllUuids(sheet, row, lastRow, uuidColumn);
      }
    },
    function send(editLocation) {
      var response=UrlFetchApp.fetch(
        url,
        {
          "method" : "post",
          "payload": {
            data: JSON.stringify(editLocation)
          }
        }
      );
      if(response.getResponseCode()!=200) {
        throw "HTTP hiba. Valaszkod: "+response.getResponseCode()+" Szoveg: "+response.getContentText();
      }
    },
    acceptSheetFunc,
    minColumn, maxColumn, minRow, maxRow
  );
  
  extend(this, rs);
}