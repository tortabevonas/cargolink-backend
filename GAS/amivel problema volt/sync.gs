function RangeSync(id, syncFunc, sendFunc, acceptSheetFunc, minColumn, maxColumn, minRow, maxRow) {
  this.minColumn=minColumn;
  this.maxColumn=maxColumn;
  this.minRow=minRow;
  this.maxRow=maxRow;
  this.acceptSheetFunc=acceptSheetFunc;
  this.syncFunc=syncFunc;
  this.sendFunc=sendFunc;
  this.id=id;
  
  this.maxRange=function(sheet) {
    var minColumn=this.minColumn>0?this.minColumn:1;
    var maxColumn=this.maxColumn>0?this.maxColumn:sheet.getMaxColumns();
    var minRow=this.minRow>0?this.minRow:1;
    var maxRow=this.maxRow>0?this.maxRow:sheet.getMaxRows();
    
    var numColumns = maxColumn - minColumn + 1;
    var numRows = maxRow - minRow + 1;
    
    return sheet.getRange(minRow, minColumn, numRows, numColumns);
  }
  
  this.rangeIntersection=function(sheet, range) {
    var possible=true;
    var minColumn=this.minColumn>0?this.minColumn:1;
    var maxColumn=this.maxColumn>0?this.maxColumn:range.getLastColumn();
    var minRow=this.minRow>0?this.minRow:1;
    var maxRow=this.maxRow>0?this.maxRow:range.getLastRow();
    
    possible = possible && (range.getColumn()>=minColumn || range.getLastColumn()>=minColumn);
    possible = possible && (range.getColumn()<=maxColumn || range.getLastColumn()<=maxColumn);
    possible = possible && (range.getRow()>=minRow || range.getLastRow()>=minRow);
    possible = possible && (range.getRow()<=maxRow || range.getLastRow()<=maxRow);

    if(possible) {
      var column = Math.max(minColumn, range.getColumn());
      var row = Math.max(minRow, range.getRow());
      var maxColumn = Math.min(maxColumn, range.getLastColumn());
      var maxRow = Math.min(maxRow, range.getLastRow());
      var numColumns = maxColumn - column + 1;
      var numRows = maxRow - row + 1;
      
      return sheet.getRange(row, column, numRows, numColumns);
    }else{
      return null;
    }
  }
  
  this.onEdit=function(event) {
    var self=this;
    var sheet=event.source.getActiveSheet();
    var range=event.range;
    if(this.acceptSheetFunc(sheet)) {
      var intersectRange=this.rangeIntersection(sheet, range);
      if(intersectRange) {
        
        var minColumn=this.minColumn>0?this.minColumn:1;
        var maxColumn=this.maxColumn>0?this.maxColumn:sheet.getMaxColums();
        
        this.syncFunc(sheet, intersectRange.getRow(), intersectRange.getLastRow(), true);
        
        var editLocation = {
          rangeId: self.id,
          spreadsheetId: sheet.getParent().getId(),
          worksheetId : sheet.getName(),
          row: intersectRange.getRow(),
          column: intersectRange.getColumn(),
          numRows: intersectRange.getLastRow() - intersectRange.getRow() + 1,
          numColumns: intersectRange.getLastColumn() - intersectRange.getColumn() + 1
        };
        this.sendFunc(editLocation);
      }
    }
  }
  
  this.sync = function() {
    var sheets=SpreadsheetApp.getActiveSpreadsheet().getSheets();
    for(var sheetIndex=0;sheetIndex<sheets.length;sheetIndex++) {
      var sheet=sheets[sheetIndex];
      if(this.acceptSheetFunc(sheet)) {
        var range=this.maxRange(sheet);
        this.syncFunc(sheet, range.getRow(), range.getLastRow(), false);
      }
    }
  }
}

function RangeSyncList() {
  this.rangeSyncs=[];
  
  this.add = function(rangeSync) {
    this.rangeSyncs.push(rangeSync);
  }
  
  this.onEdit=function(event) {
    for(var i=0;i<this.rangeSyncs.length;i++) {
      this.rangeSyncs[i].onEdit(event);
    }
  }
  
  this.sync=function() {
    for(var i=0;i<this.rangeSyncs.length;i++) {
      this.rangeSyncs[i].sync();
    }
  }
}