var rangeSyncList=new RangeSyncList();

rangeSyncList.add(new UuidRangeSync(
  "import",
  "http://cargolink.mikrotek.hu:8080/spreadsheet/edit",
  "BO",
  function(sheet) {
    return intInString(sheet.getName())>=201433;
  },
  1, 67, 5, 0));

function reportEdit(event) {
  rangeSyncList.onEdit(event);
}

function ensureUuids() {
  rangeSyncList.sync();
  SpreadsheetApp.getUi().alert("UUID-k hozzáadva a táblázathoz. A táblázat használatra kész.");
}