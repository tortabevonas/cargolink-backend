var rangeSyncList=new RangeSyncList();

rangeSyncList.add(new UuidRangeSync(
  "export",
  "http://176.63.121.164:8080/spreadsheet/edit",
  "BR",
  function(sheet) {
    return intInString(sheet.getName())>=201433;
  },
  1, 70, 5, 0));
rangeSyncList.add(new UuidRangeSync(
  "soforok",
  "http://176.63.121.164:8080/spreadsheet/edit",
  "G",
  function(sheet) {
    return sheet.getName()=="Kódok";
  },
  4, 7, 2, 0));

function reportEdit(event) {
  rangeSyncList.onEdit(event);
}

function ensureUuids() {
  rangeSyncList.sync();
  SpreadsheetApp.getUi().alert("UUID-k hozzáadva a táblázathoz. A táblázat használatra kész.");
}