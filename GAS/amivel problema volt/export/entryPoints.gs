var rangeSyncList=new RangeSyncList();

rangeSyncList.add(new UuidRangeSync(
  "export",
  "http://cargolink.mikrotek.hu:8080/spreadsheet/edit",
  "BZ",
  function(sheet) {
    return intInString(sheet.getName())>=0;
  },
  1, 78, 5, 0));
rangeSyncList.add(new UuidRangeSync(
  "sofor",
  "http://cargolink.mikrotek.hu:8080/spreadsheet/edit",
  "E",
  function(sheet) {
    return sheet.getName()=="Kódok";
  },
  3, 5, 3, 0));

function reportEdit(event) {
  rangeSyncList.onEdit(event);
}

function ensureUuids() {
  rangeSyncList.sync();
  SpreadsheetApp.getUi().alert("UUID-k hozzáadva a táblázathoz. A táblázat használatra kész.");
}