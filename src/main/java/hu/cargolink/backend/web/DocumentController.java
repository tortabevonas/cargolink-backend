package hu.cargolink.backend.web;

import hu.cargolink.backend.data.documents.*;
import hu.cargolink.backend.data.exports.ExportJob;
import hu.cargolink.backend.data.exports.ExportJobRepository;
import hu.cargolink.backend.data.imports.ImportJob;
import hu.cargolink.backend.data.imports.ImportJobRepository;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Set;

/**
 * Ez a kontroller biztosítja a dokumentumok webes megtekintőfelületének a működését.
 *
 * Created by Aron on 2014.08.17..
 */
@Controller
public class DocumentController implements LinkGenerator {
    @Autowired
    private ImportJobRepository importJobRepository;

    @Autowired
    private ExportJobRepository exportJobRepository;

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private PageRepository pageRepository;

    @Autowired
    private FileStore pageFileStore;

    @Value("${spreadsheet.serverHostInLinks}")
    private String serverHostInLinks;

    private PageFileStoreFileResolver pageFileResolver;

    @PostConstruct
    public void init() {
        pageFileResolver = new PageFileStoreFileResolver(pageFileStore);
    }

    @RequestMapping("/jobs/{jobId}/documents/")
    public String documentsForJob(@PathVariable String jobId, Model model) {
        Set<Document> documents = null;
        ExportJob exportJob = exportJobRepository.findOne(jobId);
        ImportJob importJob = importJobRepository.findOne(jobId);
        if (exportJob != null) {
            documents = exportJob.documents;//documentRepository.findByJobId(exportJob.id);
            model.addAttribute("job", exportJob);
        } else if (importJob != null) {
            documents = importJob.documents;//documentRepository.findByJobId(importJob.id);
            model.addAttribute("job", importJob);
        }

        if (documents != null && !documents.isEmpty()) {
            model.addAttribute("documents", documents);
        }
        return "documents";
    }

    @RequestMapping("/jobs/{jobId}/documents/{documentUUID}")
    public String document(@PathVariable String jobId, @PathVariable String documentUUID, Model model) {
        Document document = documentRepository.findOne(documentUUID);
        ExportJob exportJob = exportJobRepository.findOne(jobId);
        ImportJob importJob = importJobRepository.findOne(jobId);
        if (exportJob != null) {
            model.addAttribute("job", exportJob);
        } else if (importJob != null) {
            model.addAttribute("job", importJob);
        }

        if (document != null) {
            model.addAttribute("document", document);
        }
        return "document";
    }

    @RequestMapping("/jobs/{jobId}/documents/{documentUUID}/{pageUUID}")
    public void page(@PathVariable String documentUUID, @PathVariable String pageUUID, HttpServletRequest request, final HttpServletResponse response) throws IOException {
        Document document = documentRepository.findOne(documentUUID);
        if (document != null) {
            Page page = pageRepository.findOne(pageUUID);
            if (page != null) {
                response.setContentType("image/jpeg");
                File file = pageFileResolver.fileFor(page);
                FileInputStream fileInputStream = new FileInputStream(file);
                IOUtils.copy(fileInputStream, response.getOutputStream());
                fileInputStream.close();
                return;
            }
        }
        throw new PageNotFoundException("pageNotFound");
    }

    @ExceptionHandler(PageNotFoundException.class)
    public String pageNotFoundExceptionHandler(PageNotFoundException p, Model model) {
        model.addAttribute("message", p.getMessage());
        return "error";
    }


    private class PageNotFoundException extends RuntimeException {
        public PageNotFoundException(String text) {
            super(text);
        }
    }

    @Override
    public String linkToJobDocuments(String jobId) {
        return "http://" + serverHostInLinks + "/jobs/" + jobId + "/documents/";
        //return "http://" + "mikrotik.cargolink.hu:8080" + "/jobs/" + jobId + "/documents/";
    }
}
