package hu.cargolink.backend.web;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import hu.cargolink.backend.spreadsheets.sync.SyncRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

/**
 * Ez a kontroller fogadja a táblázatba épített szkripttől a szinkronizálási kéréseket amikor cellák értéke megváltozik.
 *
 * Created by Aron on 2014.08.19..
 */
@Controller
public class SpreadsheetSyncController {
    @Autowired
    @Qualifier("syncRequestChannel")
    MessageChannel syncRequestChannel;

    @RequestMapping(value = "/spreadsheet/edit", method = RequestMethod.POST)
    @ResponseBody
    public String onEdit(@RequestParam("data") String requestBody) throws IOException {
        System.out.println("Szinkronizálási kérés: HTTP kérés feldolgozása megkezdve.");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        final SyncRequest syncRequest = objectMapper.readValue(requestBody, SyncRequest.class);

        Message<SyncRequest> message = MessageBuilder.withPayload(syncRequest).build();
        syncRequestChannel.send(message);

        System.out.println(
                String.format(
                        "Szinkronizálási kérés: HTTP kérés fogadva, üzenet továbbküldve. Kérés: %s",
                        syncRequest.toString()
                ));

        return "ACK";
    }
}
