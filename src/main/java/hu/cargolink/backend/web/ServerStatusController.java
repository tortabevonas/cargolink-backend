package hu.cargolink.backend.web;

import hu.cargolink.backend.data.documents.importexport.DocumentExportService;
import hu.cargolink.backend.data.documents.importexport.DocumentImportService;
import hu.cargolink.backend.spreadsheets.PeriodicSpreadsheetImportService;
import hu.cargolink.backend.spreadsheets.SpreadsheetImportService;
import org.joda.time.Period;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Spring MVC kontroller a szerver teszelési-karbantartási és állapotkijelző oldalához.
 *
 * Created by Aron on 2014.08.17..
 */
@Controller
public class ServerStatusController {
    @Autowired
    private SpreadsheetImportService spreadsheetImportService;

    @Autowired
    private PeriodicSpreadsheetImportService periodicSpreadsheetImportService;

    @Autowired
    private DocumentImportService documentImportService;

    @Autowired
    private DocumentExportService documentExportService;

    @RequestMapping("/")
    public String status(Model model) {
        model.addAttribute("running", spreadsheetImportService.isRunning());
        if (spreadsheetImportService.getLastResult() != null) {
            if (spreadsheetImportService.getLastResult().error != null) {
                model.addAttribute("lastResult", spreadsheetImportService.getLastResult().error.toString());
            }

            PeriodFormatterBuilder periodFormatterBuilder = new PeriodFormatterBuilder();
            periodFormatterBuilder.appendMinutes().appendSuffix("perc").appendSeparator(" ").appendSeconds().appendSuffix("másodperc");
            PeriodFormatter formatter = periodFormatterBuilder.toFormatter();
            String formattedTime = formatter.print(new Period((int) spreadsheetImportService.getLastResult().time));
            model.addAttribute("importTime", formattedTime);
        }
        return "status";
    }

    @RequestMapping(value = "/dataImport", method = RequestMethod.POST)
    public void doDataImport(@RequestParam("full") boolean full, HttpServletResponse response) throws IOException {
        if (!spreadsheetImportService.isRunning()) {
            spreadsheetImportService.start(full);
            //periodicSpreadsheetImportService.skipWait();
        }
        response.sendRedirect("/");
    }

    @RequestMapping(value = "/importDocuments", method = RequestMethod.POST)
    public void importDocuments(HttpServletResponse response) throws IOException {
        if (!documentImportService.isRunning()) {
            documentImportService.start(new File("./documents_in.json"));
        }
        response.sendRedirect("/");
    }

    @RequestMapping(value = "/exportDocuments", method = RequestMethod.POST)
    public void exportDocuments(HttpServletResponse response) throws IOException {
        if (!documentExportService.isRunning()) {
            documentExportService.start(new File("./documents_out.json"));
        }
        response.sendRedirect("/");
    }
}
