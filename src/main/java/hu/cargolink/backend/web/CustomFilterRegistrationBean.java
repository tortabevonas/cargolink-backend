package hu.cargolink.backend.web;

import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.CharacterEncodingFilter;

import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.util.EnumSet;

/**
 * Spring konfiguráció osztály. A beágyazott webszerver karakterkódolását állítja UTF-8-ra egy szűrő hozzáadásával.
 */
@Component
public class CustomFilterRegistrationBean extends FilterRegistrationBean {
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        CharacterEncodingFilter filter = new CharacterEncodingFilter();
        filter.setEncoding("UTF-8");
        filter.setForceEncoding(true);
        servletContext.addFilter("encoding-filter", filter).addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), false, "/*");
    }
}