package hu.cargolink.backend.web;

/**
 * Az egyes munkák dokumentumainak webes megtekintő felületére mutató linkeket generál.
 *
 * Created by Aron on 2014.09.08..
 */
public interface LinkGenerator {
    public String linkToJobDocuments(String jobId);
}
