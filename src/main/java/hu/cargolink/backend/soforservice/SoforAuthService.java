package hu.cargolink.backend.soforservice;

import hu.cargolink.backend.data.authentication.AuthenticationToken;
import hu.cargolink.backend.data.authentication.AuthenticationTokenRepository;
import hu.cargolink.backend.data.sofor.Sofor;
import hu.cargolink.backend.data.sofor.SoforRepository;
import hu.cargolink.backend.soforservice.client.GeneralError;
import hu.cargolink.backend.soforservice.client.UnauthenticatedError;
import hu.cargolink.backend.soforservice.integration.SoforSessionStore;
import hu.cargolink.backend.soforservice.messaging.SoforSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.Header;
import org.springframework.integration.annotation.Payload;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

/**
 * A sofor azonositassal kapcsolatos teendoit intezo szolgaltatas.
 * Mindegyik metodus Spring Integration ServiceActivator-kent van mappelve,
 * ennek kovetkezteben Spring Integration uzenetekre reagalnak,
 * a visszateresi ertekuk pedig szinten uzenetekke konvertalodik, amelyeket aztan az atviteli protokoll szerializal.
 *
 * Created by Aron on 2014.08.23..
 */
@Component
public class SoforAuthService {
    @Autowired
    private SoforRepository soforRepository;

    @Autowired
    private AuthenticationTokenRepository authenticationTokenRepository;


    @ServiceActivator(inputChannel = "toSoforService.login", outputChannel = "toResponseTransformer", requiresReply = "true")
    public String login(
            @Header(SoforSessionStore.HEADER) SoforSession session,
            @Payload("name") String name,
            @Payload("password") String password
    ) {
        Sofor sofor = soforRepository.findByName(name);
        if (sofor != null && sofor.name.equals(name) && sofor.password.equals(password)) {
            AuthenticationToken newToken = new AuthenticationToken(sofor);
            authenticationTokenRepository.save(newToken);

            return newToken.getValue();
        } else {
            throw new GeneralError();
        }
    }

    @ServiceActivator(inputChannel = "toSoforService.auth", outputChannel = "toResponseTransformer", requiresReply = "true")
    public void authenticate(
            @Header(SoforSessionStore.HEADER) SoforSession session,
            @Payload String tokenValue
    ) {
        AuthenticationToken token = authenticationTokenRepository.findOne(tokenValue);
        if (token != null) {
            Sofor sofor = soforRepository.findOne(token.getSoforId());
            session.login(token, sofor);
        } else {
            throw new GeneralError();
        }
    }

    @ServiceActivator(inputChannel = "toSoforService.deauth", outputChannel = "toResponseTransformer", requiresReply = "true")
    public void deauthenticate(
            @Header(SoforSessionStore.HEADER) SoforSession session
    ) {
        if (session.isAuthenticated()) {
            session.logout();
        } else {
            throw new UnauthenticatedError();
        }
    }

    @ServiceActivator(inputChannel = "toSoforService.logout", outputChannel = "toResponseTransformer", requiresReply = "true")
    public void logout(
            @Header(SoforSessionStore.HEADER) SoforSession session
    ) {
        if (session.isAuthenticated()) {
            authenticationTokenRepository.delete(session.getAuthToken());
            session.logout();
        } else {
            throw new UnauthenticatedError();
        }
    }
}
