package hu.cargolink.backend.soforservice.integration;

import hu.cargolink.backend.soforservice.common.MessageCodes;
import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;
import hu.cargolink.backend.soforservice.transfer.general.EmptyFrameWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;

/**
 * Spring Integration uzenetatalakito
 *
 * Created by Aron on 2014.08.24..
 */
public class SoforResponseTransformer {
    @Autowired
    private RequestResponseMapping requestResponseMapping;

    public Object transform(Message message) {
        FrameWriter frameWriter = null;

        if (message.getHeaders().containsKey(SoforRequestTransformer.HEADER)) {
            short action = (Short) message.getHeaders().get(SoforRequestTransformer.HEADER);
            try {
                frameWriter = requestResponseMapping.forRequestAction(action).messageWriter(message.getPayload());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        if (frameWriter == null && message.getPayload() instanceof Short) {
            frameWriter = new EmptyFrameWriter((Short) message.getPayload());
        }

        if (frameWriter == null) {
            return new EmptyFrameWriter(MessageCodes.RESPONSE_SERVER_ERROR);
        }

        return frameWriter;
    }
}
