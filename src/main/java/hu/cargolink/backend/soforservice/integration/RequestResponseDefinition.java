package hu.cargolink.backend.soforservice.integration;

import hu.cargolink.backend.soforservice.transfer.core.FrameReader;
import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;

/**
 * Alosztalyai definialjak, hogy egy adott kodu uzenetet hogyan kell olvasni es hogyan kell ra valaszolni a sajat atviteli protokollban
 *
 * Created by Aron on 2014.08.24..
 */
public abstract class RequestResponseDefinition<RequestT, ResponseT> {
    protected abstract FrameReader<RequestT> messageReader();

    protected abstract FrameWriter messageWriter(ResponseT input) throws Exception;
}
