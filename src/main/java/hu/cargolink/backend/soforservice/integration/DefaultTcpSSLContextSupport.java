package hu.cargolink.backend.soforservice.integration;

import org.springframework.integration.ip.tcp.connection.TcpSSLContextSupport;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;

/**
 * Spring SSL socket konfiguracio a sajat binaris atviteli protokoll szamara
 *
 * Created by Aron on 2014.09.06..
 */
public class DefaultTcpSSLContextSupport implements TcpSSLContextSupport {
    @Override
    public SSLContext getSSLContext() throws GeneralSecurityException, IOException {
        SSLContext sslContext = SSLContext.getInstance("SSLv3");
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");

        KeyStore keyStore = KeyStore.getInstance("JKS");
        keyStore.load(getClass().getResourceAsStream("/keystore.jks"), "password".toCharArray());
        keyManagerFactory.init(keyStore, "password".toCharArray());

        sslContext.init(keyManagerFactory.getKeyManagers(), null, null);
        return sslContext;
    }
}