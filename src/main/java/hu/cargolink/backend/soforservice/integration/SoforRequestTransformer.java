package hu.cargolink.backend.soforservice.integration;

import hu.cargolink.backend.soforservice.messaging.SoforRequest;
import org.springframework.integration.transformer.Transformer;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

/**
 * Spring Integration uzenetatalakito
 *
 * Created by Aron on 2014.08.22..
 */
public class SoforRequestTransformer implements Transformer {
    public static final String HEADER = "request";

    @Override
    public Message<?> transform(Message<?> message) {
        SoforRequest soforRequest = (SoforRequest) message.getPayload();
        Object param = soforRequest.getParam();
        MessageBuilder messageBuilder = MessageBuilder.withPayload(param != null ? param : new Object());
        messageBuilder.copyHeaders(message.getHeaders());
        messageBuilder.setHeader(HEADER, soforRequest.getAction());
        return messageBuilder.build();
    }
}
