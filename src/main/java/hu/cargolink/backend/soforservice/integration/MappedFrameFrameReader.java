package hu.cargolink.backend.soforservice.integration;

import hu.cargolink.backend.soforservice.transfer.core.Frame;
import hu.cargolink.backend.soforservice.transfer.core.LimitedInputStream;
import hu.cargolink.backend.soforservice.transfer.core.FrameReader;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Egy adatfolyamrol olvas le folyamatosan Frame-eket, amelyek a kommunikacios protokoll epitoelemeit kepezik
 *
 * Created by Aron on 2014.08.24..
 */
public class MappedFrameFrameReader extends FrameReader<List<Frame>> {
    private RequestResponseMapping requestResponseMapping = null;

    @Override
    public List<Frame> read(InputStream inputStream, short a, int size) throws IOException {
        List<Frame> result = new ArrayList<Frame>();

        long bytesRead = 0;
        do {
            DataInputStream dis = new DataInputStream(inputStream);

            short action = dis.readShort();
            int length = dis.readInt();
            bytesRead += 2 + 4;

            FrameReader frameReader = null;
            RequestResponseDefinition requestResponseDefinition = requestResponseMapping.forRequestAction(action);
            if (requestResponseDefinition != null) {
                frameReader = requestResponseDefinition.messageReader();
            }

            Frame frame = new Frame();
            frame.action = action;

            LimitedInputStream lis = new LimitedInputStream(inputStream);
            lis.allow(length);
            if (frameReader != null) {
                frame.payload = frameReader.read(lis, action, length);
            }
            lis.skipRemaining();
            bytesRead += length;

            result.add(frame);
        } while (bytesRead < size);

        return result;
    }

    public void setRequestResponseMapping(RequestResponseMapping requestResponseMapping) {
        this.requestResponseMapping = requestResponseMapping;
    }

    public Frame readOne(InputStream inputStream) throws IOException {
        return read(inputStream, (short) 0, 0).get(0);
    }
}
