package hu.cargolink.backend.soforservice.integration;

import hu.cargolink.backend.data.documents.FileStore;
import hu.cargolink.backend.data.documents.PageFileStoreFileResolver;
import hu.cargolink.backend.soforservice.common.MessageCodes;
import hu.cargolink.backend.soforservice.messages.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * A RequestResponseMapping konkret implementacioja.
 * Az atviteli protokoll kiterjesztheto, itt lehet uj keres-valasz tipusokat hozzaadni.
 *
 * Created by Aron on 2014.08.24..
 */
@Component
public class SoforRequestResponseMapping implements RequestResponseMapping {
    @Autowired
    private FileStore pageFileStore;

    @Override
    public RequestResponseDefinition forRequestAction(short requestAction) {
        if (requestAction == MessageCodes.REQUEST_LOGIN) {
            return new LoginRequestResponseDefinition();
        } else if (requestAction == MessageCodes.REQUEST_AUTH) {
            return new AuthRequestResponseDefinition();
        } else if (requestAction == MessageCodes.REQUEST_DEAUTH) {
            return new DeauthRequestResponseDefinition();
        } else if (requestAction == MessageCodes.REQUEST_LOGOUT) {
            return new LogoutRequestResponseDefinition();
        } else if (requestAction == MessageCodes.REQUEST_UPDATES) {
            return new UpdateRequestResponseDefinition();
        } else if (requestAction == MessageCodes.REQUEST_IMPORT_LKW_FELVETTE) {
            return new ImportLkwFelvetteRequestResponseDefinition(new PageFileStoreFileResolver(pageFileStore));
        } else if (requestAction == MessageCodes.REQUEST_IMPORT_VAMKEZELES_BEFEJEZ) {
            return new ImportVamkezelesBefRequestResponseDefinition(new PageFileStoreFileResolver(pageFileStore));
        } else if (requestAction == MessageCodes.REQUEST_IMPORT_RAKTARBA_BEERKEZETT) {
            return new ImportRaktarbaBeerkezettRequestResponseDefinition();
        } else if (requestAction == MessageCodes.REQUEST_IMPORT_LERAKVA_CIMZETTNEL) {
            return new ImportLerakvaCimzettnelRequestResponseDefinition(new PageFileStoreFileResolver(pageFileStore));
        } else if (requestAction == MessageCodes.REQUEST_EXPORT_LKW_FELVETTE) {
            return new ExportLkwFelvetteRequestResponseDefinition(new PageFileStoreFileResolver(pageFileStore));
        } else if (requestAction == MessageCodes.REQUEST_EXPORT_RAKTARBA_BEERKEZETT) {
            return new ExportRaktarbaBeerkezettRequestResponseDefinition();
        } else if (requestAction == MessageCodes.REQUEST_EXPORT_FELRAKVA_POTKOCSIRA) {
            return new ExportFelrakvaPotkocsiraRequestResponseDefinition();
        } else if (requestAction == MessageCodes.REQUEST_EXPORT_LERAKVA_CIMZETTNEL) {
            return new ExportLerakvaCimzettnelRequestResponseDefinition(new PageFileStoreFileResolver(pageFileStore));
        } else if (requestAction == MessageCodes.REQUEST_GET_PAGE) {
            return new GetDocumentPageRequestResponseDefinition(new PageFileStoreFileResolver(pageFileStore));
        } else if (requestAction == MessageCodes.REQUEST_SET_POSITION_FOR_JOB) {
            return new SetRequestForJobRequestResponseDefinition();
        } else if (requestAction == MessageCodes.REQUEST_ADD_DOCUMENT) {
            return new AddDocumentRequestResponseDefinition(new PageFileStoreFileResolver(pageFileStore));
        }
        return null;
    }
}