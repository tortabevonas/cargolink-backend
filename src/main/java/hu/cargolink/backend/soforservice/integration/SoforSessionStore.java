package hu.cargolink.backend.soforservice.integration;

import hu.cargolink.backend.soforservice.messaging.SoforSession;
import org.springframework.context.ApplicationListener;
import org.springframework.integration.ip.IpHeaders;
import org.springframework.integration.ip.tcp.connection.TcpConnectionEvent;
import org.springframework.messaging.Message;

import java.util.HashMap;
import java.util.Map;

/**
 * A soforokkel fenntartott aktiv TCP kapcsolatokat tarolja.
 * Egyelore semilyen celt nem szolgal.
 *
 * Created by Aron on 2014.08.22..
 */
public class SoforSessionStore implements ApplicationListener<TcpConnectionEvent> {
    private Map<String, SoforSession> sessions = new HashMap<String, SoforSession>();

    public static final String HEADER = "soforSession";

    public Map<String, Object> applySession(Message<?> message) {
        String connectionId = (String) message.getHeaders().get(IpHeaders.CONNECTION_ID);

        SoforSession session = sessions.get(connectionId);
        if (session == null) {
            session = new SoforSession();
            sessions.put(connectionId, session);
        }

        Map<String, Object> map = new HashMap<String, Object>();
        map.put(HEADER, session);

        return map;
    }

    @Override
    public void onApplicationEvent(TcpConnectionEvent event) {
        // Valahogy észleljük majd a kapcsolat bontást és töröljük a session-t
    }
}
