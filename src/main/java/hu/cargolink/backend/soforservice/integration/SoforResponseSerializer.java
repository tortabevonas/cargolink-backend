package hu.cargolink.backend.soforservice.integration;

import hu.cargolink.backend.soforservice.transfer.general.EmptyFrameWriter;
import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;
import hu.cargolink.backend.soforservice.common.MessageCodes;
import org.springframework.core.serializer.Serializer;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Spring Integration uzenet szerializalo. Ez irja ra a socketre a szerver valaszat.
 *
 * Created by Aron on 2014.08.22..
 */
public class SoforResponseSerializer implements Serializer {
    @Override
    public void serialize(Object object, OutputStream outputStream) throws IOException {
        FrameWriter frameWriter = null;
        if (object instanceof FrameWriter) {
            frameWriter = (FrameWriter) object;
        } else if (object instanceof Short) {
            frameWriter = new EmptyFrameWriter((Short) object);
        } else {
            frameWriter = new EmptyFrameWriter(MessageCodes.RESPONSE_SERVER_ERROR);
        }
        frameWriter.performWrite(outputStream);
    }
}