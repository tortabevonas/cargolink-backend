package hu.cargolink.backend.soforservice.integration;

import hu.cargolink.backend.soforservice.common.MessageCodes;
import hu.cargolink.backend.soforservice.messaging.SoforRequest;
import hu.cargolink.backend.soforservice.transfer.core.Frame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.serializer.Deserializer;

import java.io.IOException;
import java.io.InputStream;

/**
 * Spring Integration deserializer a sofor uzeneteinek olvasasara
 *
 * Created by Aron on 2014.08.22..
 */
public class SoforRequestDeserializer implements Deserializer {
    @Autowired
    private RequestResponseMapping requestResponseMapping;

    private MappedFrameFrameReader frameMessageReader;

    public SoforRequestDeserializer() {
        frameMessageReader = new MappedFrameFrameReader();
    }

    @Override
    public Object deserialize(InputStream inputStream) {
        frameMessageReader.setRequestResponseMapping(requestResponseMapping);
        try {
            Frame frame = frameMessageReader.readOne(inputStream);
            return new SoforRequest(frame.action, frame.payload);
        } catch (IOException e) {
            // Ha hiba történik a lekérés olvasása közben, akkor küldünk egy "ismeretlen" lekérdezést
            return new SoforRequest(MessageCodes.REQUEST_UNKNOWN, null);
        }
    }
}
