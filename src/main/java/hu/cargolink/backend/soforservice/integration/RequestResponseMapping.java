package hu.cargolink.backend.soforservice.integration;

/**
 * Uzenetkodot RequestResponseDefinition-na old fel a sajat atviteli protokollban
 *
 * Created by Aron on 2014.08.24..
 */
public interface RequestResponseMapping {
    public RequestResponseDefinition forRequestAction(short requestAction);
}
