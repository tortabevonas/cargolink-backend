package hu.cargolink.backend.soforservice;

import com.google.gdata.util.ServiceException;
import hu.cargolink.backend.data.documents.*;
import hu.cargolink.backend.data.exports.ExportJob;
import hu.cargolink.backend.data.exports.ExportJobEntityMapping;
import hu.cargolink.backend.data.exports.ExportJobRepository;
import hu.cargolink.backend.data.exports.ExportJobSpreadsheetRepository;
import hu.cargolink.backend.data.imports.ImportJob;
import hu.cargolink.backend.data.imports.ImportJobEntityMapping;
import hu.cargolink.backend.data.imports.ImportJobRepository;
import hu.cargolink.backend.data.imports.ImportJobSpreadsheetRepository;
import hu.cargolink.backend.soforservice.client.GeneralError;
import hu.cargolink.backend.soforservice.client.UnauthenticatedError;
import hu.cargolink.backend.soforservice.integration.SoforSessionStore;
import hu.cargolink.backend.soforservice.messaging.SoforSession;
import hu.cargolink.backend.spreadsheets.sync.ExportSheetSyncRequestService;
import hu.cargolink.backend.spreadsheets.sync.ImportSheetSyncRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.Header;
import org.springframework.integration.annotation.Payload;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * A sofor dokumentumokkal kapcsolatos feladatait ellato szolgaltatas.
 * Lasd meg: SoforAuthService
 *
 * Created by Aron on 2014.09.03..
 */
@Component
public class SoforDocumentService {

    @Autowired
    private ImportJobRepository importJobRepository;

    @Autowired
    private ExportJobRepository exportJobRepository;

    @Autowired
    private ImportJobSpreadsheetRepository importJobSpreadsheetRepository;

    @Autowired
    private ExportJobSpreadsheetRepository exportJobSpreadsheetRepository;

    @Autowired
    private PageRepository pageRepository;

    @Autowired
    private FileStore pageFileStore;

    @Autowired
    private ImportSheetSyncRequestService importSheetSyncRequestService;

    @Autowired
    private ExportSheetSyncRequestService exportSheetSyncRequestService;

    private PageFileStoreFileResolver pageFileResolver;

    @PostConstruct
    public void init() {
        pageFileResolver = new PageFileStoreFileResolver(pageFileStore);
    }


    @ServiceActivator(inputChannel = "toSoforService.getDocumentPage", outputChannel = "toResponseTransformer", requiresReply = "true")
    public Page getPage(
            @Header(SoforSessionStore.HEADER) SoforSession session,
            @Payload("id") String id
    ) {
        if (session.isAuthenticated()) {
            Page page = pageRepository.findOne(id);
            if (page != null) {
                return page;
            } else {
                throw new GeneralError();
            }
        } else {
            throw new UnauthenticatedError();
        }
    }

    @ServiceActivator(inputChannel = "toSoforService.addDocument", outputChannel = "toResponseTransformer", requiresReply = "true")
    @Transactional
    public void addDocument(
            @Header(SoforSessionStore.HEADER) SoforSession session,
            @Payload("job") String jobUuid,
            @Payload("documents") List<Document> documents
    ) throws ServiceException, IOException, URISyntaxException {
        if (!session.isAuthenticated()) {
            throw new UnauthenticatedError();
        }

        synchronized (exportSheetSyncRequestService) {
            synchronized (importSheetSyncRequestService) {
                ImportJob importJob = importJobRepository.findOne(jobUuid);
                ExportJob exportJob = exportJobRepository.findOne(jobUuid);
                if (importJob != null) {
                    importJob.documents.addAll(documents);
                    importJobSpreadsheetRepository.save(importJob,
                            ImportJobEntityMapping.FIELD_DOCUMENTS);
                    importJobRepository.saveWithLastModified(importJob);
                } else if (exportJob != null) {
                    exportJob.documents.addAll(documents);
                    exportJobSpreadsheetRepository.save(exportJob,
                            ExportJobEntityMapping.FIELD_DOCUMENTS);
                    exportJobRepository.saveWithLastModified(exportJob);
                } else {
                    throw new GeneralError();
                }
            }
        }
    }
}
