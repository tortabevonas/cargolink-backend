package hu.cargolink.backend.soforservice.messaging;

import hu.cargolink.backend.data.authentication.AuthenticationToken;
import hu.cargolink.backend.data.sofor.Sofor;

/**
 * Egy sofornek a mobil appont torteno bejelentkezest koveto munkamenetet jelkepezi
 *
 * Created by Aron on 2014.08.22..
 */
public class SoforSession {
    private AuthenticationToken authToken;
    private Sofor sofor;

    public void login(AuthenticationToken authToken, Sofor sofor) {
        this.authToken = authToken;
        this.sofor = sofor;
    }

    public void logout() {
        this.authToken = null;
        sofor = null;
    }

    public Sofor getSofor() {
        return sofor;
    }

    public AuthenticationToken getAuthToken() {
        return authToken;
    }

    public boolean isAuthenticated() {
        return authToken != null;
    }
}
