package hu.cargolink.backend.soforservice.messaging;

/**
 * Egy, a mobil apptol erkezo keres az atviteli protokollban
 *
 * Created by Aron on 2014.08.22..
 */
public class SoforRequest {
    private final short action;
    private final Object param;

    public SoforRequest(short action, Object param) {
        this.action = action;
        this.param = param;
    }

    public SoforRequest(short action) {
        this.action = action;
        this.param = null;
    }

    public short getAction() {
        return action;
    }

    public Object getParam() {
        return param;
    }
}