package hu.cargolink.backend.soforservice.transfer.general;

import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;
import hu.cargolink.backend.soforservice.transfer.core.MessageUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

/**
 * Sima szoveget ir bele egy frame-be, UTF-8 kodolassal
 *
 * Created by Aron on 2014.08.22..
 */
public class StringFrameWriter extends FrameWriter<String> {
    public StringFrameWriter(short action, String string) {
        super(action, string);
        bytes = string != null ? string.getBytes(Charset.forName("UTF8")) : new byte[0];
    }

    private final byte[] bytes;

    @Override
    public final int length() {
        return bytes.length;
    }

    @Override
    public void write(OutputStream outputStream) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        MessageUtils.copyStreams(byteArrayInputStream, outputStream, bytes.length);
    }
}
