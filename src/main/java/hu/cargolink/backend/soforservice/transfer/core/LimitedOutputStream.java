package hu.cargolink.backend.soforservice.transfer.core;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Olyan adatfolyam, amely becsomagol egy masik adatfolyamot,
 * es kikenyszeriti hogy annyi bájt ténylegesen is bele legyen írva,
 * amennyit az allow() metodus hivasaval eloirunk.
 *
 * Created by Aron on 2014.08.22..
 */
public class LimitedOutputStream extends OutputStream {
    private OutputStream wrappedStream;
    private long allowedBytes = 0;
    private long bytesWritten = 0;

    public LimitedOutputStream(OutputStream wrappedStream) {
        this.wrappedStream = wrappedStream;
    }

    public void allow(long allowedBytes) {
        if (this.allowedBytes != 0) {
            throw new IllegalStateException("Nem lett az igért számú byte beleírva az adatfolyamba!");
        }
        this.allowedBytes = allowedBytes;
        bytesWritten = 0;
    }

    private void consume(long length) {
        allowedBytes -= length;
        bytesWritten += length;
    }

    public long remaining() {
        return allowedBytes;
    }

    @Override
    public void write(int i) throws IOException {
        if (allowedBytes > 0) {
            wrappedStream.write(i);
            consume(1);
        } else {
            throw new IllegalStateException("Túl sok byte lett az adatfolyamba írva!");
        }
    }

    @Override
    public void write(byte[] bytes) throws IOException {
        write(bytes, 0, bytes.length);
    }

    @Override
    public void write(byte[] bytes, int offset, int length) throws IOException {
        if (allowedBytes > 0) {
            length = (int) Math.min(length, allowedBytes);
            wrappedStream.write(bytes, offset, length);
            consume(length);
        } else {
            throw new IllegalStateException("Túl sok byte lett az adatfolyamba írva!");
        }
    }

    @Override
    public void flush() throws IOException {
        wrappedStream.flush();
    }

    @Override
    public void close() throws IOException {
        wrappedStream.close();
    }
}
