package hu.cargolink.backend.soforservice.transfer;

import hu.cargolink.backend.data.documents.Page;
import hu.cargolink.backend.soforservice.FileResolver;
import hu.cargolink.backend.soforservice.common.MessageCodes;
import hu.cargolink.backend.soforservice.transfer.core.Frame;
import hu.cargolink.backend.soforservice.transfer.core.FrameFrameReader;
import hu.cargolink.backend.soforservice.transfer.core.FrameReader;
import hu.cargolink.backend.soforservice.transfer.general.FileFrameReader;
import hu.cargolink.backend.soforservice.transfer.general.ObjectJsonFrameReader;
import hu.cargolink.backend.utils.Reference;

import java.io.IOException;
import java.io.InputStream;

/**
 * Dokumentumoldalt olvas be egy frame-bol, beleertve a metaadatokat es a fajl tartalmat is.
 *
 * Created by Aron on 2014.08.25..
 */
public class PageFrameReader extends FrameReader<Page> {
    private final FileResolver<Page> fileResolver;

    public PageFrameReader(FileResolver<Page> fileResolver) {
        this.fileResolver = fileResolver;
    }

    @Override
    public Page read(InputStream inputStream, short action, int length) throws IOException {
        final FrameFrameReader frameFrameReader = new FrameFrameReader();

        frameFrameReader.setReader(MessageCodes.ACTION_METADATA, new ObjectJsonFrameReader(Page.class));

        final Reference<Page> pageReference = new Reference<Page>();

        frameFrameReader.read(inputStream, action, length, new FrameFrameReader.FrameListener() {
            @Override
            public void onFrame(Frame frame) {
                if (frame.action == MessageCodes.ACTION_METADATA) {
                    pageReference.referee = (Page) frame.payload;
                    frameFrameReader.setReader(MessageCodes.ACTION_BODY, new FileFrameReader(fileResolver.fileFor(pageReference.referee)));
                }
            }
        });

        return pageReference.referee;
    }
}
