package hu.cargolink.backend.soforservice.transfer.general;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hu.cargolink.backend.soforservice.transfer.core.MessageUtils;
import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Tetszoleges Java objektumot ir bele egy frame-be.
 *
 * Created by Aron on 2014.08.25..
 */
public class ObjectJsonFrameWriter<T> extends FrameWriter<T> {
    public ObjectJsonFrameWriter(short action, T payload) throws JsonProcessingException {
        super(action, payload);

        jsonBytes = new ObjectMapper().writeValueAsBytes(payload);
    }

    private byte[] jsonBytes;

    @Override
    protected int length() {
        return jsonBytes.length;
    }

    @Override
    protected void write(OutputStream outputStream) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(jsonBytes);
        MessageUtils.copyStreams(byteArrayInputStream, outputStream, jsonBytes.length);
    }
}
