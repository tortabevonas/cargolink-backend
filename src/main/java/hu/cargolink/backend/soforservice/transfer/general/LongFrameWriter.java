package hu.cargolink.backend.soforservice.transfer.general;


import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;

/**
 * Long integert ir egy frame-be.
 *
 * Created by Aron on 2014.08.25..
 */
public class LongFrameWriter extends FrameWriter<Long> {
    public LongFrameWriter(short action, Long payload) {
        super(action, payload);
    }

    @Override
    protected int length() {
        return 8;
    }

    @Override
    protected void write(OutputStream outputStream) throws IOException {
        DataOutputStream dos = new DataOutputStream(outputStream);
        dos.writeLong(getPayload());
    }
}
