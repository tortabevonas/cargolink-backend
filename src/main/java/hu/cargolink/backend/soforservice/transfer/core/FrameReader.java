package hu.cargolink.backend.soforservice.transfer.core;

import java.io.IOException;
import java.io.InputStream;

/**
 * Leolvas egy megadott hosszusagu es megadott tipusu (action parameter) frame-t egy adatfolyamrol.
 * Ez az adatfolyam a socket amin a szerver a mobil appal kommunikal.
 * Hogy konkretan mive alakitja a beolvasott frame-t, azt az alosztalyok dontik el.
 *
 * Created by Aron on 2014.08.21..
 */
public abstract class FrameReader<T> {
    public abstract T read(InputStream inputStream, short action, int length) throws IOException;
}
