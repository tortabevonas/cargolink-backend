package hu.cargolink.backend.soforservice.transfer.general;


import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;
import hu.cargolink.backend.soforservice.transfer.core.MessageUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Egy file-t ir bele egy frame-be.
 *
 * Created by Aron on 2014.08.22..
 */
public class FileFrameWriter extends FrameWriter<File> {
    public FileFrameWriter(short action, File payload) {
        super(action, payload);
    }

    @Override
    protected int length() {
        return (int) getPayload().length();
    }

    @Override
    protected void write(OutputStream outputStream) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(getPayload());
        MessageUtils.copyStreams(fileInputStream, outputStream, length());
        fileInputStream.close();
    }
}
