package hu.cargolink.backend.soforservice.transfer.core;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Az atviteli protokollhoz tartozo seged-fuggvenyek
 *
 * Created by Aron on 2014.08.22..
 */
public class MessageUtils {
    public static long copyStreams(InputStream is, OutputStream os, long length) throws IOException {
        long bytesRead = 0;
        byte[] buffer = new byte[1024];
        while (bytesRead < length) {
            int bytesToReadNow = (int) Math.min(buffer.length, length - bytesRead);
            int bytesReadNow = is.read(buffer, 0, bytesToReadNow);
            os.write(buffer, 0, bytesReadNow);
            if (bytesReadNow < 0) {
                return bytesRead;
            } else {
                bytesRead += bytesReadNow;
            }
        }
        return bytesRead;
    }
}
