package hu.cargolink.backend.soforservice.transfer.general;

import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;

import java.io.IOException;
import java.io.OutputStream;


/**
 * Ures frame-t ir. Olyankor van hasznalva, amikor egy uzenetnek nem szukseges az action-jen kivul mas adatokat tartalmaznia.
 *
 * Created by Aron on 2014.08.22..
 */
public class EmptyFrameWriter extends FrameWriter<Short> {
    public EmptyFrameWriter(short action) {
        super(action, null);
    }

    @Override
    protected int length() {
        return 0;
    }

    @Override
    protected void write(OutputStream outputStream) throws IOException {
    }
}