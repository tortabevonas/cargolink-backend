package hu.cargolink.backend.soforservice.transfer;


import hu.cargolink.backend.data.documents.Document;
import hu.cargolink.backend.data.documents.Page;
import hu.cargolink.backend.soforservice.FileResolver;
import hu.cargolink.backend.soforservice.common.MessageCodes;
import hu.cargolink.backend.soforservice.transfer.core.FrameFrameWriter;
import hu.cargolink.backend.soforservice.transfer.general.JsonFrameWriter;

import java.util.HashMap;

/**
 * Dokumentumot ir bele egy frame-be.
 *
 * Created by Aron on 2014.08.25..
 */
public class DocumentFrameWriter extends FrameFrameWriter {
    public DocumentFrameWriter(short action, Document document, FileResolver<Page> pageFileResolver) {
        super(action);

        HashMap<String, Object> metadata = new HashMap<String, Object>();
        metadata.put("type", document.getType().name());
        JsonFrameWriter metadataMessageWriter = new JsonFrameWriter(MessageCodes.ACTION_METADATA, metadata);
        addMessageWriter(metadataMessageWriter);

        for (Page page : document.getPages()) {
            PageFrameWriter pageFrameWriter = new PageFrameWriter(MessageCodes.ACTION_PAGE, page, pageFileResolver);
            addMessageWriter(pageFrameWriter);
        }
    }
}
