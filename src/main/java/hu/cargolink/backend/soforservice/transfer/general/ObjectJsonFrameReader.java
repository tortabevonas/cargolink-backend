package hu.cargolink.backend.soforservice.transfer.general;

import com.fasterxml.jackson.databind.ObjectMapper;

import hu.cargolink.backend.soforservice.transfer.core.FrameReader;

import java.io.IOException;
import java.io.InputStream;

/**
 * Tetszoleges Java objektumot olvas a frame-bol, amely JSON formatumban tartalmazza azt.
 *
 * Created by Aron on 2014.08.25..
 */
public class ObjectJsonFrameReader<T> extends FrameReader<T> {
    private Class<T> clazz;

    public ObjectJsonFrameReader(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public T read(InputStream inputStream, short action, int length) throws IOException {
        return new ObjectMapper().readValue(inputStream, clazz);
    }
}
