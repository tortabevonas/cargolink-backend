package hu.cargolink.backend.soforservice.transfer.core;

import java.io.IOException;
import java.io.InputStream;

/**
 * Becsomagol egy masik InputStream-et, es ugy tesz, mint ha az csak N bájt hosszú lenne. N bájt kiolvasása után EOF-ot jelent.
 *
 * Created by Aron on 2014.08.22..
 */
public class LimitedInputStream extends InputStream {
    private InputStream wrappedStream;
    private long allowedBytes = 0;
    private long bytesRead = 0;

    public LimitedInputStream(InputStream wrappedStream) {
        this.wrappedStream = wrappedStream;
    }

    public void allow(long length) {
        if (allowedBytes != 0) {
            throw new IllegalStateException("Az adatfolyamról nem lett az igért számú byte leolvasva!");
        }
        allowedBytes = length;
        bytesRead = 0;
    }

    private void consume(int length) {
        allowedBytes -= length;
        bytesRead += length;
    }

    public long remainingBytes() {
        return allowedBytes;
    }

    @Override
    public int read() throws IOException {
        if (allowedBytes > 0) {
            int result = wrappedStream.read();
            consume(1);
            return result;
        } else {
            return -1;
        }
    }

    @Override
    public int read(byte[] bytes, int offset, int length) throws IOException {
        if (allowedBytes > 0) {
            length = (int) Math.min(length, allowedBytes);
            int readNow = wrappedStream.read(bytes, offset, length);
            consume(readNow);
            return readNow;
        } else {
            return -1;
        }
    }

    @Override
    public int read(byte[] bytes) throws IOException {
        return read(bytes, 0, bytes.length);
    }

    @Override
    public long skip(long bytesToSkip) throws IOException {
        if (allowedBytes > 0) {
            bytesToSkip = Math.min(bytesToSkip, allowedBytes);
            long readNow = wrappedStream.skip(bytesToSkip);
            consume((int) readNow);
            return readNow;
        } else {
            return 0;
        }
    }

    public void skipRemaining() throws IOException {
        skip(remainingBytes());
    }

    public long getBytesRead() {
        return bytesRead;
    }
}
