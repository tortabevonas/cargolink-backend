package hu.cargolink.backend.soforservice.transfer.general;

import hu.cargolink.backend.soforservice.transfer.core.FrameReader;
import hu.cargolink.backend.soforservice.transfer.core.MessageUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Sima szoveget olvas be egy frame-bol, feltetelezve hogy annak kodolasa UTF-8
 *
 * Created by Aron on 2014.08.22..
 */
public class StringFrameReader extends FrameReader<String> {
    @Override
    public String read(InputStream inputStream, short action, int length) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        MessageUtils.copyStreams(inputStream, buffer, length);
        String result = new String(buffer.toByteArray(), Charset.forName("UTF8"));
        return result;
    }
}
