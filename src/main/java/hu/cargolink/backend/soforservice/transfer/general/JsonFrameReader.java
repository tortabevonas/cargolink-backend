package hu.cargolink.backend.soforservice.transfer.general;


import com.fasterxml.jackson.databind.ObjectMapper;
import hu.cargolink.backend.soforservice.transfer.core.FrameReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Egy tetszoleges Map-et olvas be a frame tartalmabol, amely JSON formatumban tartalmazza azt.
 *
 * Created by Aron on 2014.08.24..
 */
public class JsonFrameReader extends FrameReader<Map<String, Object>> {
    private StringFrameReader stringMessageReader = new StringFrameReader();

    @Override
    public Map<String, Object> read(InputStream inputStream, short action, int length) throws IOException {
        String string = stringMessageReader.read(inputStream, action, length);
        return new ObjectMapper().readValue(string, HashMap.class);
    }
}
