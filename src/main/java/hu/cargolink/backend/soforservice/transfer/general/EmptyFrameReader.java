package hu.cargolink.backend.soforservice.transfer.general;

import hu.cargolink.backend.soforservice.transfer.core.FrameReader;

import java.io.IOException;
import java.io.InputStream;


/**
 * Ures (0 hosszusagu) frame-ket olvas, es a frame action-jevel ter vissza.
 * Akkor hasznalt, amikor az action-nek nem szukseges parametereket atadni.
 *
 * Created by Aron on 2014.08.22..
 */
public class EmptyFrameReader extends FrameReader<Short> {
    @Override
    public Short read(InputStream message, short action, int length) throws IOException {
        return action;
    }
}
