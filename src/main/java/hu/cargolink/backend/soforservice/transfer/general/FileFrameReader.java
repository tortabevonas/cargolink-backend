package hu.cargolink.backend.soforservice.transfer.general;


import hu.cargolink.backend.soforservice.transfer.core.FrameReader;
import hu.cargolink.backend.soforservice.transfer.core.MessageUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Olyan frame reader, amely egy frame-t egy fajlba olvas be.
 * Olyankor van hasznalva, amikor a mobil app dokumentumokat tolt fel.
 *
 * Created by Aron on 2014.08.22..
 */
public class FileFrameReader extends FrameReader<File> {
    private final File file;

    public FileFrameReader(File file) {
        this.file = file;
    }

    @Override
    public File read(InputStream inputStream, short action, int length) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        MessageUtils.copyStreams(inputStream, fileOutputStream, length);
        fileOutputStream.close();
        return file;
    }

}
