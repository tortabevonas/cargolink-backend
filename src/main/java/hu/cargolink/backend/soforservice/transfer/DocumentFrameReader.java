package hu.cargolink.backend.soforservice.transfer;


import hu.cargolink.backend.data.documents.Document;
import hu.cargolink.backend.data.documents.DocumentType;
import hu.cargolink.backend.data.documents.Page;
import hu.cargolink.backend.soforservice.FileResolver;
import hu.cargolink.backend.soforservice.common.MessageCodes;
import hu.cargolink.backend.soforservice.transfer.core.Frame;
import hu.cargolink.backend.soforservice.transfer.core.FrameFrameReader;
import hu.cargolink.backend.soforservice.transfer.core.FrameReader;
import hu.cargolink.backend.soforservice.transfer.general.JsonFrameReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

/**
 * Egy dokumentumot olvas be egy frame-bol.
 *
 * Created by Aron on 2014.08.25..
 */
public class DocumentFrameReader extends FrameReader<Document> {
    private final FileResolver<Page> pageFileResolver;

    public DocumentFrameReader(FileResolver<Page> pageFileResolver) {
        this.pageFileResolver = pageFileResolver;
    }

    @Override
    public Document read(InputStream inputStream, short action, int length) throws IOException {
        Document document = new Document();

        FrameFrameReader frameFrameReader = new FrameFrameReader();
        frameFrameReader.setReader(MessageCodes.ACTION_METADATA, new JsonFrameReader());
        frameFrameReader.setReader(MessageCodes.ACTION_PAGE, new PageFrameReader(pageFileResolver));

        List<Frame> frames = frameFrameReader.read(inputStream, action, length);

        for (Frame frame : frames) {
            if (frame.action == MessageCodes.ACTION_METADATA) {
                Map<String, Object> metadata = (Map<String, Object>) frame.payload;
                document.setType(DocumentType.valueOf((String) metadata.get("type")));
            } else if (frame.action == MessageCodes.ACTION_PAGE) {
                Page page = (Page) frame.payload;
                document.getPages().add(page);
            }
        }

        return document;
    }
}
