package hu.cargolink.backend.soforservice.transfer.general;


import hu.cargolink.backend.soforservice.transfer.core.FrameReader;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Long integert olvas ki egy frame-bol.
 *
 * Created by Aron on 2014.08.25..
 */
public class LongFrameReader extends FrameReader<Long> {
    @Override
    public Long read(InputStream inputStream, short action, int length) throws IOException {
        DataInputStream dis = new DataInputStream(inputStream);
        return dis.readLong();
    }
}
