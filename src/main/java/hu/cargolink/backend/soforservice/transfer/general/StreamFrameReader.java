package hu.cargolink.backend.soforservice.transfer.general;

import hu.cargolink.backend.soforservice.transfer.core.FrameReader;

import java.io.IOException;
import java.io.InputStream;

/**
 * Lehetove teszi az alkalmazas szamara, hogy egy frame-t sajat maga dolgozzon fel az adatfolyam beolvasasaval.
 *
 * Created by Aron on 2014.09.03..
 */
public class StreamFrameReader extends FrameReader<InputStream> {
    @Override
    public InputStream read(InputStream inputStream, short action, int length) throws IOException {
        return inputStream;
    }
}
