package hu.cargolink.backend.soforservice.transfer.core;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Rair egy adott tipusu framet egy adatfolyamra.
 * Hogy hogyan szerializalja a frame tartalmat, azt az alosztalyok dontik el.
 *
 * Created by Aron on 2014.08.22..
 */
public abstract class FrameWriter<T> {
    private final short action;
    private T payload;

    public FrameWriter(short action, T payload) {
        this.action = action;
        this.payload = payload;
    }

    public final void performWrite(OutputStream outputStream) throws IOException {
        DataOutputStream dos = new DataOutputStream(outputStream);
        dos.writeShort(action);
        dos.writeInt(length());
        write(outputStream);
    }

    protected final T getPayload() {
        return payload;
    }

    public final int getLength() {
        return length() + 2 + 4;
    }

    protected abstract int length();

    protected abstract void write(OutputStream outputStream) throws IOException;
}
