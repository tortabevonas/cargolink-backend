package hu.cargolink.backend.soforservice.transfer.general;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

/**
 * Egy Map-et ir a frame-be, JSON-ba szerializalva.
 * A JSON szerializalasra a Jackson serializer szabalyai vonatkoznak, mert ez az osztaly a kore epul.
 *
 * Created by Aron on 2014.08.24..
 */
public class JsonFrameWriter extends FrameWriter<Map<String, Object>> {
    public JsonFrameWriter(short action, Map<String, Object> payload) {
        super(action, payload);
        try {
            stringMessageWriter = new StringFrameWriter((short) 0, new ObjectMapper().writeValueAsString(payload));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private StringFrameWriter stringMessageWriter;

    @Override
    protected int length() {
        return stringMessageWriter.length();
    }

    @Override
    protected void write(OutputStream outputStream) throws IOException {
        stringMessageWriter.write(outputStream);
    }
}
