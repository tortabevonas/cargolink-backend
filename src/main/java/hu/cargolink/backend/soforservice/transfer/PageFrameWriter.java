package hu.cargolink.backend.soforservice.transfer;

import com.fasterxml.jackson.core.JsonProcessingException;
import hu.cargolink.backend.data.documents.Page;
import hu.cargolink.backend.soforservice.FileResolver;
import hu.cargolink.backend.soforservice.common.MessageCodes;
import hu.cargolink.backend.soforservice.transfer.core.FrameFrameWriter;
import hu.cargolink.backend.soforservice.transfer.general.FileFrameWriter;
import hu.cargolink.backend.soforservice.transfer.general.ObjectJsonFrameWriter;

/**
 * Dokumentumoldalt ir bele egy frame-be, beleertve a metaadatokat es a file tartalmat is.
 *
 * Created by Aron on 2014.09.03..
 */
public class PageFrameWriter extends FrameFrameWriter {
    public PageFrameWriter(short action, Page page, FileResolver<Page> pageFileResolver) {
        super(action);

        try {
            addMessageWriter(new ObjectJsonFrameWriter(MessageCodes.ACTION_METADATA, page));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        addMessageWriter(new FileFrameWriter(MessageCodes.ACTION_BODY, pageFileResolver.fileFor(page)));
    }
}
