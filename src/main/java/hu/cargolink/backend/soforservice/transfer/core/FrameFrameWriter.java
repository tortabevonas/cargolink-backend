package hu.cargolink.backend.soforservice.transfer.core;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Olyan FrameWriter, amely al-frameket ír bele egy frame-be.
 * Az ativiteli protokoll kozponti eleme, mivel egy uzenet gyakran tobb frame-bol all.
 *
 * Created by Aron on 2014.08.22..
 */
public class FrameFrameWriter extends FrameWriter<FrameWriter[]> {
    private List<FrameWriter> frameWriters;

    public FrameFrameWriter(short action) {
        super(action, null);
        frameWriters = new ArrayList<FrameWriter>();
    }

    public FrameFrameWriter(short action, FrameWriter... frameWritersToAdd) {
        this(action);
        frameWriters.addAll(Arrays.asList(frameWritersToAdd));
    }

    public FrameFrameWriter(short action, List<FrameWriter> frameWritersToAdd) {
        this(action);
        frameWriters.addAll(frameWritersToAdd);
    }

    public void addMessageWriter(FrameWriter frameWriter) {
        frameWriters.add(frameWriter);
    }

    @Override
    public final int length() {
        int length = 0;
        for (FrameWriter frameWriter : frameWriters) {
            length += frameWriter.getLength();
        }
        return length;
    }

    @Override
    public final void write(OutputStream outputStream) throws IOException {
        LimitedOutputStream los = new LimitedOutputStream(outputStream);
        for (FrameWriter frameWriter : frameWriters) {

            los.allow(frameWriter.getLength());
            frameWriter.performWrite(los);
        }
    }
}
