package hu.cargolink.backend.soforservice.transfer.core;

/**
 * A sajat fejlesztesu binaris atviteli protokoll kozponti eleme.
 * A frame egy muveletbol (action) es egy objektumbol all, ami a muvelet parameterekent foghato fel.
 * Egy frame a binaris adatfolyamon a kovetkezokeppen kerul tovabbitasra:
 * [4 bytes] frame hossza (N)
 * [2 bytes] frame action-je
 * [N bytes] frame tartalma
 *
 * Created by Aron on 2014.08.24..
 */
public class Frame {
    public short action;
    public Object payload;
}
