package hu.cargolink.backend.soforservice.transfer.core;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Egy olyan frame reader, amely al-frame-eket olvas ki egy frame-bol.
 * Az atviteli protokoll kozponti eleme, mivel egy uzenet gyakran tobb reszbol epul fel.
 *
 * Created by Aron on 2014.08.21..
 */
public class FrameFrameReader extends FrameReader<List<Frame>> {
    private Map<Short, FrameReader> readers = new HashMap<Short, FrameReader>();
    private FrameReader defaultReader;

    public void setReader(short type, FrameReader handler) {
        readers.put(type, handler);
    }

    public void setDefaultReader(FrameReader defaultReader) {
        this.defaultReader = defaultReader;
    }

    @Override
    public List<Frame> read(InputStream inputStream, short a, int size) throws IOException {
        return read(inputStream, a, size, null);
    }

    public List<Frame> read(InputStream inputStream, short a, int size, FrameListener frameListener) throws IOException {
        List<Frame> result = new ArrayList<Frame>();
        long bytesRead = 0;
        do {
            DataInputStream dis = new DataInputStream(inputStream);

            short action = dis.readShort();
            int length = dis.readInt();
            bytesRead += 2 + 4;

            FrameReader reader = readers.get(action);

            LimitedInputStream lis = new LimitedInputStream(inputStream);
            lis.allow(length);

            Frame frame = new Frame();
            frame.action = action;

            if (reader != null) {
                frame.payload = reader.read(lis, action, length);
            } else if (defaultReader != null) {
                frame.payload = defaultReader.read(lis, action, length);
            } else {
                lis.skip(length);
            }
            lis.skipRemaining();
            bytesRead += length;

            if (frameListener != null) {
                frameListener.onFrame(frame);
            }
            result.add(frame);
        } while (bytesRead < size);

        return result;
    }

    public Frame readOne(InputStream inputStream) throws IOException {
        return read(inputStream, (short) 0, 0).get(0);
    }

    public interface FrameListener {
        public void onFrame(Frame frame);
    }
}
