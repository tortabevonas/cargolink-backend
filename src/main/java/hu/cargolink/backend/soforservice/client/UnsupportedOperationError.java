package hu.cargolink.backend.soforservice.client;

/**
 * A HTTP 404 megfeleloje. Azt jelzi, hogy a megadott kodu muvelet nem letezik.
 *
 * Created by Aron on 2014.08.25..
 */
public class UnsupportedOperationError extends ServerError {
}
