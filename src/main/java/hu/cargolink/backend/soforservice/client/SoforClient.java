package hu.cargolink.backend.soforservice.client;


import hu.cargolink.backend.data.documents.Document;
import hu.cargolink.backend.data.documents.Page;
import hu.cargolink.backend.data.exports.ExportJob;
import hu.cargolink.backend.data.imports.ImportJob;
import hu.cargolink.backend.soforservice.FileResolver;
import hu.cargolink.backend.soforservice.common.JobUpdate;
import hu.cargolink.backend.soforservice.common.LatLng;
import hu.cargolink.backend.soforservice.common.MessageCodes;
import hu.cargolink.backend.soforservice.transfer.DocumentFrameWriter;
import hu.cargolink.backend.soforservice.transfer.PageFrameReader;
import hu.cargolink.backend.soforservice.transfer.core.*;
import hu.cargolink.backend.soforservice.transfer.general.*;
import org.apache.http.conn.ssl.SSLSocketFactory;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.KeyStore;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A mobil appban ezen a szolgaltatason keresztul kommunial a sofor a szerverrel.
 * Ez a valtozat kizarolag a teszteles megkonnyitesenek celjaval keszult.
 *
 * Created by Aron on 2014.08.23..
 */
public abstract class SoforClient {
    private Socket socket;
    private String host;
    private int port;
    private FrameFrameReader messageReader;
    private long lastUpdate = 0;
    private SSLSocketFactory sslSocketFactory;

    private static final int CONNECT_TIMEOUT = 10000;
    private static final int READ_TIMEOUT = 30000;

    public SoforClient(String host, int port, InputStream keystoreInputStream, String keystorePassword) {
        this.host = host;
        this.port = port;

        messageReader = new FrameFrameReader();
        messageReader.setDefaultReader(new EmptyFrameReader());

        try {
            KeyStore trusted = KeyStore.getInstance("BKS");
            try {
                trusted.load(keystoreInputStream, keystorePassword.toCharArray());
            } finally {
                keystoreInputStream.close();
            }
            sslSocketFactory = new SSLSocketFactory(trusted);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void ensureConnected() throws IOException {
        if (!isConnected()) {
            socket = sslSocketFactory.createSocket();
            socket.connect(new InetSocketAddress(host, port), CONNECT_TIMEOUT);
            socket.setSoTimeout(READ_TIMEOUT);
            if (isLoggedIn()) {
                authenticate(getAuthToken());
            }
        }
    }

    public boolean isLoggedIn() {
        return getAuthToken() != null;
    }

    private void authenticate(String token) {
        try {
            request(new StringFrameWriter(MessageCodes.REQUEST_AUTH, token), new EmptyFrameReader(), true);
        } catch (GeneralError error) {
            deleteToken();
            throw error;
        }
    }

    private boolean isConnected() {
        return socket != null && socket.isConnected();
    }

    public void disconnect() throws IOException {
        if (socket != null) {
            Socket socketToClose = socket;
            socket = null;
            socketToClose.close();
        }
    }

    private <T> T request(FrameWriter frameWriter, FrameReader<T> successReader) {
        return (T) request(frameWriter, successReader, false);
    }

    private synchronized <T> T request(FrameWriter frameWriter, FrameReader<T> successReader, boolean silentRun) {
        try {
            if (!silentRun) {
                ensureConnected();
            }

            frameWriter.performWrite(socket.getOutputStream());

            messageReader.setReader(MessageCodes.RESPONSE_OK, successReader);
            Frame frame = messageReader.readOne(socket.getInputStream());
            if (frame.action == MessageCodes.RESPONSE_SERVER_ERROR) {
                throw new ServerError();
            } else if (frame.action == MessageCodes.RESPONSE_ERROR) {
                throw new GeneralError();
            } else if (frame.action == MessageCodes.RESPONSE_UNAUTHENTICATED) {
                throw new UnauthenticatedError();
            } else if (frame.action == MessageCodes.RESPONSE_OK) {
                return (T) frame.payload;
            } else {
                throw new UnsupportedOperationError();
            }
        } catch (ServerError e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            try {
                disconnect();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            throw new RuntimeException(e);
        }
    }

    private void request(FrameWriter frameWriter) {
        request(frameWriter, new EmptyFrameReader());
    }

    public void login(String name, String password) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("name", name);
        map.put("password", password);

        String authToken = request(new JsonFrameWriter(MessageCodes.REQUEST_LOGIN, map), new StringFrameReader());
        storeCredentials(name, authToken);
        authenticate(authToken);
    }


    public void logout() {
        //String token = getAuthToken();
        //request(new StringMessageWriter(MessageCodes.REQUEST_LOGOUT, token));
        deleteToken();
    }

    public JobUpdate update() {
        JobUpdate result = request(
                new LongFrameWriter(MessageCodes.REQUEST_UPDATES, lastUpdate),
                new ObjectJsonFrameReader<JobUpdate>(JobUpdate.class)
        );
        lastUpdate = result.when;
        return result;

    }

    /*
     * Import állapotváltások
     */

    public void importLkwFelvette(ImportJob job, LatLng position, boolean vamkezelesKesz, String serules, int colli, BigDecimal kg, BigDecimal cbm, List<Document> documents, FileResolver<Page> pageFileResolver) {
        FrameFrameWriter messageWriter = new FrameFrameWriter(MessageCodes.REQUEST_IMPORT_LKW_FELVETTE);

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("job", job.id);
        data.put("position", position);
        data.put("vamkezelesKesz", vamkezelesKesz);
        data.put("serules", serules);
        data.put("colli", colli);
        data.put("kg", kg);
        data.put("cbm", cbm);
        messageWriter.addMessageWriter(new JsonFrameWriter(MessageCodes.ACTION_METADATA, data));

        for (Document document : documents) {
            messageWriter.addMessageWriter(new DocumentFrameWriter(MessageCodes.ACTION_DOCUMENT, document, pageFileResolver));
        }

        request(messageWriter);
    }

    public void importVamkezelesBefejezese(ImportJob job, List<Document> documents, FileResolver<Page> pageFileResolver) {
        FrameFrameWriter messageWriter = new FrameFrameWriter(MessageCodes.REQUEST_IMPORT_VAMKEZELES_BEFEJEZ);

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("job", job.id);
        messageWriter.addMessageWriter(new JsonFrameWriter(MessageCodes.ACTION_METADATA, data));

        for (Document document : documents) {
            messageWriter.addMessageWriter(new DocumentFrameWriter(MessageCodes.ACTION_DOCUMENT, document, pageFileResolver));
        }

        request(messageWriter);
    }

    public void importRaktarbaBeerkezett(ImportJob job) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("job", job.id);
        JsonFrameWriter messageWriter = new JsonFrameWriter(MessageCodes.REQUEST_IMPORT_RAKTARBA_BEERKEZETT, data);

        request(messageWriter);
    }

    public void importLerakvaCimzettnel(ImportJob job, LatLng position, String serules, List<Document> documents, FileResolver<Page> pageFileResolver) {
        FrameFrameWriter messageWriter = new FrameFrameWriter(MessageCodes.REQUEST_IMPORT_LERAKVA_CIMZETTNEL);

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("job", job.id);
        data.put("position", position);
        data.put("serules", serules);
        messageWriter.addMessageWriter(new JsonFrameWriter(MessageCodes.ACTION_METADATA, data));

        for (Document document : documents) {
            messageWriter.addMessageWriter(new DocumentFrameWriter(MessageCodes.ACTION_DOCUMENT, document, pageFileResolver));
        }

        request(messageWriter);
    }

    /*
     * Export állapotváltások
     */

    public void exportLkwFelvette(ExportJob job, LatLng position, String serules, List<Document> documents, FileResolver<Page> pageFileResolver) {
        FrameFrameWriter messageWriter = new FrameFrameWriter(MessageCodes.REQUEST_EXPORT_LKW_FELVETTE);

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("job", job.getId());
        data.put("position", position);
        data.put("serules", serules);
        messageWriter.addMessageWriter(new JsonFrameWriter(MessageCodes.ACTION_METADATA, data));

        for (Document document : documents) {
            messageWriter.addMessageWriter(new DocumentFrameWriter(MessageCodes.ACTION_DOCUMENT, document, pageFileResolver));
        }

        request(messageWriter);
    }

    public void exportRaktarbaBeerkezett(ExportJob job) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("job", job.getId());
        JsonFrameWriter messageWriter = new JsonFrameWriter(MessageCodes.REQUEST_EXPORT_RAKTARBA_BEERKEZETT, data);

        request(messageWriter);
    }

    public void exportFelrakvaPotkocsira(ExportJob job) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("job", job.getId());

        request(new JsonFrameWriter(MessageCodes.REQUEST_EXPORT_FELRAKVA_POTKOCSIRA, data));
    }

    public void exportLerakvaCimzettnel(ExportJob job, LatLng position, String serules, List<Document> documents, FileResolver<Page> pageFileResolver) {
        FrameFrameWriter messageWriter = new FrameFrameWriter(MessageCodes.REQUEST_EXPORT_LERAKVA_CIMZETTNEL);

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("job", job.getId());
        data.put("position", position);
        data.put("serules", serules);
        messageWriter.addMessageWriter(new JsonFrameWriter(MessageCodes.ACTION_METADATA, data));

        for (Document document : documents) {
            messageWriter.addMessageWriter(new DocumentFrameWriter(MessageCodes.ACTION_DOCUMENT, document, pageFileResolver));
        }

        request(messageWriter);
    }

    public Page getPage(String id, FileResolver<Page> pageFileResolver) {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("id", id);
        JsonFrameWriter frameWriter = new JsonFrameWriter(MessageCodes.REQUEST_GET_PAGE, data);

        return request(frameWriter, new PageFrameReader(pageFileResolver));
    }

    protected abstract void storeCredentials(String name, String token);

    protected abstract void deleteToken();

    protected abstract String getAuthToken();

    public abstract String getSoforName();
}
