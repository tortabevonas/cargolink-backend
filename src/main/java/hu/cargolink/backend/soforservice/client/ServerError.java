package hu.cargolink.backend.soforservice.client;

/**
 * Olyan hibat jelkepez, amely a mobil app keresenek feldolgozasa soran, nem pedig a kommunikacio kozben tortenik
 *
 * Created by Aron on 2014.08.25..
 */
public class ServerError extends RuntimeException {
}
