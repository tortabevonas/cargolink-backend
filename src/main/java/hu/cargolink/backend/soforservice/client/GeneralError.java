package hu.cargolink.backend.soforservice.client;

/**
 * Altalanos hiba, akkor fordul elo, ha egy adott muvelet az uzleti logika szabalyai miatt nem hajthato vegre
 *
 * Created by Aron on 2014.08.25..
 */
public class GeneralError extends ServerError {
}
