package hu.cargolink.backend.soforservice.client;

/**
 * Azt jelzi, hogy a sofor nincsen bejelentkezve, vagy az auth token-je nem ervenyes,
 * tehat be kell jelentkeznie a muvelet vegrehajtasahoz
 *
 * Created by Aron on 2014.08.25..
 */
public class UnauthenticatedError extends ServerError {
}
