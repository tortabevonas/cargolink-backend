package hu.cargolink.backend.soforservice;

import hu.cargolink.backend.soforservice.client.GeneralError;
import hu.cargolink.backend.soforservice.client.UnauthenticatedError;
import hu.cargolink.backend.soforservice.common.MessageCodes;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.handler.ReplyRequiredException;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandlingException;
import org.springframework.stereotype.Component;

/**
 * Ez a szolgaltatas akkor general valaszuzenetet a mobil appnak,
 * ha a kerest kiszolgalo szolgaltatasban hiba keletkezik.
 *
 * Created by Aron on 2014.08.22..
 */
@Component
public class SoforErrorService {
    @ServiceActivator(inputChannel = "toSoforErrorService")
    public short returnServerErrorCode(Message<MessageHandlingException> message) {
        Throwable exception = message.getPayload();
        if (exception instanceof MessageHandlingException) {
            exception = exception.getCause();
        }

        if (exception instanceof ReplyRequiredException) {
            return MessageCodes.RESPONSE_OK;
        } else if (exception instanceof GeneralError) {
            return MessageCodes.RESPONSE_ERROR;
        } else if (exception instanceof UnauthenticatedError) {
            return MessageCodes.RESPONSE_UNAUTHENTICATED;
        }
        return MessageCodes.RESPONSE_SERVER_ERROR;
    }

    @ServiceActivator(inputChannel = "toSoforService.unknown")
    public void unknownRequest() {
    }
}
