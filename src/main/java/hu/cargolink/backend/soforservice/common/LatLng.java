package hu.cargolink.backend.soforservice.common;

import javax.persistence.Embeddable;

/**
 * Foldrajzi helyet ir le
 *
 * Created by Aron on 2014.08.21..
 */
@Embeddable
public class LatLng {
    public double lat;
    public double lng;

    public static final String SEPARATOR = ",";

    public LatLng() {
    }

    public LatLng(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public boolean isZero() {
        return lat == 0.0 && lng == 0.0;
    }

    @Override
    public String toString() {
        return "" + lat + SEPARATOR + lng + "";
    }


    public static final LatLng fromString(String string) {
        if (string == null) {
            return null;
        }
        String[] coordStrings = string.split(SEPARATOR);
        if (coordStrings.length < 2) {
            return null;
        } else {
            try {
                return new LatLng(Double.valueOf(coordStrings[0]), Double.valueOf(coordStrings[1]));
            } catch (NumberFormatException e) {
                return null;
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LatLng)) return false;

        LatLng latLng = (LatLng) o;

        if (Double.compare(latLng.lat, lat) != 0) return false;
        if (Double.compare(latLng.lng, lng) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(lat);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lng);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
