package hu.cargolink.backend.soforservice.common;

/**
 * A mobil appal valo, sajat fejlesztesu binaris protokollon keresztul torteno kommunikacio soran hasznalt uzenetkodok.
 *
 * Created by Aron on 2014.08.25..
 */
public class MessageCodes {

    public static final short REQUEST_UNKNOWN = 0;
    public static final short REQUEST_LOGIN = 1;
    public static final short REQUEST_AUTH = 2;
    public static final short REQUEST_DEAUTH = 3;
    public static final short REQUEST_LOGOUT = 4;
    public static final short REQUEST_UPDATES = 5;
    public static final short REQUEST_IMPORT_LKW_FELVETTE = 6;
    public static final short REQUEST_IMPORT_VAMKEZELES_BEFEJEZ = 7;
    public static final short REQUEST_IMPORT_RAKTARBA_BEERKEZETT = 8;
    public static final short REQUEST_IMPORT_LERAKVA_CIMZETTNEL = 9;
    public static final short REQUEST_EXPORT_LKW_FELVETTE = 10;
    public static final short REQUEST_EXPORT_RAKTARBA_BEERKEZETT = 11;
    public static final short REQUEST_EXPORT_FELRAKVA_POTKOCSIRA = 12;
    public static final short REQUEST_EXPORT_LERAKVA_CIMZETTNEL = 13;
    public static final short REQUEST_GET_PAGE = 14;
    public static final short REQUEST_SET_POSITION_FOR_JOB = 15;
    public static final short REQUEST_ADD_DOCUMENT = 16;

    public static final short ACTION_DOCUMENT = 821;
    public static final short ACTION_METADATA = 3874;
    public static final short ACTION_BODY = 392;
    public static final short ACTION_PAGE = 4324;

    public static final short RESPONSE_SERVER_ERROR = 0;
    public static final short RESPONSE_ERROR = 1;
    public static final short RESPONSE_UNAUTHENTICATED = 2;
    public static final short RESPONSE_OK = 3;
}
