package hu.cargolink.backend.soforservice.common;

import hu.cargolink.backend.data.exports.ExportJob;
import hu.cargolink.backend.data.imports.ImportJob;

import java.util.Set;

/**
 * Ebben az objektumban kuldjuk el a mobil appnak a legutobbi szinkronizacio ota megvaltozott adatokat.
 *
 * Created by Aron on 2014.08.20..
 */
public class JobUpdate {
    public Set<ExportJob> exportJobs;
    public Set<ImportJob> importJobs;

    /**
     * Az elkuldott munkak adatbazisbol valo kiolvasasanak pillanata
     */
    public long when;
}
