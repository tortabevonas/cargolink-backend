package hu.cargolink.backend.soforservice;

import hu.cargolink.backend.data.exports.ExportJobRepository;
import hu.cargolink.backend.data.imports.ImportJobRepository;
import hu.cargolink.backend.soforservice.client.UnauthenticatedError;
import hu.cargolink.backend.soforservice.common.JobUpdate;
import hu.cargolink.backend.soforservice.integration.SoforSessionStore;
import hu.cargolink.backend.soforservice.messaging.SoforSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.Header;
import org.springframework.integration.annotation.Payload;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

/**
 * A mobil app adatainak frissiteset vegzo szolgaltatas.
 * Az mobil app rendszeres idokozonkent lekeri azokat a munkakat, amelyek a legutobbi ilyen keres ota valtoztak (lastModified tulajdonsaguk alapjan).
 * Az legutobbi lekeres idejet a szerver oraja szerint merjuk.
 * Kliens oldalon ez az ido eltarolodik, es az app kovetkezo frissiteskor azt kuldi majd vissza.
 *
 * Created by Aron on 2014.08.23..
 */
@Component
public class SoforUpdateService {
    @Autowired
    private ExportJobRepository exportJobRepository;

    @Autowired
    private ImportJobRepository importJobRepository;

    // Munkalista olvasása

    @ServiceActivator(inputChannel = "toSoforService.updates", outputChannel = "toResponseTransformer", requiresReply = "true")
    public JobUpdate getInterestingJobs(
            @Header(SoforSessionStore.HEADER) SoforSession session,
            @Payload long since
    ) {
        if (!session.isAuthenticated()) {
            throw new UnauthenticatedError();
        }

        JobUpdate jobUpdate = new JobUpdate();

        jobUpdate.when = System.currentTimeMillis();

        jobUpdate.exportJobs = exportJobRepository.findSoforUpdates(since);

        jobUpdate.importJobs = importJobRepository.findSoforUpdates(since);

        return jobUpdate;
    }
}
