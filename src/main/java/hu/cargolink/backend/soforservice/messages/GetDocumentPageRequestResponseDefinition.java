package hu.cargolink.backend.soforservice.messages;

import hu.cargolink.backend.data.documents.Page;
import hu.cargolink.backend.soforservice.FileResolver;
import hu.cargolink.backend.soforservice.common.MessageCodes;
import hu.cargolink.backend.soforservice.integration.RequestResponseDefinition;
import hu.cargolink.backend.soforservice.transfer.PageFrameWriter;
import hu.cargolink.backend.soforservice.transfer.core.FrameReader;
import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;
import hu.cargolink.backend.soforservice.transfer.general.JsonFrameReader;

import java.util.Map;

/**
 * Dokumentum lekeresere valo keres-valasz part definial a sajat atviteli protokollban
 *
 * Created by Aron on 2014.09.03..
 */
public class GetDocumentPageRequestResponseDefinition extends RequestResponseDefinition<Map<String, Object>, Page> {
    private final FileResolver<Page> pageFileResolver;

    public GetDocumentPageRequestResponseDefinition(FileResolver<Page> pageFileResolver) {
        this.pageFileResolver = pageFileResolver;
    }

    @Override
    protected FrameReader<Map<String, Object>> messageReader() {
        return new JsonFrameReader();
    }

    @Override
    protected FrameWriter messageWriter(Page input) throws Exception {
        return new PageFrameWriter(MessageCodes.RESPONSE_OK, input, pageFileResolver);
    }
}
