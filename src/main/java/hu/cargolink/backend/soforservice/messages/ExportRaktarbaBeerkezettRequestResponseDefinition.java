package hu.cargolink.backend.soforservice.messages;

import hu.cargolink.backend.soforservice.integration.RequestResponseDefinition;
import hu.cargolink.backend.soforservice.transfer.core.FrameReader;
import hu.cargolink.backend.soforservice.transfer.general.JsonFrameReader;
import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;

import java.util.Map;

/**
 * "Export munka raktarba leadva" keres-valasz part definial a sajat atviteli protokollban
 *
 * Created by Aron on 2014.08.25..
 */
public class ExportRaktarbaBeerkezettRequestResponseDefinition extends RequestResponseDefinition<Map<String, Object>, Void> {
    @Override
    protected FrameReader<Map<String, Object>> messageReader() {
        return new JsonFrameReader();
    }

    @Override
    protected FrameWriter messageWriter(Void input) throws Exception {
        return null;
    }
}
