package hu.cargolink.backend.soforservice.messages;

import hu.cargolink.backend.soforservice.integration.RequestResponseDefinition;
import hu.cargolink.backend.soforservice.transfer.core.FrameReader;
import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;
import hu.cargolink.backend.soforservice.transfer.general.StringFrameReader;

/**
 * Meglevo auth token-nel torteno azonositasi keres-valasz part definial a sajat atviteli protokollban
 *
 * Created by Aron on 2014.08.24..
 */
public class AuthRequestResponseDefinition extends RequestResponseDefinition<String, Void> {
    @Override
    protected FrameReader<String> messageReader() {
        return new StringFrameReader();
    }

    @Override
    protected FrameWriter<Void> messageWriter(Void input) {
        return null;
    }
}
