package hu.cargolink.backend.soforservice.messages;

import com.fasterxml.jackson.core.JsonProcessingException;
import hu.cargolink.backend.soforservice.common.JobUpdate;
import hu.cargolink.backend.soforservice.common.MessageCodes;
import hu.cargolink.backend.soforservice.integration.RequestResponseDefinition;
import hu.cargolink.backend.soforservice.transfer.general.LongFrameReader;
import hu.cargolink.backend.soforservice.transfer.general.ObjectJsonFrameWriter;
import hu.cargolink.backend.soforservice.transfer.core.FrameReader;
import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;

/**
 * "Legutobbi frissites ota megvaltozott adatok lekerese" keres-valasz part definial a sajat atviteli protokollban
 *
 * Created by Aron on 2014.08.25..
 */
public class UpdateRequestResponseDefinition extends RequestResponseDefinition<Long, JobUpdate> {
    @Override
    protected FrameReader<Long> messageReader() {
        return new LongFrameReader();
    }

    @Override
    protected FrameWriter messageWriter(JobUpdate input) throws JsonProcessingException {
        return new ObjectJsonFrameWriter<JobUpdate>(MessageCodes.RESPONSE_OK, input);
    }
}
