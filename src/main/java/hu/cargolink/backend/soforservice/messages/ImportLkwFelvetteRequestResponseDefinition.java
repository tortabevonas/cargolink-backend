package hu.cargolink.backend.soforservice.messages;

import hu.cargolink.backend.data.documents.Document;
import hu.cargolink.backend.data.documents.Page;
import hu.cargolink.backend.soforservice.FileResolver;
import hu.cargolink.backend.soforservice.common.MessageCodes;
import hu.cargolink.backend.soforservice.integration.RequestResponseDefinition;
import hu.cargolink.backend.soforservice.transfer.DocumentFrameReader;
import hu.cargolink.backend.soforservice.transfer.core.FrameReader;
import hu.cargolink.backend.soforservice.transfer.general.JsonFrameReader;
import hu.cargolink.backend.soforservice.transfer.core.Frame;
import hu.cargolink.backend.soforservice.transfer.core.FrameFrameReader;
import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * "Import munka felveve" keres-valasz part definial a sajat atviteli protokollban
 *
 * Created by Aron on 2014.08.25..
 */
public class ImportLkwFelvetteRequestResponseDefinition extends RequestResponseDefinition<Map<String, Object>, Void> {

    private final FileResolver<Page> pageFileResolver;

    public ImportLkwFelvetteRequestResponseDefinition(FileResolver<Page> pageFileResolver) {
        this.pageFileResolver = pageFileResolver;
    }

    @Override
    protected FrameReader<Map<String, Object>> messageReader() {
        return new FrameReader<Map<String, Object>>() {
            @Override
            public Map<String, Object> read(InputStream inputStream, short action, int length) throws IOException {
                FrameFrameReader frameMessageReader = new FrameFrameReader();
                frameMessageReader.setReader(MessageCodes.ACTION_METADATA, new JsonFrameReader());
                frameMessageReader.setReader(MessageCodes.ACTION_DOCUMENT, new DocumentFrameReader(pageFileResolver));

                Map<String, Object> result = new HashMap<String, Object>();
                List<Document> documents = new ArrayList<Document>();

                List<Frame> frames = frameMessageReader.read(inputStream, action, length);
                for (Frame frame : frames) {
                    if (frame.action == MessageCodes.ACTION_METADATA) {
                        result = (Map<String, Object>) frame.payload;
                    } else if (frame.action == MessageCodes.ACTION_DOCUMENT) {
                        documents.add((Document) frame.payload);
                    }
                }
                result.put("documents", documents);

                return result;
            }
        };
    }

    @Override
    protected FrameWriter messageWriter(Void input) throws Exception {
        return null;
    }
}
