package hu.cargolink.backend.soforservice.messages;

import hu.cargolink.backend.soforservice.integration.RequestResponseDefinition;
import hu.cargolink.backend.soforservice.transfer.core.FrameReader;
import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;

/**
 * "Kijelentkezes" keres-valasz part definial a sajat atviteli protokollban
 *
 * Created by Aron on 2014.08.24..
 */
public class DeauthRequestResponseDefinition extends RequestResponseDefinition<Void, Void> {
    @Override
    protected FrameReader<Void> messageReader() {
        return null;
    }

    @Override
    protected FrameWriter<Void> messageWriter(Void input) {
        return null;
    }
}
