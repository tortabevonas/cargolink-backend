package hu.cargolink.backend.soforservice.messages;

import hu.cargolink.backend.data.documents.Document;
import hu.cargolink.backend.data.documents.Page;
import hu.cargolink.backend.soforservice.FileResolver;
import hu.cargolink.backend.soforservice.common.MessageCodes;
import hu.cargolink.backend.soforservice.integration.RequestResponseDefinition;
import hu.cargolink.backend.soforservice.transfer.DocumentFrameReader;
import hu.cargolink.backend.soforservice.transfer.core.Frame;
import hu.cargolink.backend.soforservice.transfer.core.FrameFrameReader;
import hu.cargolink.backend.soforservice.transfer.core.FrameReader;
import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;
import hu.cargolink.backend.soforservice.transfer.general.JsonFrameReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * "Uj dokumentum hozzadasa egy munkahoz" keres-valasz part definial a sajat atviteli protokollban
 *
 * Created by Aron on 14. 10. 24..
 */
public class AddDocumentRequestResponseDefinition extends RequestResponseDefinition<Map<String, Object>, Void> {
    private final FileResolver<Page> pageFileResolver;

    public AddDocumentRequestResponseDefinition(FileResolver<Page> pageFileResolver) {
        this.pageFileResolver = pageFileResolver;
    }

    @Override
    protected FrameReader<Map<String, Object>> messageReader() {
        return new FrameReader<Map<String, Object>>() {
            @Override
            public Map<String, Object> read(InputStream inputStream, short action, int length) throws IOException {
                Map<String, Object> ret = new HashMap<String, Object>();
                List<Document> documents = new ArrayList<Document>();

                FrameFrameReader frameReader = new FrameFrameReader();
                frameReader.setReader(MessageCodes.ACTION_METADATA, new JsonFrameReader());
                frameReader.setReader(MessageCodes.ACTION_DOCUMENT, new DocumentFrameReader(pageFileResolver));

                List<Frame> frames = frameReader.read(inputStream, action, length);
                for (Frame frame : frames) {
                    if (frame.action == MessageCodes.ACTION_METADATA) {
                        ret = (Map<String, Object>) frame.payload;
                    } else if (frame.action == MessageCodes.ACTION_DOCUMENT) {
                        documents.add((Document) frame.payload);
                    }
                }

                ret.put("documents", documents);

                return ret;
            }
        };
    }

    @Override
    protected FrameWriter messageWriter(Void input) throws Exception {
        return null;
    }
}
