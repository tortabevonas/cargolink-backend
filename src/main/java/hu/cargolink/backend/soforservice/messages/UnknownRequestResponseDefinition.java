package hu.cargolink.backend.soforservice.messages;

import hu.cargolink.backend.soforservice.integration.RequestResponseDefinition;
import hu.cargolink.backend.soforservice.transfer.core.FrameReader;
import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;

/**
 * Ismeretlen keres-valasz part definial a sajat atviteli protokollban.
 * Nem csinal semmit, eldobja es null-lal valaszol.
 *
 * Created by Aron on 2014.08.25..
 */
public class UnknownRequestResponseDefinition extends RequestResponseDefinition {
    @Override
    protected FrameReader messageReader() {
        return null;
    }

    @Override
    protected FrameWriter messageWriter(Object input) throws Exception {
        return null;
    }
}
