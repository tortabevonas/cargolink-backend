package hu.cargolink.backend.soforservice.messages;

import hu.cargolink.backend.soforservice.common.MessageCodes;
import hu.cargolink.backend.soforservice.integration.RequestResponseDefinition;
import hu.cargolink.backend.soforservice.transfer.general.JsonFrameReader;
import hu.cargolink.backend.soforservice.transfer.general.StringFrameWriter;
import hu.cargolink.backend.soforservice.transfer.core.FrameReader;
import hu.cargolink.backend.soforservice.transfer.core.FrameWriter;

import java.util.Map;

/**
 * Bejelentkezesi keres-valasz part definial a sajat atviteli protokollban
 *
 * Created by Aron on 2014.08.24..
 */
public class LoginRequestResponseDefinition extends RequestResponseDefinition<Map<String, Object>, String> {
    @Override
    protected FrameReader<Map<String, Object>> messageReader() {
        return new JsonFrameReader();
    }

    @Override
    protected FrameWriter<String> messageWriter(String input) {
        return new StringFrameWriter(MessageCodes.RESPONSE_OK, input);
    }
}
