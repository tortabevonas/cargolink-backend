package hu.cargolink.backend.soforservice;

import java.io.File;

/**
 * Tetszoleges objektumhoz tartozo fajlt hataroz meg
 *
 * Created by Aron on 2014.08.28..
 */
public interface FileResolver<T> {
    public File fileFor(T object);
}
