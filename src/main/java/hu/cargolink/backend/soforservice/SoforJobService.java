package hu.cargolink.backend.soforservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gdata.util.ServiceException;
import hu.cargolink.backend.data.documents.Document;
import hu.cargolink.backend.data.exports.*;
import hu.cargolink.backend.data.imports.*;
import hu.cargolink.backend.soforservice.client.GeneralError;
import hu.cargolink.backend.soforservice.client.UnauthenticatedError;
import hu.cargolink.backend.soforservice.common.LatLng;
import hu.cargolink.backend.soforservice.integration.SoforSessionStore;
import hu.cargolink.backend.soforservice.messaging.SoforSession;
import hu.cargolink.backend.spreadsheets.sync.ExportSheetSyncRequestService;
import hu.cargolink.backend.spreadsheets.sync.ImportSheetSyncRequestService;
import hu.cargolink.backend.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.annotation.Header;
import org.springframework.integration.annotation.Payload;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;

/**
 * A sofor munkakkal kapcsolatos feladatait ellato szolgaltatas.
 * Az alkalmazaslogika nagy resze ebben a szolgaltatasban talalhato meg.
 * Lasd meg: SoforAuthService.
 *
 * Created by Aron on 2014.08.20..
 */
@Component
public class SoforJobService {
    @Autowired
    private ExportJobRepository exportJobRepository;

    @Autowired
    private ExportJobSpreadsheetRepository exportJobSpreadsheetRepository;

    @Autowired
    private ImportJobRepository importJobRepository;

    @Autowired
    private ImportJobSpreadsheetRepository importJobSpreadsheetRepository;

    @Autowired
    private ImportSheetSyncRequestService importSheetSyncRequestService;

    @Autowired
    private ExportSheetSyncRequestService exportSheetSyncRequestService;

    // Import munka állapotváltások:
    @Transactional
    @ServiceActivator(inputChannel = "toSoforService.importLkwFelvette", outputChannel = "toResponseTransformer", requiresReply = "true")
    public void importLkwFelvette(
            @Header(SoforSessionStore.HEADER) SoforSession session,
            @Payload("job") String jobUuid,
            @Payload("position.lat") double lat,
            @Payload("position.lng") double lng,
            @Payload("vamkezelesKesz") boolean vamkezelesKesz,
            @Payload("serules") String serules,
            @Payload("euPalHely") int euPalHely,
            @Payload("colli") int colli,
            @Payload("kg") BigDecimal kg,
            @Payload("cbm") BigDecimal cbm,
            @Payload("marker") String marker,
            @Payload("documents") List<Document> documents
    ) throws ServiceException, IOException, URISyntaxException

    {
        if (!session.isAuthenticated()) {
            throw new UnauthenticatedError();
        }

        synchronized (importSheetSyncRequestService) {
            ImportJob job = importJobRepository.findOne(jobUuid);
            if (job != null && session.getSofor().responsibleForNk(job)
                    && job.status == ImportJobStatus.KIADVA_FELVETELRE_IMP_LKWNAK) {

                Set<ImportJob> jobsOnJarat = importJobRepository.findByElofutasJarat(job.elofutasJarat);
                int euPalHelySum = 0;
                for (ImportJob jaratJob : jobsOnJarat) {
                    if (jaratJob.status.ordinal() == ImportJobStatus.IMP_LKW_FELVETTE.ordinal()) {
                        euPalHelySum += jaratJob.euPalettaHelyFelvetelkor;
                    }
                }
                euPalHelySum += euPalHely;

                boolean lkwFull = euPalHelySum >= ImportJob.MAX_EU_PAL_PER_JARAT;

                job.status = lkwFull ? ImportJobStatus.IMP_LKW_FULL : ImportJobStatus.IMP_LKW_FELVETTE;
                job.felvetelTenylDatuma = System.currentTimeMillis();
                job.felvetelPozicio = new LatLng(lat, lng);
                job.vamkezelesKesz = vamkezelesKesz;
                job.serulesFelvetelkor = serules;
                job.euPalettaHelyFelvetelkor = euPalHely;
                job.colliFelvetelkor = colli;
                job.kgFelvetelkor = kg;
                job.cbmFelvetelkor = cbm;

                job.documents.addAll(documents);
                if (Utils.stringValid(marker)) {
                    job.marker = marker;
                }

                importJobSpreadsheetRepository.save(job,
                        ImportJobEntityMapping.FIELD_STATUS,
                        ImportJobEntityMapping.FIELD_FELVETEL_TENYL_DATUMA,
                        ImportJobEntityMapping.FIELD_FELVETEL_POZICIO,
                        ImportJobEntityMapping.FIELD_VAMKEZELES_KESZ,
                        ImportJobEntityMapping.FIELD_SERULES_FELVETELKOR,
                        ImportJobEntityMapping.FIELD_SERULES_LEIRASA_FEVETELKOR,
                        ImportJobEntityMapping.FIELD_EU_PALETTA_HELY_FELVETELKOR,
                        ImportJobEntityMapping.FIELD_COLLI_FELVETELKOR,
                        ImportJobEntityMapping.FIELD_KG_FELVETELKOR,
                        ImportJobEntityMapping.FIELD_CBM_FELVETELKOR,
                        ImportJobEntityMapping.FIELD_FELVETT_MENNYISEG_EGYEZIK,
                        ImportJobEntityMapping.FIELD_MARKER,
                        ImportJobEntityMapping.FIELD_DOCUMENTS
                );
                if (lkwFull) {
                    for (ImportJob jaratJob : jobsOnJarat) {
                        if ((!jaratJob.getId().equals(job.getId())) && jaratJob.status == ImportJobStatus.IMP_LKW_FELVETTE) {
                            jaratJob.status = ImportJobStatus.IMP_LKW_FULL;
                            importJobSpreadsheetRepository.save(jaratJob, ImportJobEntityMapping.FIELD_STATUS);
                            importJobRepository.saveWithLastModified(jaratJob);
                        }
                    }
                }
                importJobRepository.saveWithLastModified(job);
            } else {
                System.out.println("importLkwFelvette: GeneralError!");
                System.out.println("Munka: " + (job != null ? new ObjectMapper().writeValueAsString(job) : "null"));
                System.out.println("Sofor: " + (session.getSofor() != null ? new ObjectMapper().writeValueAsString(session.getSofor()) : "null"));
                throw new GeneralError();
            }
        }
    }

    @Transactional
    @ServiceActivator(inputChannel = "toSoforService.importVamkezelesBefejez", outputChannel = "toResponseTransformer", requiresReply = "true")
    public void importVamkezelesBefejezese(
            @Header(SoforSessionStore.HEADER) SoforSession session,
            @Payload("job") String jobUuid,
            @Payload("documents") List<Document> documents
    ) throws ServiceException, IOException, URISyntaxException {
        if (!session.isAuthenticated()) {
            throw new UnauthenticatedError();
        }

        synchronized (importSheetSyncRequestService) {
            ImportJob job = importJobRepository.findOne(jobUuid);
            if (job != null && session.getSofor().responsibleForNk(job)
                    && job.vamkezelesreVar()) {
                job.vamkezelesKesz = true;

                job.documents.addAll(documents);

                importJobSpreadsheetRepository.save(job,
                        ImportJobEntityMapping.FIELD_VAMKEZELES_KESZ,
                        ImportJobEntityMapping.FIELD_DOCUMENTS
                );
                importJobRepository.saveWithLastModified(job);
            } else {
                throw new GeneralError();
            }
        }
    }

    @Transactional
    @ServiceActivator(inputChannel = "toSoforService.importRaktarbaBeerkezett", outputChannel = "toResponseTransformer", requiresReply = "true")
    public void importRaktarbaBeerkezett(
            @Header(SoforSessionStore.HEADER) SoforSession session,
            @Payload("job") String jobUuid
    ) throws ServiceException, IOException, URISyntaxException {
        if (!session.isAuthenticated()) {
            throw new UnauthenticatedError();
        }

        synchronized (importSheetSyncRequestService) {
            ImportJob job = importJobRepository.findOne(jobUuid);
            if (job != null && session.getSofor().responsibleForNk(job)
                    && (job.status == ImportJobStatus.IMP_LKW_FELVETTE || job.status == ImportJobStatus.IMP_LKW_FULL)) {
                job.status = ImportJobStatus.RAKTARBA_BEERKEZETT;
                job.betarDatum = System.currentTimeMillis();

                importJobSpreadsheetRepository.save(job,
                        ImportJobEntityMapping.FIELD_STATUS,
                        ImportJobEntityMapping.FIELD_BETAR_DATUM
                );
                importJobRepository.saveWithLastModified(job);
            } else {
                throw new GeneralError();
            }
        }
    }

    @Transactional
    @ServiceActivator(inputChannel = "toSoforService.importLerakvaCimzettnel", outputChannel = "toResponseTransformer", requiresReply = "true")
    public void importLerakvaCimzettnel(
            @Header(SoforSessionStore.HEADER) SoforSession session,
            @Payload("job") String jobUuid,
            @Payload("position.lat") double lat,
            @Payload("position.lng") double lng,
            @Payload("serules") String serules,
            @Payload("documents") List<Document> documents
    ) throws ServiceException, IOException, URISyntaxException {
        if (!session.isAuthenticated()) {
            throw new UnauthenticatedError();
        }

        synchronized (importSheetSyncRequestService) {
            ImportJob job = importJobRepository.findOne(jobUuid);
            if (job != null && session.getSofor().responsibleForBelf(job)
                    && job.status == ImportJobStatus.KIADVA_KISZALLITASRA_SAJAT_LKW) {
                job.status = ImportJobStatus.LERAKVA_CIMZETTNEL_V_KIADVA_MEGBIZONAK;
                job.lerakasTenylDatuma = System.currentTimeMillis();
                job.lerakasPozicio = new LatLng(lat, lng);
                job.serulesLerakaskor = serules;

            /*for (Document document : documents) {
                job.addDocument(document);
                documentRepository.save(document);
            }*/
                job.documents.addAll(documents);

                importJobSpreadsheetRepository.save(job,
                        ImportJobEntityMapping.FIELD_STATUS,
                        ImportJobEntityMapping.FIELD_LERAKAS_TENYL_DATUMA,
                        ImportJobEntityMapping.FIELD_LERAKAS_POZICIO,
                        ImportJobEntityMapping.FIELD_SERULES_LERAKASKOR,
                        ImportJobEntityMapping.FIELD_SERULES_LEIRASA_LERAKASKOR,
                        ImportJobEntityMapping.FIELD_DOCUMENTS
                );
                importJobRepository.saveWithLastModified(job);
            } else {
                throw new GeneralError();
            }
        }
    }

    //Export munka állapotváltások

    @Transactional
    @ServiceActivator(inputChannel = "toSoforService.exportLkwFelvette", outputChannel = "toResponseTransformer", requiresReply = "true")
    public void exportLkwFelvette(
            @Header(SoforSessionStore.HEADER) SoforSession session,
            @Payload("job") String jobUuid,
            @Payload("position.lat") double lat,
            @Payload("position.lng") double lng,
            @Payload("euPalHely") int euPalHely,
            @Payload("colli") int colli,
            @Payload("kg") BigDecimal kg,
            @Payload("cbm") BigDecimal cbm,
            @Payload("serules") String serules,
            @Payload("marker") String marker,
            @Payload("documents") List<Document> documents
    ) throws ServiceException, IOException, URISyntaxException {
        if (!session.isAuthenticated()) {
            throw new UnauthenticatedError();
        }

        synchronized (exportSheetSyncRequestService) {
            ExportJob job = exportJobRepository.findOne(jobUuid);
            if (job != null && session.getSofor().responsibleForBelf(job)
                    && job.status == ExportJobStatus.KIADVA_BESSZALLITASRA_SAJAT_LKW) {
                job.status = ExportJobStatus.BESZALLITASRA_FELVEVE;
                job.felvetelPozicio = new LatLng(lat, lng);
                job.felvetelTenylDatuma = System.currentTimeMillis();
                job.euPalettaHelyFelvetelkor = euPalHely;
                job.colliFelvetelkor = colli;
                job.kgFelvetelkor = kg;
                job.cbmFelvetelkor = cbm;
                job.serulesFelvetelkor = serules;
            /*for (Document document : documents) {
                job.addDocument(document);
                documentRepository.save(document);
            }*/
                job.documents.addAll(documents);
                if (Utils.stringValid(marker)) {
                    job.marker = marker;
                }

                exportJobSpreadsheetRepository.save(job,
                        ExportJobEntityMapping.FIELD_STATUS,
                        ExportJobEntityMapping.FIELD_FELVETEL_POZICIO,
                        ExportJobEntityMapping.FIELD_FELVETEL_TENYL_DATUMA,
                        ExportJobEntityMapping.FIELD_FELVETT_MENNYISEG_EGYEZIK,
                        ExportJobEntityMapping.FIELD_EU_PAL_HELY_FELVETELKOR,
                        ExportJobEntityMapping.FIELD_COLLI_FELVETELKOR,
                        ExportJobEntityMapping.FIELD_KG_FELVETELKOR,
                        ExportJobEntityMapping.FIELD_CBM_FELVETELKOR,
                        ExportJobEntityMapping.FIELD_SERULES_FELVETELKOR,
                        ExportJobEntityMapping.FIELD_SERULES_LEIRASA_FELVETELKOR,
                        ExportJobEntityMapping.FIELD_MARKER,
                        ExportJobEntityMapping.FIELD_DOCUMENTS
                );
                exportJobRepository.saveWithLastModified(job);
            } else {
                throw new GeneralError();
            }
        }
    }

    @Transactional
    @ServiceActivator(inputChannel = "toSoforService.exportRaktarbaBeerkezett", outputChannel = "toResponseTransformer", requiresReply = "true")
    public void exportRaktarbaBeerkezett(
            @Header(SoforSessionStore.HEADER) SoforSession session,
            @Payload("job") String jobUuid
    ) throws ServiceException, IOException, URISyntaxException {
        if (!session.isAuthenticated()) {
            throw new UnauthenticatedError();
        }

        synchronized (exportSheetSyncRequestService) {
            ExportJob job = exportJobRepository.findOne(jobUuid);
            if (job != null && session.getSofor().responsibleForBelf(job)
                    && job.status == ExportJobStatus.BESZALLITASRA_FELVEVE) {
                job.status = ExportJobStatus.RAKTARBA_LEADVA;

                exportJobSpreadsheetRepository.save(job,
                        ExportJobEntityMapping.FIELD_STATUS
                );
                exportJobRepository.saveWithLastModified(job);
            } else {
                throw new GeneralError();
            }
        }
    }

    //TO/DO: Ezt kihajítani alkalomadtán
    /*@Transactional
    @ServiceActivator(inputChannel = "toSoforService.exportFelrakvaPotkocsira", outputChannel = "toResponseTransformer", requiresReply = "true")
    public void exportFelrakvaPotkocsira(
            @Header(SoforSessionStore.HEADER) SoforSession session,
            @Payload("job") String jobUuid
    ) throws ServiceException, IOException, URISyntaxException {
        if (!session.isAuthenticated()) {
            throw new UnauthenticatedError();
        }

        ExportJob job = exportJobRepository.findOne(jobUuid);
        if (job != null && session.getSofor().responsibleForNk(job)
                && job.status == ExportJobStatus.EXPORT_NK_SZALLITAS_ALATT) {
            job.status = ExportJobStatus.FELRAKVA_POTKOCSIRA_LKWRA;

            exportJobSpreadsheetRepository.save(job,
                    ExportJobEntityMapping.FIELD_STATUS
            );
            exportJobRepository.saveWithLastModified(job);
        } else {
            throw new GeneralError();
        }
    }*/

    @Transactional
    @ServiceActivator(inputChannel = "toSoforService.exportLerakvaCimzettnel", outputChannel = "toResponseTransformer", requiresReply = "true")
    public void exportLerakvaCimzettnel(
            @Header(SoforSessionStore.HEADER) SoforSession session,
            @Payload("job") String jobUuid,
            @Payload("position.lat") double lat,
            @Payload("position.lng") double lng,
            @Payload("serules") String serules,
            @Payload("documents") List<Document> documents
    ) throws ServiceException, IOException, URISyntaxException {
        if (!session.isAuthenticated()) {
            throw new UnauthenticatedError();
        }

        synchronized (exportSheetSyncRequestService) {
            ExportJob job = exportJobRepository.findOne(jobUuid);
            if (job != null && session.getSofor().responsibleForNk(job)
                    && job.status == ExportJobStatus.EXPORT_NK_SZALLITAS_ALATT) {
                job.status = ExportJobStatus.LERAKVA_CIMZETTNEL;
                job.lerakasTenylDatuma = System.currentTimeMillis();
                job.lerakasPozicio = new LatLng(lat, lng);
                job.serulesLerakaskor = serules;

            /*for (Document document : documents) {
                job.addDocument(document);
                documentRepository.save(document);
            }*/
                job.documents.addAll(documents);

                exportJobSpreadsheetRepository.save(job,
                        ExportJobEntityMapping.FIELD_STATUS,
                        ExportJobEntityMapping.FIELD_LERAKAS_TENYL_DATUMA,
                        ExportJobEntityMapping.FIELD_LERAKAS_POZICIO,
                        ExportJobEntityMapping.FIELD_SERULES_LERAKASKOR,
                        ExportJobEntityMapping.FIELD_SERULES_LEIRASA_LERAKASKOR,
                        ExportJobEntityMapping.FIELD_DOCUMENTS
                );
                exportJobRepository.saveWithLastModified(job);
            } else {
                throw new GeneralError();
            }
        }
    }

    @Transactional
    @ServiceActivator(inputChannel = "toSoforService.setPositionForJob", outputChannel = "toResponseTransformer", requiresReply = "true")
    public void setPositionForJob(
            @Header(SoforSessionStore.HEADER) SoforSession session,
            @Payload("job") String jobUuid,
            @Payload("felveteli") boolean felveteli,
            @Payload("position.lat") double lat,
            @Payload("position.lng") double lng
    ) throws ServiceException, IOException, URISyntaxException {
        if (!session.isAuthenticated()) {
            throw new UnauthenticatedError();
        }


        synchronized (exportSheetSyncRequestService) {
            synchronized (importSheetSyncRequestService) {
                LatLng position = new LatLng(lat, lng);

                ImportJob importJob = importJobRepository.findOne(jobUuid);
                ExportJob exportJob = exportJobRepository.findOne(jobUuid);

                if (importJob != null) {
                    if (felveteli) {
                        importJob.felvetelPozicio = position;
                    } else {
                        importJob.lerakasPozicio = position;
                    }
                    importJobSpreadsheetRepository.save(importJob,
                            felveteli ?
                                    ImportJobEntityMapping.FIELD_FELVETEL_POZICIO :
                                    ImportJobEntityMapping.FIELD_LERAKAS_POZICIO
                    );
                    importJobRepository.saveWithLastModified(importJob);
                } else if (exportJob != null) {
                    if (felveteli) {
                        exportJob.felvetelPozicio = position;
                    } else {
                        exportJob.lerakasPozicio = position;
                    }
                    exportJobSpreadsheetRepository.save(exportJob,
                            felveteli ?
                                    ExportJobEntityMapping.FIELD_FELVETEL_POZICIO :
                                    ExportJobEntityMapping.FIELD_LERAKAS_POZICIO
                    );
                    exportJobRepository.saveWithLastModified(exportJob);
                } else {
                    throw new GeneralError();
                }
            }
        }
    }
}
