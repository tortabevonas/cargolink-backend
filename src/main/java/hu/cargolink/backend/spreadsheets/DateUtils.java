package hu.cargolink.backend.spreadsheets;

import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;
import org.joda.time.Weeks;

/**
 * Dátumokkal való munka megkönnyítését segítő függvények.
 *
 * Created by aronlorincz on 14. 12. 02..
 */
public class DateUtils {
    private static DateTime worksheetEpoch() {
        return new DateTime(2000, 1, 3, 0, 1);
    }

    public static int currentWeek() {
        DateTime now = DateTime.now();//new DateTime(2014, 12, 8, 12, 16);
        Weeks weeks = Weeks.weeksBetween(worksheetEpoch(), now);
        int currentWeek = weeks.getWeeks();

        return currentWeek;
    }

    public static int weekFromWorksheetName(String worksheetName) {
        try {
            String digits = filterDigits(worksheetName);
            String yearName = digits.substring(0, 4);
            String weekName = digits.substring(4, 4 + Math.min(2, digits.length() - 4));

            int year = Integer.valueOf(yearName);
            int week = Integer.valueOf(weekName);

            MutableDateTime md = new MutableDateTime(0);
            md.setWeekyear(year);
            md.setWeekOfWeekyear(week);
            Weeks weeks = Weeks.weeksBetween(worksheetEpoch(), md);
            int worksheetWeek = weeks.getWeeks();

            return worksheetWeek;
        } catch (NumberFormatException e) {
            return -1;
        } catch (IndexOutOfBoundsException e) {
            return -1;
        }
    }

    private static String filterDigits(String text) {
        StringBuilder sb = new StringBuilder();
        for (char c : text.toCharArray()) {
            if (Character.isDigit(c)) {
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
