package hu.cargolink.backend.spreadsheets;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.util.ServiceException;
import hu.cargolink.backend.spreadsheets.core.SpreadsheetIntegration;
import hu.cargolink.backend.spreadsheets.core.SyncRange;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

/**
 * A Google Spreadsheets API kapcsolódási pontja.
 * Ez a szolgáltatás állítja be a kapcsolódáshoz szükséges adatokat és inicializálja a klienskönyvtárat.
 *
 * Created by Aron on 2014.08.19..
 */
public class SpreadsheetIntegrationService {
    @Value("${google.serviceAccountId}")
    String serviceAccountId;

    @Value("${google.p12certPath}")
    String p12certPath;

    @Value("${spreadsheet.importsheet}")
    String importSheet;

    @Value("${spreadsheet.exportsheet}")
    String exportSheet;

    private SpreadsheetService spreadsheetService;
    private SpreadsheetIntegration exportSheetIntegration;
    private SpreadsheetIntegration importSheetIntegration;
    private SpreadsheetIntegration soforSheetIntegration;

    @PostConstruct
    public void init() throws ServiceException, IOException, GeneralSecurityException {
        spreadsheetService = new SpreadsheetService("cargolink");
        //spreadsheetService.setUserCredentials(username, password);
        GoogleCredential credential = new GoogleCredential.Builder()
                .setTransport(GoogleNetHttpTransport.newTrustedTransport())
                .setJsonFactory(JacksonFactory.getDefaultInstance())
                .setServiceAccountId(serviceAccountId)
                .setServiceAccountScopes(Arrays.asList("https://www.googleapis.com/auth/drive", "https://spreadsheets.google.com/feeds", "https://www.googleapis.com/auth/spreadsheets" /*,"https://spreadsheets.google.com/feeds/spreadsheets/private/full"*/))
                .setServiceAccountPrivateKeyFromP12File(new File(p12certPath))
                .setServiceAccountUser("arpad.kaman@cargolink.hu")
                .build();
        spreadsheetService.setOAuth2Credentials(credential);
        URL SPREADSHEET_FEED_URL = new URL("https://spreadsheets.google.com/feeds/spreadsheets/private/full");

        SpreadsheetFeed feed = spreadsheetService.getFeed(SPREADSHEET_FEED_URL, SpreadsheetFeed.class);
        List<SpreadsheetEntry> spreadsheets = feed.getEntries();

        for (SpreadsheetEntry spreadsheet : spreadsheets) {
            String title = spreadsheet.getTitle().getPlainText();
            if (title.equals(importSheet)) {
                importSheetIntegration = new SpreadsheetIntegration(spreadsheet, new SyncRange(5, SyncRange.MAX, 1, 67));
            } else if (title.equals(exportSheet)) {
                exportSheetIntegration = new SpreadsheetIntegration(spreadsheet, new SyncRange(5, SyncRange.MAX, 1, 78));
                soforSheetIntegration = new SpreadsheetIntegration(spreadsheet, new SyncRange(2, SyncRange.MAX, 3, 3));
            }
        }

        if (exportSheetIntegration == null) {
            throw new IllegalStateException("Export és sofőr táblázat nem található a megadott néven! Talán nincs megosztva a felhasználóval?");
        }

        if (importSheetIntegration == null) {
            throw new IllegalStateException("Import táblázat nem található a megadott néven! Talán nincs megosztva a felhasználóval?");
        }
    }

    public SpreadsheetService getSpreadsheetService() {
        return spreadsheetService;
    }

    public SpreadsheetIntegration getExportSheetIntegration() {
        return exportSheetIntegration;
    }

    public SpreadsheetIntegration getImportSheetIntegration() {
        return importSheetIntegration;
    }

    public SpreadsheetIntegration getSoforSheetIntegration() {
        return soforSheetIntegration;
    }
}
