package hu.cargolink.backend.spreadsheets.core;

/**
 * Google táblázat cellát időpontnak feleltet meg, és fordítva.
 *
 * Created by Aron on 2014.08.18..
 */
public abstract class DefaultTimeFieldMapping<T> extends StringFieldMapping<T> {
    @Override
    public final void read(T object, String value) {
        readTime(object, DefaultDateFormat.parsePrecise(value));
    }

    @Override
    public final String write(T object) {
        return DefaultDateFormat.formatPrecise(writeTime(object));
    }

    public abstract void readTime(T object, long date);

    public abstract long writeTime(T object);
}
