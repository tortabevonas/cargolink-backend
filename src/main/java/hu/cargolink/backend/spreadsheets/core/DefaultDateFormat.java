package hu.cargolink.backend.spreadsheets.core;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A táblázatban használt dátum- és időformátumokkal való műveletvégzést egyszerűsíti le
 *
 * Created by Aron on 2014.08.18..
 */
public class DefaultDateFormat {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd");
    private static final SimpleDateFormat PRECISE_DATE_FORMAT = new SimpleDateFormat("yyyy.MM.dd kk:mm:ss");

    public static long parse(String date) {
        if (date == null) {
            return 0;
        }
        try {
            return DATE_FORMAT.parse(date).getTime();
        } catch (ParseException e) {
            return 0;
        }
    }

    public static String format(long time) {
        return DATE_FORMAT.format(new Date(time));
    }

    public static long parsePrecise(String date) {
        if (date == null) {
            return 0;
        }
        try {
            return PRECISE_DATE_FORMAT.parse(date).getTime();
        } catch (ParseException e) {
            try {
                return DATE_FORMAT.parse(date).getTime();
            } catch (ParseException e1) {
            }
            return 0;
        }
    }

    public static String formatPrecise(long time) {
        return PRECISE_DATE_FORMAT.format(new Date(time));
    }
}
