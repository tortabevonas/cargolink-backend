package hu.cargolink.backend.spreadsheets.core;

import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.ServiceException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * T típusú Java objektum és egy Google táblasor kapcsolatát definiálja.
 * Alosztályai összepárosítják a cellákat az objektum mezőivel.
 *
 * Created by Aron on 2014.08.18..
 */
public abstract class EntityMapping<T> {
    private final Map<String, FieldMapping<T>> fieldsByColumn = new HashMap<String, FieldMapping<T>>();
    private final Map<String, FieldMapping<T>> fieldsByName = new HashMap<String, FieldMapping<T>>();
    private final Map<FieldMapping<T>, String> columnsByField = new HashMap<FieldMapping<T>, String>();
    private final List<FieldMapping<T>> fields = new ArrayList<FieldMapping<T>>();

    protected void addField(FieldMapping<T> fieldMapping, String column, String name) {
        fields.add(fieldMapping);
        fieldsByColumn.put(column, fieldMapping);
        columnsByField.put(fieldMapping, column);
        fieldsByName.put(name, fieldMapping);
    }

    public FieldMapping<T> fieldForColumn(String column) {
        return fieldsByColumn.get(column);
    }

    public FieldMapping<T> fieldForName(String name) {
        return fieldsByName.get(name);
    }

    public String columnForField(FieldMapping<T> field) {
        return columnsByField.get(field);
    }

    public List<FieldMapping<T>> getFields() {
        return fields;
    }

    public String[] getFieldNames() {
        String[] fieldNames = new String[fieldsByName.keySet().size()];
        int index = 0;
        for (String fieldName : fieldsByName.keySet()) {
            fieldNames[index++] = fieldName;
        }
        return fieldNames;
    }

    public void readWorksheetInfo(T entity, WorksheetEntry worksheet) {
    }

    protected abstract T createNewEntity();

    public final T read(T entity, WorksheetEntry worksheet, CellRow row) {
        for (String column : row.keySet()) {
            CellEntry cell = row.get(column);
            FieldMapping<T> fieldMapping = fieldForColumn(column);
            if (fieldMapping != null) {
                fieldMapping.read(entity, cell);
            }
        }
        readWorksheetInfo(entity, worksheet);
        forceReadIdColumn(entity, row);
        return entity;
    }

    /**
     * Igaz, ha ez az EntityMapping tudja olvasni ezt a munkalapot. Kulonben hamisssal ter vissza.
     *
     * @param worksheet
     * @return
     */
    protected abstract boolean acceptWorksheet(WorksheetEntry worksheet);

    public final T read(WorksheetEntry worksheet, CellRow row) {
        return read(createNewEntity(), worksheet, row);
    }

    public final FieldMapping<T> fieldForColumn(int column) {
        String columnName = CellRow.column(column);
        return fieldForColumn(columnName);
    }

    public final T read(T entity, WorksheetEntry worksheet, CellRow row, int minColumn, int numColumns) {
        for (int columnIndex = minColumn; columnIndex < minColumn + numColumns; columnIndex++) {
            FieldMapping<T> fieldMapping = fieldForColumn(columnIndex);
            if (fieldMapping != null) {
                fieldMapping.read(entity, row.get(columnIndex));
            }
        }
        readWorksheetInfo(entity, worksheet);
        forceReadIdColumn(entity, row);
        return entity;
    }

    /**
     * Kulon beolvassa az ID oszlopot (barmi legyen is az), nehogy friss ID-ju entitasok keruljenek az adatbazisba az ures sorok helyett.
     * @param entity
     * @param row
     */
    private void forceReadIdColumn(T entity, CellRow row) {
        FieldMapping<T> idFieldMapping = idField();
        CellEntry cell = row.get(CellRow.columnIndex(columnForField(idFieldMapping)));
        idFieldMapping.read(entity, cell);
    }

    public abstract FieldMapping<T> idField();

    public final void write(T entity, CellRow row, String... fieldNames) throws IOException, ServiceException {
        for (String fieldName : fieldNames) {
            FieldMapping<T> fieldMapping = fieldForName(fieldName);
            if (fieldMapping != null) {
                String newInputValue = fieldMapping.write(entity);
                CellEntry cell = row.get(columnForField(fieldMapping));
                cell.changeInputValueLocal(newInputValue);
                cell.update();
            }
        }
    }
}