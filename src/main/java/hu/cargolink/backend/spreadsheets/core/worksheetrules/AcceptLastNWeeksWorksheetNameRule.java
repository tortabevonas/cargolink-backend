package hu.cargolink.backend.spreadsheets.core.worksheetrules;

import hu.cargolink.backend.spreadsheets.DateUtils;

/**
 * Meghatarozott szamu heti munkalapot valaszt ki a mai datumtol visszamenoleg.
 * A munkalap hetenek a szama a nevebol kovetkeztetheto ki (Lasd: DateUtils osztaly).
 *
 * Created by aronlorincz on 14. 12. 02..
 */
public class AcceptLastNWeeksWorksheetNameRule implements AcceptWorksheetNameRule {
    private final int weeksBack;

    public AcceptLastNWeeksWorksheetNameRule(int weeksBack) {
        this.weeksBack = weeksBack;
    }

    @Override
    public boolean acceptWorksheet(String name) {
        boolean accept = DateUtils.weekFromWorksheetName(name) > (DateUtils.currentWeek() - weeksBack);
        return accept;
    }
}
