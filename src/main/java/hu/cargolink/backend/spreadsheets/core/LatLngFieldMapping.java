package hu.cargolink.backend.spreadsheets.core;

import hu.cargolink.backend.soforservice.common.LatLng;

/**
 * Földrajzi koordinátákat (LatLng típusú mezőt) feleltet meg egy táblázatcella értékének, és fordítva.
 *
 * Created by Aron on 2014.08.23..
 */
public abstract class LatLngFieldMapping<T> extends StringFieldMapping<T> {
    @Override
    public void read(T object, String value) {
        readLatLng(object, LatLng.fromString(value));
    }

    @Override
    public String write(T object) {
        LatLng latLng = writeLatLng(object);
        if (latLng == null || latLng.isZero()) {
            return "";
        }
        return latLng.toString();
    }

    public abstract void readLatLng(T object, LatLng latLng);

    public abstract LatLng writeLatLng(T object);
}
