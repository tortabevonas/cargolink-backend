package hu.cargolink.backend.spreadsheets.core;

/**
 * Java enumerációt feleltet meg egy Google táblázat cellának, és fordítva.
 *
 * Created by Aron on 2014.08.18..
 */
public abstract class EnumFieldMapping<T, EnumT> extends StringFieldMapping<T> {
    @Override
    public final void read(T object, String value) {
        EnumT readValue = null;
        if (value != null) {
            for (int i = 0; i < names().length; i++) {
                String name = names()[i];
                if (name.trim().toLowerCase().equals(value.trim().toLowerCase())) {
                    readValue = values()[i];
                    break;
                }
            }
        }
        readEnum(object, readValue);
    }

    @Override
    public final String write(T object) {
        EnumT writtenValue = writeEnum(object);
        for (int i = 0; i < values().length; i++) {
            EnumT value = values()[i];
            if (value == writtenValue) {
                return names()[i];
            }
        }
        return null;
    }

    public abstract void readEnum(T object, EnumT value);

    public abstract EnumT writeEnum(T object);

    public abstract String[] names();

    public abstract EnumT[] values();
}
