package hu.cargolink.backend.spreadsheets.core.worksheetrules;

/**
 * Táblázat írása/olvasása során kiválasztja a célmunkalapot vagy célmunkalapokat
 *
 * Created by aronlorincz on 14. 12. 02..
 */
public interface AcceptWorksheetNameRule {
    public boolean acceptWorksheet(String name);
}
