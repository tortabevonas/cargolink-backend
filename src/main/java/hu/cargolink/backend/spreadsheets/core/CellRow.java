package hu.cargolink.backend.spreadsheets.core;

import com.google.gdata.client.spreadsheet.CellQuery;
import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.CellFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.ServiceException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

/**
 * Egy Google táblázat egy munkalapjának egy sorát reprezentálja.
 * Az oszlop betűjele vagy sorzáma szerinti keresést tesz lehetővé.
 *
 * Created by Aron on 2014.08.18..
 */
public class CellRow extends HashMap<String, CellEntry> {
    public CellRow(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    private final int rowIndex;

    public CellRow(CellFeed feed, int rowIndex) {
        this.rowIndex = rowIndex;
        for (CellEntry cell : feed.getEntries()) {
            int row = row(cell.getTitle().getPlainText());
            if (row == rowIndex) {
                String col = column(cell.getTitle().getPlainText());
                put(col, cell);
            }
        }
    }

    public CellEntry get(int columnIndex) {
        return get(column(columnIndex));
    }

    public static Map<Integer, CellRow> fromFeed(CellFeed feed) {
        Map<Integer, CellRow> result = new HashMap<Integer, CellRow>();
        for (CellEntry cellEntry : feed.getEntries()) {
            String cellTitle = cellEntry.getTitle().getPlainText();
            int rowIndex = row(cellTitle);

            CellRow row = result.get(rowIndex);
            if (row == null) {
                row = new CellRow(feed, rowIndex);
                result.put(rowIndex, row);
            }
        }
        return result;
    }

    public static Map<Integer, CellRow> fromSpreadsheet(SpreadsheetService service, WorksheetEntry worksheet, SyncRange syncRange) throws ServiceException, IOException, URISyntaxException {
        return fromSpreadsheet(service, worksheet, syncRange.row, syncRange.numRows, syncRange.column, syncRange.numColumns, false);
    }

    public static Map<Integer, CellRow> fromSpreadsheet(SpreadsheetService service, WorksheetEntry worksheet, SyncRange syncRange, boolean returnEmpty) throws ServiceException, IOException, URISyntaxException {
        return fromSpreadsheet(service, worksheet, syncRange.row, syncRange.numRows, syncRange.column, syncRange.numColumns, returnEmpty);
    }

    public static Map<Integer, CellRow> fromSpreadsheet(SpreadsheetService service, WorksheetEntry worksheet, int row, int numRows, int column, int numColumns) throws ServiceException, IOException, URISyntaxException {
        return fromSpreadsheet(service, worksheet, row, numRows, column, numColumns, false);
    }

    public static Map<Integer, CellRow> fromSpreadsheet(SpreadsheetService service, WorksheetEntry worksheet, int row, int numRows, int column, int numColumns, boolean returnEmpty) throws URISyntaxException, IOException, ServiceException {
        numRows = numRows > 0 ? numRows : worksheet.getRowCount() - row + 1;
        numColumns = numColumns > 0 ? numColumns : worksheet.getColCount() - column + 1;
        CellFeed cellFeed = null;//service.getFeed(new URI(worksheet.getCellFeedUrl().toString() + "?min-row=" + row + "&max-row=" + (row + numRows - 1) + "&min-col=" + column + "&max-col=" + (column + numColumns - 1) + "&return-empty" + (returnEmpty ? "true" : "false")).toURL(), CellFeed.class);
        CellQuery cellQuery = new CellQuery(worksheet.getCellFeedUrl());
        cellQuery.setMinimumRow(row);
        cellQuery.setMaximumRow(row + numRows - 1);
        cellQuery.setMinimumCol(column);
        cellQuery.setMaximumCol(column + numColumns - 1);
        cellQuery.setReturnEmpty(returnEmpty);
        cellFeed = service.getFeed(cellQuery, CellFeed.class);
        return fromFeed(cellFeed);
    }

    public static String column(String cellTitle) {
        String ret = "";
        for (char c : cellTitle.toCharArray()) {
            if (Character.isLetter(c)) {
                ret += c;
            }
        }
        return ret;
    }

    private static final String LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public static String column(int columnIndex) {
        String result = "";
        while (columnIndex > 0) {
            columnIndex--; // 1 => a, not 0 => a
            int remainder = columnIndex % 26;
            char digit = (char) (remainder + 97);
            result = digit + result;
            columnIndex = (columnIndex - remainder) / 26;
        }
        return result.toUpperCase();
    }

    public static int columnIndex(String columnName) {
        for (int i = 1; i < 1000; i++) {
            if (column(i).equals(columnName)) {
                return i;
            }
        }
        return 0;
    }

    public static int row(String cellTitle) {
        String ret = "";
        for (char c : cellTitle.toCharArray()) {
            if (Character.isDigit(c)) {
                ret += c;
            }
        }
        return Integer.valueOf(ret);
    }

    public int getRowIndex() {
        return rowIndex;
    }


}
