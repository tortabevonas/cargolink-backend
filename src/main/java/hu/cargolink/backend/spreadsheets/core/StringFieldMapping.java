package hu.cargolink.backend.spreadsheets.core;

import com.google.gdata.data.spreadsheet.CellEntry;

/**
 * Egy táblázatcella értékét String típusú mezővé felelteti meg, és fordítva.
 *
 * Created by Aron on 2014.08.18..
 */
public abstract class StringFieldMapping<ObjT> extends FieldMapping<ObjT> {
    @Override
    public final void read(ObjT object, CellEntry cell) {
        String value = null;
        if (cell != null) {
            value = cell.getCell().getValue();
        }
        read(object, value);
    }

    public abstract void read(ObjT object, String value);
}
