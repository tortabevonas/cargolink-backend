package hu.cargolink.backend.spreadsheets.core;

import com.google.gdata.data.spreadsheet.CellEntry;

import java.math.BigDecimal;

/**
 * Egy Google táblázat cellát BigDecimal objektumnak feleltet meg, vagy fordítva.
 *
 * Created by Aron on 2014.08.18..
 */
public abstract class DecimalFieldMapping<ObjT> extends FieldMapping<ObjT> {
    @Override
    public final void read(ObjT object, CellEntry cell) {
        BigDecimal value = BigDecimal.ZERO;
        if (cell != null) {
            try {
                value = new BigDecimal(cell.getCell().getNumericValue().toString());
            } catch (Exception e) {
            }
        }
        readDecimal(object, value);
    }

    @Override
    public final String write(ObjT object) {
        return ("" + writeDecimal(object)).replace(".", ",");
    }

    public abstract void readDecimal(ObjT object, BigDecimal value);

    public abstract BigDecimal writeDecimal(ObjT object);
}
