package hu.cargolink.backend.spreadsheets.core;

/**
 * Cellatartományt ír le egy Google táblázatban.
 *
 * Created by Aron on 2014.08.20..
 */
public class SyncRange {
    public SyncRange() {
    }

    public SyncRange(int row, int numRows, int column, int numColumns) {
        this.row = row;
        this.numRows = numRows;
        this.column = column;
        this.numColumns = numColumns;
    }

    public int row = 1;
    public int numRows = MAX;
    public int column = 1;
    public int numColumns = MAX;

    public static final int MAX = 0;
}
