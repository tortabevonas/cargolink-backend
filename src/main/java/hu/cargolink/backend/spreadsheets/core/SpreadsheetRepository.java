package hu.cargolink.backend.spreadsheets.core;

import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.ServiceException;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

/**
 * Egy Google Táblázatot mint adatbázist személyesít meg, amelybe objektumok írhatók és amelyből objektumok olvashatók.
 *
 * Created by Aron on 2014.08.21..
 */
public abstract class SpreadsheetRepository<T> {
    private final SpreadsheetService service;
    private final SpreadsheetEntry spreadsheet;
    private final EntityMapping<T> entityMapping;
    private final SyncRange syncRange;

    public SpreadsheetRepository(SpreadsheetService service, SpreadsheetEntry spreadsheet, EntityMapping<T> entityMapping, SyncRange syncRange) {
        this.service = service;
        this.spreadsheet = spreadsheet;
        this.entityMapping = entityMapping;
        this.syncRange = syncRange;
    }

    public final T save(T entity, String... fieldNames) throws IOException, ServiceException, URISyntaxException {
        List<WorksheetEntry> worksheets = spreadsheet.getWorksheets();
        Object spreadsheetIdentifierInfo = getSpreadsheetIdentifierInfo(entity);
        for (WorksheetEntry worksheet : worksheets) {
            if (acceptWorksheet(worksheet, spreadsheetIdentifierInfo)) {
                Map<Integer, CellRow> rows = CellRow.fromSpreadsheet(service, worksheet, syncRange, true);
                for (CellRow row : rows.values()) {
                    T rowEntity = entityMapping.read(worksheet, row);
                    if (acceptRow(rowEntity, entity)) {
                        entityMapping.write(entity, row, fieldNames);
                        break;
                    }
                }
                break;
            }
        }
        return entity;
    }

    public final T save(T entity) throws ServiceException, IOException, URISyntaxException {
        save(entity, entityMapping.getFieldNames());
        return entity;
    }

    protected abstract Object getSpreadsheetIdentifierInfo(T entity);

    protected abstract boolean acceptWorksheet(WorksheetEntry worksheet, Object spreadsheetIdentifierInfo);

    protected abstract boolean acceptRow(T rowEntity, T entityToSave);
}
