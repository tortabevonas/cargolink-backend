package hu.cargolink.backend.spreadsheets.core.worksheetrules;

/**
 * Minden munkalapot kivalaszt.
 *
 * Created by aronlorincz on 14. 12. 02..
 */
public class AcceptAllWorksheetNamesRule implements AcceptWorksheetNameRule {
    @Override
    public boolean acceptWorksheet(String name) {
        return true;
    }
}
