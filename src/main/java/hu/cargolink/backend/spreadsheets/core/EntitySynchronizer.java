package hu.cargolink.backend.spreadsheets.core;

import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.util.ServiceException;
import hu.cargolink.backend.spreadsheets.core.worksheetrules.AcceptWorksheetNameRule;
import hu.cargolink.backend.utils.Utils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

/**
 * T típusú objektumokat olvas be Google táblázat megadott tartományából.
 * Mindig csak a megadott tartománynak megfelelő mezőket frissíti.
 * Az objektumokat a táblázat UUID oszlopa alapján azonosítja.
 *
 * Ez az osztaly vegzi a teljes beolvasast (amikor az egész táblázatot beolvassuk az adatbázisba),
 * és a szűktartományú beolvasásokat is (amikor a táblázatban található szkript szól, hogy egy adott tartomány megváltozott).
 *
 * Hogy hogyan keressük ki az adatbázisból az onjektumokat és hogyan tároljuk el őket, azt az alosztályok definiálják.
 *
 * Created by Aron on 2014.08.20..
 */
public abstract class EntitySynchronizer<T> {
    private SyncRange maxSyncRange;
    private EntityMapping<T> entityMapping;

    public EntitySynchronizer(SyncRange maxSyncRange, EntityMapping<T> entityMapping) {
        this.maxSyncRange = maxSyncRange;
        this.entityMapping = entityMapping;
    }

    public synchronized void syncEntities(SpreadsheetService service, SpreadsheetEntry spreadsheet, AcceptWorksheetNameRule acceptWorksheetRule, SyncRange syncRange) throws IOException, ServiceException, URISyntaxException {
        List<WorksheetEntry> worksheets = spreadsheet.getWorksheets();
        for (WorksheetEntry worksheet : worksheets) {
            if (entityMapping.acceptWorksheet(worksheet) && acceptWorksheetRule.acceptWorksheet(worksheet.getTitle().getPlainText())) {
                int numRows = syncRange.numRows > 0 ? syncRange.numRows : worksheet.getRowCount() - syncRange.row + 1;
                int numColumns = syncRange.numColumns > 0 ? syncRange.numColumns : worksheet.getColCount() - syncRange.column + 1;

                Map<Integer, CellRow> rows = CellRow.fromSpreadsheet(service, worksheet, maxSyncRange);

                boolean batchMethod = numRows > 5;
                if (batchMethod) {
                    // Kotegelt modszer, amikor minden adatot ujraolvasunk

                    List<T> entitiesOnWorksheet = new ArrayList<T>();
                    entitiesOnWorksheet.addAll(allEntitiesOnWorksheet(worksheet));

                    // Cellák beolvasása
                    for (int rowIndex = syncRange.row; rowIndex < syncRange.row + numRows; rowIndex++) {
                        CellRow row = rows.get(rowIndex);
                        if (row != null) {
                            T readEntity = entityMapping.read(worksheet, row);
                            String readUuid = entityUuid(readEntity);

                            if (Utils.stringValid(readUuid)) {
                                T savedEntity = entityWithUuid(entitiesOnWorksheet, readUuid);

                                if (savedEntity != null) {
                                    if (!savedEntity.equals(readEntity)) {
                                        entityMapping.read(savedEntity, worksheet, row, syncRange.column, numColumns);
                                        updateLastModified(savedEntity);
                                    }
                                } else {
                                    updateLastModified(readEntity);
                                    entitiesOnWorksheet.add(readEntity);
                                }
                            }
                        }
                    }

                    // Sorrend tartas
                    for (CellRow row : rows.values()) {
                        T readEntity = entityMapping.read(worksheet, row);
                        T localEntity = entityWithUuid(entitiesOnWorksheet, entityUuid(readEntity));
                        if (localEntity != null) {
                            if (getPosition(localEntity) != row.getRowIndex()) {
                                setPosition(localEntity, row.getRowIndex());
                                updateLastModified(localEntity);
                            }
                        }
                    }

                    saveAll(entitiesOnWorksheet);
                } else {
                    // "Egyenkent" modszer, amikor csak bizonyos cellak erteket olvassuk ujra

                    // Cellák beolvasása
                    for (int rowIndex = syncRange.row; rowIndex < syncRange.row + numRows; rowIndex++) {
                        CellRow row = rows.get(rowIndex);
                        if (row != null) {
                            T readEntity = entityMapping.read(worksheet, row);
                            String readUuid = entityUuid(readEntity);

                            if (Utils.stringValid(readUuid)) {
                                T savedEntity = entityWithUuid(readUuid);

                                if (savedEntity != null) {
                                    entityMapping.read(savedEntity, worksheet, row, syncRange.column, numColumns);
                                    updateLastModified(savedEntity);
                                    save(savedEntity);
                                } else {
                                    updateLastModified(readEntity);
                                    save(readEntity);
                                }
                            }
                        }
                    }

                    // Sorrend tartas
                    for (CellRow row : rows.values()) {
                        T readEntity = entityMapping.read(worksheet, row);
                        T localEntity = entityWithUuid(entityUuid(readEntity));
                        if (localEntity != null) {
                            if (getPosition(localEntity) != row.getRowIndex()) {
                                setPosition(localEntity, row.getRowIndex());
                                updateLastModified(localEntity);
                                save(localEntity);
                            }
                        }
                    }
                }
            }
        }
    }

    protected abstract String entityUuid(T entity);

    private T entityWithUuid(List<T> entities, String uuid) {
        for (T entity : entities) {
            if (uuid != null && uuid.equals(entityUuid(entity))) {
                return entity;
            }
        }
        return null;
    }

    protected T entityWithUuid(String uuid) {
        return null;
    }

    protected abstract void save(T entity);

    protected abstract void updateLastModified(T enity);

    protected void delete(T entity) {
    }

    protected void setPosition(T entity, int position) {
    }

    protected int getPosition(T entity) {
        return 0;
    }

    protected abstract Set<T> allEntitiesOnWorksheet(WorksheetEntry worksheet);

    protected abstract void saveAll(List<T> entities);
}
