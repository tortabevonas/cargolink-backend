package hu.cargolink.backend.spreadsheets.core;

/**
 * Egy Google Táblázat cellát boolean értékeknek feleltet meg, és fordítva.
 *
 * Created by Aron on 2014.08.23..
 */
public abstract class BooleanFieldMapping<T> extends StringFieldMapping<T> {
    private static final String[] TRUE_VALUES = new String[]{"yes", "true", "igen"};
    private static final String TRUE_VALUE = "IGEN";
    private static final String FALSE_VALUE = "NEM";

    @Override
    public void read(T object, String value) {
        boolean booleanValue = false;
        if (value != null) {
            for (String trueValue : TRUE_VALUES) {
                if (value.trim().equalsIgnoreCase(trueValue.trim())) {
                    booleanValue = true;
                }
            }
        }
        readBoolean(object, booleanValue);
    }

    @Override
    public String write(T object) {
        return writeBoolean(object) ? TRUE_VALUE : FALSE_VALUE;
    }

    public abstract void readBoolean(T object, boolean value);

    public abstract boolean writeBoolean(T object);
}
