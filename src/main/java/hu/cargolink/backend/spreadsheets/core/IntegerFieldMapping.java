package hu.cargolink.backend.spreadsheets.core;

import com.google.gdata.data.spreadsheet.CellEntry;

import java.math.BigDecimal;

/**
 * Egy ObjT típusú objektum integer mezőjét felelteti meg egy táblázatcellának, és fordítva.
 *
 * Created by Aron on 2014.08.18..
 */
public abstract class IntegerFieldMapping<ObjT> extends FieldMapping<ObjT> {
    @Override
    public final void read(ObjT object, CellEntry cell) {
        int value = 0;
        if (cell != null) {
            try {
                value = new BigDecimal(cell.getCell().getNumericValue().toString()).intValue();
            } catch (Exception e) {
            }
        }
        readNumber(object, value);
    }

    @Override
    public final String write(ObjT object) {
        return "" + writeNumber(object);
    }

    public abstract void readNumber(ObjT object, int value);

    public abstract int writeNumber(ObjT object);
}
