package hu.cargolink.backend.spreadsheets.core;

import com.google.gdata.data.spreadsheet.SpreadsheetEntry;

/**
 * Egy adott táblázattal való szinkronizáció részleteit tartalmazza.
 *
 *
 * Created by Aron on 2014.08.21..
 */
public class SpreadsheetIntegration {
    public SpreadsheetIntegration(SpreadsheetEntry spreadsheet, SyncRange syncRange) {
        this.spreadsheet = spreadsheet;
        this.syncRange = syncRange;
    }

    /**
     * A szóban forgó táblázat (nem munkalap, azt majd az EntitySynchronizer meg az EntityMapping választja ki el)
     */
    private SpreadsheetEntry spreadsheet;

    /**
     * Azon cellatartomány, amelyet az alkalmazás használhat. A többihez nem nyúl.
     */
    private SyncRange syncRange;

    public SpreadsheetEntry getSpreadsheet() {
        return spreadsheet;
    }

    public SyncRange getSyncRange() {
        return syncRange;
    }
}