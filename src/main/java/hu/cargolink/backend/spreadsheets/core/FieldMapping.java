package hu.cargolink.backend.spreadsheets.core;

import com.google.gdata.data.spreadsheet.CellEntry;

/**
 * Egy Java objektum mezőjét felelteti meg egy Google táblázatsor egyik cellájának.
 * Alosztályai leírják, hogyan írható és olvasható az adott tulajdonság.
 *
 * Created by Aron on 2014.08.18..
 */
public abstract class FieldMapping<T> {
    public abstract void read(T object, CellEntry cell);

    public abstract String write(T object);
}
