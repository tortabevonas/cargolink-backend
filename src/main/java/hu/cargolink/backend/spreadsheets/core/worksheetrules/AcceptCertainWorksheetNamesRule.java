package hu.cargolink.backend.spreadsheets.core.worksheetrules;

/**
 * Csak meghatarozott nevu munkalapokat valaszt ki.
 *
 * Created by aronlorincz on 14. 12. 02..
 */
public class AcceptCertainWorksheetNamesRule implements AcceptWorksheetNameRule {
    private final String[] namesToAccept;

    public AcceptCertainWorksheetNamesRule(String... namesToAccept) {
        this.namesToAccept = namesToAccept;
    }

    @Override
    public boolean acceptWorksheet(String name) {
        for (String nameToAccept : namesToAccept) {
            if (nameToAccept.equals(name)) {
                return true;
            }
        }
        return false;
    }
}
