package hu.cargolink.backend.spreadsheets.core;

/**
 * Egy Google táblázat cellát dátumnak feleltet meg, és fordítva.
 *
 * Created by Aron on 2014.08.18..
 */
public abstract class DefaultDateFieldMapping<T> extends StringFieldMapping<T> {
    @Override
    public final void read(T object, String value) {
        readDate(object, DefaultDateFormat.parse(value));
    }

    @Override
    public final String write(T object) {
        return DefaultDateFormat.format(writeDate(object));
    }

    public abstract void readDate(T object, long date);

    public abstract long writeDate(T object);
}
