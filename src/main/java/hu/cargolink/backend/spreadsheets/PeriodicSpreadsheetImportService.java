package hu.cargolink.backend.spreadsheets;

import hu.cargolink.backend.utils.PeriodicService;

/**
 * Szolgáltatás amely rendszeres időközönként újraimportálja a táblázatok tartalmát.
 *
 * Created by aronlorincz on 14. 12. 02..
 */
public class PeriodicSpreadsheetImportService extends PeriodicService {
    private final SpreadsheetImportService spreadsheetImportService;
    private final long fullImportMaxPeriod;
    private long timeOfLastFullReimport = 0;

    public PeriodicSpreadsheetImportService(SpreadsheetImportService spreadsheetImportService, long period, long fullImportMaxPeriod) {
        super(period);
        this.spreadsheetImportService = spreadsheetImportService;
        this.fullImportMaxPeriod = fullImportMaxPeriod;
    }

    @Override
    protected void doJob() {
        boolean fullReimport = System.currentTimeMillis() - timeOfLastFullReimport > fullImportMaxPeriod;
        spreadsheetImportService.start(fullReimport);
        spreadsheetImportService.await();

        if (fullReimport) {
            timeOfLastFullReimport = System.currentTimeMillis();
        }
    }
}
