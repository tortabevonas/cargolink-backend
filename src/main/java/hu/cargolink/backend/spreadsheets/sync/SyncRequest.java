package hu.cargolink.backend.spreadsheets.sync;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;

/**
 * A táblázatban található szkripttől érkező szinkronizálási kérés
 *
 * Created by Aron on 2014.08.19..
 */
public class SyncRequest implements Serializable {
    @JsonProperty("rangeId")
    public String rangeId;

    @JsonProperty("spreadsheetId")
    public String spreadsheetId;

    @JsonProperty("worksheetId")
    public String worksheetId;

    @JsonProperty("row")
    public int row;

    @JsonProperty("column")
    public int column;

    @JsonProperty("numRows")
    public int numRows;

    @JsonProperty("numColumns")
    public int numColumns;

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "{Could not convert to JSON!}";
        }
    }
}
