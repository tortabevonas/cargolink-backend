package hu.cargolink.backend.spreadsheets.sync;

import hu.cargolink.backend.spreadsheets.SpreadsheetIntegrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.annotation.Router;
import org.springframework.messaging.MessageChannel;

/**
 * Spring Integration router a táblázatból érkező szinkronizálási kérések megfelelő szolgáltatáshoz való továbbítására.
 *
 * Created by Aron on 2014.08.20..
 */
public class SyncRequestRouter {
    @Autowired
    private SpreadsheetIntegrationService spreadsheetIntegrationService;

    @Autowired
    @Qualifier("exportSheetSyncRequestChannel")
    private MessageChannel exportChannel;

    @Autowired
    @Qualifier("importSheetSyncRequestChannel")
    private MessageChannel importChannel;

    @Autowired
    @Qualifier("soforSheetSyncRequestChannel")
    private MessageChannel soforChannel;

    private static final String RANGE_EXPORT = "export";
    private static final String RANGE_IMPORT = "import";
    private static final String RANGE_SOFOROK = "sofor";

    @Router
    public MessageChannel route(SyncRequest syncRequest) {
        System.out.println(
                String.format(
                        "Szinkronizálási kérés: megérkezett a routerbe: %s",
                        syncRequest.toString()
                )
        );

        String rangeId = syncRequest.rangeId;
        if (rangeId.equals(RANGE_EXPORT)) {
            return exportChannel;
        } else if (rangeId.equals(RANGE_IMPORT)) {
            return importChannel;
        } else if (rangeId.equals(RANGE_SOFOROK)) {
            return soforChannel;
        }
        return null;
    }
}
