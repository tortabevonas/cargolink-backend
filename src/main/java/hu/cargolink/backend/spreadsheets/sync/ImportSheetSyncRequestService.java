package hu.cargolink.backend.spreadsheets.sync;

import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import hu.cargolink.backend.data.imports.ImportJob;
import hu.cargolink.backend.data.imports.ImportJobRepository;
import hu.cargolink.backend.data.imports.ImportJobSynchronizer;
import hu.cargolink.backend.spreadsheets.DateUtils;
import hu.cargolink.backend.spreadsheets.core.SyncRange;
import hu.cargolink.backend.spreadsheets.core.worksheetrules.AcceptCertainWorksheetNamesRule;
import hu.cargolink.backend.utils.Stopwatch;
import hu.cargolink.backend.utils.Utils;
import hu.cargolink.backend.web.LinkGenerator;
import org.springframework.integration.annotation.ServiceActivator;

import java.util.List;
import java.util.Set;

/**
 * Created by Aron on 2014.08.20..
 */
public class ImportSheetSyncRequestService extends ImportJobSynchronizer {
    private SpreadsheetService spreadsheetService;
    private SpreadsheetEntry spreadsheetEntry;
    private ImportJobRepository importJobRepository;

    public ImportSheetSyncRequestService(SpreadsheetService spreadsheetService, SpreadsheetEntry spreadsheetEntry, ImportJobRepository importJobRepository, SyncRange maxSyncRange, LinkGenerator linkGenerator) {
        super(maxSyncRange, linkGenerator);
        this.spreadsheetService = spreadsheetService;
        this.spreadsheetEntry = spreadsheetEntry;
        this.importJobRepository = importJobRepository;
    }

    @ServiceActivator
    public void handleMessage(SyncRequest syncRequest) {
        System.out.println("Szinkronizálási kérés: kiszolgálás megkezdve: " + syncRequest.toString());

        Stopwatch stopwatch = new Stopwatch();
        try {
            syncEntities(
                    spreadsheetService,
                    spreadsheetEntry,
                    new AcceptCertainWorksheetNamesRule(syncRequest.worksheetId),
                    new SyncRange(
                            syncRequest.row,
                            syncRequest.numRows,
                            syncRequest.column,
                            syncRequest.numColumns
                    )
            );
        } catch (Exception e) {
            System.out.println("------------------\nSzinkronizálási kérés hiba\n---------------------");
            e.printStackTrace();
            System.out.println("------------------");
        }
        //System.out.println("Export táblázat változások frissítve: " + stopwatch.getElapsedSeconds() + " másodperc");
        System.out.println("Szinkronizálási kérés: " + stopwatch.getElapsedSeconds() + " másodperc alatt kiszolgálva: " + syncRequest.toString());
    }

    @Override
    public ImportJob entityWithUuid(String uuid) {
        if (!Utils.stringValid(uuid)) {
            return null;
        }
        return importJobRepository.findOne(uuid);
    }

    @Override
    public void save(ImportJob entity) {
        importJobRepository.saveWithLastModified(entity);
    }

    @Override
    protected void updateLastModified(ImportJob enity) {
        enity.lastModified = System.currentTimeMillis();
    }

    @Override
    public void delete(ImportJob entity) {
        importJobRepository.delete(entity);
    }

    @Override
    protected Set<ImportJob> allEntitiesOnWorksheet(WorksheetEntry worksheet) {
        return importJobRepository.findByWeek(DateUtils.weekFromWorksheetName(worksheet.getTitle().getPlainText()));
    }

    @Override
    protected void saveAll(List<ImportJob> entities) {
        importJobRepository.save(entities);
    }
}