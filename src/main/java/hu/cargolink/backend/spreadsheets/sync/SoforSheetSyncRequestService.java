package hu.cargolink.backend.spreadsheets.sync;

import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import hu.cargolink.backend.data.sofor.Sofor;
import hu.cargolink.backend.data.sofor.SoforRepository;
import hu.cargolink.backend.data.sofor.SoforSynchronizer;
import hu.cargolink.backend.spreadsheets.core.SyncRange;
import hu.cargolink.backend.spreadsheets.core.worksheetrules.AcceptCertainWorksheetNamesRule;
import hu.cargolink.backend.utils.Stopwatch;
import hu.cargolink.backend.utils.Utils;
import org.springframework.integration.annotation.ServiceActivator;

import java.util.List;
import java.util.Set;

/**
 * A sofőrök munkalapját beolvasó szolgáltatás
 *
 * Created by Aron on 2014.08.20..
 */
public class SoforSheetSyncRequestService extends SoforSynchronizer {
    private SpreadsheetService spreadsheetService;
    private SpreadsheetEntry spreadsheetEntry;
    private SoforRepository soforRepository;

    public SoforSheetSyncRequestService(SpreadsheetService spreadsheetService, com.google.gdata.data.spreadsheet.SpreadsheetEntry spreadsheetEntry, SoforRepository soforRepository, SyncRange maxSyncRange) {
        super(maxSyncRange);
        this.spreadsheetService = spreadsheetService;
        this.spreadsheetEntry = spreadsheetEntry;
        this.soforRepository = soforRepository;
    }

    @ServiceActivator
    public void handleMessage(SyncRequest syncRequest) {
        System.out.println("Szinkronizálási kérés: kiszolgálás megkezdve: " + syncRequest.toString());

        Stopwatch stopwatch = new Stopwatch();
        try {
            syncEntities(
                    spreadsheetService,
                    spreadsheetEntry,
                    new AcceptCertainWorksheetNamesRule(syncRequest.worksheetId),
                    new SyncRange(
                            syncRequest.row,
                            syncRequest.numRows,
                            syncRequest.column,
                            syncRequest.numColumns
                    )
            );
        } catch (Exception e) {
            System.out.println("------------------\nSzinkronizálási kérés hiba\n---------------------");
            e.printStackTrace();
            System.out.println("------------------");
        }
        //System.out.println("Export táblázat változások frissítve: " + stopwatch.getElapsedSeconds() + " másodperc");
        System.out.println("Szinkronizálási kérés: " + stopwatch.getElapsedSeconds() + " másodperc alatt kiszolgálva: " + syncRequest.toString());
    }

    @Override
    public Sofor entityWithUuid(String uuid) {
        if (!Utils.stringValid(uuid)) {
            return null;
        }
        return soforRepository.findOne(uuid);
    }

    @Override
    public void save(Sofor entity) {
        soforRepository.save(entity);
    }

    @Override
    protected void updateLastModified(Sofor enity) {
    }

    @Override
    public void delete(Sofor entity) {
        soforRepository.delete(entity);
    }

    @Override
    protected Set<Sofor> allEntitiesOnWorksheet(WorksheetEntry worksheet) {
        return Utils.toSet(soforRepository.findAll());
    }

    @Override
    protected void saveAll(List<Sofor> entities) {
        soforRepository.save(entities);
    }
}
