package hu.cargolink.backend.spreadsheets;

/**
 * Táblázat betöltésének az eredményéről szóló jelentés.
 *
 * Created by aronlorincz on 14. 12. 02..
 */
public class SpreadsheetImportServiceResult {
    public Throwable error;
    public long time;
}
