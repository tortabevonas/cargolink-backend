package hu.cargolink.backend.spreadsheets;

import hu.cargolink.backend.spreadsheets.core.worksheetrules.AcceptAllWorksheetNamesRule;
import hu.cargolink.backend.spreadsheets.core.worksheetrules.AcceptLastNWeeksWorksheetNameRule;
import hu.cargolink.backend.spreadsheets.core.worksheetrules.AcceptWorksheetNameRule;
import hu.cargolink.backend.spreadsheets.sync.ExportSheetSyncRequestService;
import hu.cargolink.backend.spreadsheets.sync.ImportSheetSyncRequestService;
import hu.cargolink.backend.spreadsheets.sync.SoforSheetSyncRequestService;
import hu.cargolink.backend.utils.AsyncService;
import hu.cargolink.backend.utils.Stopwatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * Táblázatok betöltését végző szolgáltatás.
 *
 * Created by Aron on 2014.08.19..
 */
public class SpreadsheetImportService extends AsyncService<Boolean, SpreadsheetImportServiceResult> {
    @Autowired
    private SpreadsheetIntegrationService service;

    public ImportSheetSyncRequestService importSheetSyncRequestService;

    public ExportSheetSyncRequestService exportSheetSyncRequestService;

    public SoforSheetSyncRequestService soforSheetSyncRequestService;

    private Stopwatch stopwatch;

    @Transactional
    @Override
    protected SpreadsheetImportServiceResult doJob(Boolean fullReimport) {
        stopwatch = new Stopwatch();
        System.out.println("Adatok " + (fullReimport ? "TELJES" : "RÉSZLEGES") + " újraimportálása megkezdve");


        SpreadsheetImportServiceResult result = new SpreadsheetImportServiceResult();
        try {
            AcceptWorksheetNameRule worksheetNameRule;
            if (fullReimport.booleanValue()) {
                worksheetNameRule = new AcceptAllWorksheetNamesRule();
            } else {
                worksheetNameRule = new AcceptLastNWeeksWorksheetNameRule(2);
            }

            importSheetSyncRequestService.syncEntities(service.getSpreadsheetService(), service.getImportSheetIntegration().getSpreadsheet(), worksheetNameRule, service.getImportSheetIntegration().getSyncRange());
            exportSheetSyncRequestService.syncEntities(service.getSpreadsheetService(), service.getExportSheetIntegration().getSpreadsheet(), worksheetNameRule, service.getExportSheetIntegration().getSyncRange());
            soforSheetSyncRequestService.syncEntities(service.getSpreadsheetService(), service.getSoforSheetIntegration().getSpreadsheet(), new AcceptAllWorksheetNamesRule(), service.getSoforSheetIntegration().getSyncRange());

            result.time = stopwatch.getElapsedTime();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            result.error = throwable;
        }


        System.out.println("Adatok " + (fullReimport ? "TELJES" : "RÉSZLEGES") + " újraimportálása befejeződött, " + (result.error == null ? "sikeresen" : "hibával") + ". Idotartam: " + stopwatch.getElapsedSeconds() + " masodperc");
        return result;
    }
}
