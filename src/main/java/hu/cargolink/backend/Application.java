package hu.cargolink.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * A Spring Boot alkalmazás fő belépési pontja, amely tartalmazza a konfigurációs fájlok és osztályok elérhetőségét (lásd a Spring Boot dokumentációját).
 *
 * Created by Aron on 2014.08.17..
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(new Object[]{Application.class, "classpath:config/sync-configuration.xml", "classpath:config/integration-configuration.xml"}, args);
    }
}
