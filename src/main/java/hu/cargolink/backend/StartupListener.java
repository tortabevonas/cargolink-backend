package hu.cargolink.backend;

import hu.cargolink.backend.spreadsheets.PeriodicSpreadsheetImportService;
import hu.cargolink.backend.spreadsheets.SpreadsheetImportService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Az alkalmazás indítására reagálva megkezdi a táblázatok teljes újraimportálását
 *
 * Created by Aron on 2014.08.26..
 */
public class StartupListener implements ApplicationContextAware {
    @Autowired
    private SpreadsheetImportService spreadsheetImportService;

    @Autowired
    private PeriodicSpreadsheetImportService periodicSpreadsheetImportService;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        periodicSpreadsheetImportService.start();
    }
}
