package hu.cargolink.backend.data.imports;

import hu.cargolink.backend.spreadsheets.core.EntitySynchronizer;
import hu.cargolink.backend.spreadsheets.core.SyncRange;
import hu.cargolink.backend.web.LinkGenerator;

/**
 * Import munkak Google tablazatokbol valo betolteset menedzseli.
 * Alosztalyai dontik el, hogy mikor es mit toltsenek be, ez az osztaly csak azt definialja, ami az importalando
 * adatokra tipusara vonatkozik.
 *
 * Created by Aron on 2014.08.20..
 */
public abstract class ImportJobSynchronizer extends EntitySynchronizer<ImportJob> {
    protected ImportJobSynchronizer(SyncRange maxSyncRange, LinkGenerator linkGenerator) {
        super(maxSyncRange, new ImportJobEntityMapping(linkGenerator));
    }

    @Override
    public String entityUuid(ImportJob entity) {
        return entity.id;
    }

    @Override
    protected void setPosition(ImportJob entity, int order) {
        entity.position = order;
    }

    @Override
    protected int getPosition(ImportJob entity) {
        return entity.position;
    }
}
