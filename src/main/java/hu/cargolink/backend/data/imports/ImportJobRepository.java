package hu.cargolink.backend.data.imports;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

/**
 * Spring Data repository az import munkaknak
 *
 * Created by Aron on 2014.08.17..
 */
public interface ImportJobRepository extends JpaRepository<ImportJob, String>, ImportJobRepositoryCustom {
    @Query("from ImportJob j where j.lastModified > ?1")
    public Set<ImportJob> findSoforUpdates(long lastModified);

    public Set<ImportJob> findByWeek(int week);

    public Set<ImportJob> findByElofutasJarat(String elofutasJarat);
}
