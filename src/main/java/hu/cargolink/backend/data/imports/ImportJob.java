package hu.cargolink.backend.data.imports;

import hu.cargolink.backend.data.Job;
import hu.cargolink.backend.data.documents.Document;
import hu.cargolink.backend.soforservice.common.LatLng;
import hu.cargolink.backend.utils.Utils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * A cég egy import irányú munkáját jelképezi.
 * Az ilyen munkák két szakaszon mennek keresztül, ebben a sorrendben:
 *  - Nemzetközi szakasz: behozzák külföldről a központi raktárba az árut
 *  - Belföldi szakasz: a központi raktárba beszállított árut az országon belül rendeltetési helyére szállítják
 *
 * Created by Aron on 2014.08.11..
 */
@Entity
public class ImportJob implements Job, Serializable {
    @Id
    public String id = UUID.randomUUID().toString();

    public long lastModified;

    public int position;

    public int week;

    public ImportJobStatus status;

    @Column(length = 8000)
    public String megbizo;

    @Column(length = 8000)
    public String felrakasiHely;

    public long felvetelTervDatuma;

    @Column(length = 8000)
    public String lerakasiHely;

    @Column(length = 8000)
    public String cimzett;

    public long lerakasiHatarido;

    @Column(length = 8000)
    public String felveteliRef;

    @Column(length = 8000)
    public String elofutasJarat;

    /**
     * Kulfoldi szakasz gepjarmu rendszama
     */
    @Column(length = 8000)
    public String elofutasTgk;

    /**
     * Kulfoldi szakasz potkocsi rendszam
     */
    @Column(length = 8000)
    public String elofutasPotkocsi;

    /**
     * Kulfoldi szakasz gepjarmuvezeto ID-je
     */
    @Column(length = 8000)
    public String elofutasGkv;

    public long felvetelTenylDatuma;

    @Column(length = 8000)
    public String serulesFelvetelkor;

    public int euPalettaHelyFelvetelkor;

    public int colliFelvetelkor = 0;

    public BigDecimal kgFelvetelkor = BigDecimal.ZERO;

    public BigDecimal cbmFelvetelkor = BigDecimal.ZERO;

    public long betarDatum;

    @Column(length = 8000)
    public String serulesBetarolaskor;

    @Column(length = 8000)
    public String raktariMegjegyzes;

    public long kitarDatum;

    /**
     * Belfoldi szakasz gepjarmu rendszam
     */
    @Column(length = 8000)
    public String kiszallTgk;

    /**
     * Belfoldi szakasz potkocsi rendszam
     */
    @Column(length = 8000)
    public String kiszallPotkocsi;

    /**
     * Belfoldi szakasz jaratszam
     */
    @Column(length = 8000)
    public String kiszallJarat;

    /**
     * Belfoldi szakasz gepjarmuvezeto ID
     */
    @Column(length = 8000)
    public String kiszallGkv;

    public long lerakasTenylDatuma;

    @Column(length = 8000)
    public String serulesLerakaskor;

    public AfaType afaTipusa;

    public boolean vamkezelesKesz;

    public int euPalettaHely;

    public int colli = 0;

    public BigDecimal kg = BigDecimal.ZERO;

    public BigDecimal cbm = BigDecimal.ZERO;

    public String marker;

    @OrderBy("dateOfCreation ASC")
    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    public Set<Document> documents = new HashSet<Document>();
    /*public void addDocument(Document document) {
        document.jobId = id;
    }*/

    //koordináták

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "lat", column = @Column(name = "felvetel_pozicio_lat")),
            @AttributeOverride(name = "lng", column = @Column(name = "felvetel_pozicio_lng")),
    })
    public LatLng felvetelPozicio;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "lat", column = @Column(name = "lerakas_pozicio_lat")),
            @AttributeOverride(name = "lng", column = @Column(name = "lerakas_pozicio_lng")),
    })
    public LatLng lerakasPozicio;

    public boolean vamkezelesreVar() {
        return (status == ImportJobStatus.IMP_LKW_FELVETTE || status == ImportJobStatus.IMP_LKW_FULL) && afaTipusa == AfaType.IMPORT_FUVAR && !vamkezelesKesz;
    }

    public static final int MAX_EU_PAL_PER_JARAT = 33;

    @Override
    public String getId() {
        return id;
    }

    /*@Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof ImportJob) {
            ImportJob to = (ImportJob) obj;
            return true
                    && (id != null ? id.equals(to.id) : to.id == null)
                    && (lastModified == to.lastModified)
                    && (position == to.position)
                    && (week == to.week)
                    && (status == to.status)
                    && (megbizo != null ? megbizo.equals(to.megbizo) : to.megbizo == null)
                    && (felrakasiHely != null ? felrakasiHely.equals(to.felrakasiHely) : to.felrakasiHely == null)
                    && ()
                    ;
        } else {
            return false;
        }
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ImportJob)) return false;

        ImportJob importJob = (ImportJob) o;

        if (betarDatum != importJob.betarDatum) return false;
        if (colli != importJob.colli) return false;
        if (colliFelvetelkor != importJob.colliFelvetelkor) return false;
        if (euPalettaHely != importJob.euPalettaHely) return false;
        if (euPalettaHelyFelvetelkor != importJob.euPalettaHelyFelvetelkor) return false;
        if (felvetelTenylDatuma != importJob.felvetelTenylDatuma) return false;
        if (felvetelTervDatuma != importJob.felvetelTervDatuma) return false;
        if (kitarDatum != importJob.kitarDatum) return false;
        if (lerakasTenylDatuma != importJob.lerakasTenylDatuma) return false;
        if (lerakasiHatarido != importJob.lerakasiHatarido) return false;
        if (vamkezelesKesz != importJob.vamkezelesKesz) return false;
        if (week != importJob.week) return false;
        if (afaTipusa != importJob.afaTipusa) return false;
        if (!Utils.same(cbm, importJob.cbm)) return false;
        if (!Utils.same(cbmFelvetelkor, importJob.cbmFelvetelkor))
            return false;
        if (cimzett != null ? !cimzett.equals(importJob.cimzett) : importJob.cimzett != null) return false;
        if (elofutasGkv != null ? !elofutasGkv.equals(importJob.elofutasGkv) : importJob.elofutasGkv != null)
            return false;
        if (elofutasJarat != null ? !elofutasJarat.equals(importJob.elofutasJarat) : importJob.elofutasJarat != null)
            return false;
        if (elofutasPotkocsi != null ? !elofutasPotkocsi.equals(importJob.elofutasPotkocsi) : importJob.elofutasPotkocsi != null)
            return false;
        if (elofutasTgk != null ? !elofutasTgk.equals(importJob.elofutasTgk) : importJob.elofutasTgk != null)
            return false;
        if (felrakasiHely != null ? !felrakasiHely.equals(importJob.felrakasiHely) : importJob.felrakasiHely != null)
            return false;
        if (felvetelPozicio != null ? !felvetelPozicio.equals(importJob.felvetelPozicio) : importJob.felvetelPozicio != null)
            return false;
        if (felveteliRef != null ? !felveteliRef.equals(importJob.felveteliRef) : importJob.felveteliRef != null)
            return false;
        if (id != null ? !id.equals(importJob.id) : importJob.id != null) return false;
        if (!Utils.same(kg, importJob.kg)) return false;
        if (!Utils.same(kgFelvetelkor, importJob.kgFelvetelkor))
            return false;
        if (kiszallGkv != null ? !kiszallGkv.equals(importJob.kiszallGkv) : importJob.kiszallGkv != null) return false;
        if (kiszallJarat != null ? !kiszallJarat.equals(importJob.kiszallJarat) : importJob.kiszallJarat != null)
            return false;
        if (kiszallPotkocsi != null ? !kiszallPotkocsi.equals(importJob.kiszallPotkocsi) : importJob.kiszallPotkocsi != null)
            return false;
        if (kiszallTgk != null ? !kiszallTgk.equals(importJob.kiszallTgk) : importJob.kiszallTgk != null) return false;
        if (lerakasPozicio != null ? !lerakasPozicio.equals(importJob.lerakasPozicio) : importJob.lerakasPozicio != null)
            return false;
        if (lerakasiHely != null ? !lerakasiHely.equals(importJob.lerakasiHely) : importJob.lerakasiHely != null)
            return false;
        if (marker != null ? !marker.equals(importJob.marker) : importJob.marker != null) return false;
        if (megbizo != null ? !megbizo.equals(importJob.megbizo) : importJob.megbizo != null) return false;
        if (raktariMegjegyzes != null ? !raktariMegjegyzes.equals(importJob.raktariMegjegyzes) : importJob.raktariMegjegyzes != null)
            return false;
        if (serulesBetarolaskor != null ? !serulesBetarolaskor.equals(importJob.serulesBetarolaskor) : importJob.serulesBetarolaskor != null)
            return false;
        if (serulesFelvetelkor != null ? !serulesFelvetelkor.equals(importJob.serulesFelvetelkor) : importJob.serulesFelvetelkor != null)
            return false;
        if (serulesLerakaskor != null ? !serulesLerakaskor.equals(importJob.serulesLerakaskor) : importJob.serulesLerakaskor != null)
            return false;
        if (status != importJob.status) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + position;
        result = 31 * result + week;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (megbizo != null ? megbizo.hashCode() : 0);
        result = 31 * result + (felrakasiHely != null ? felrakasiHely.hashCode() : 0);
        result = 31 * result + (int) (felvetelTervDatuma ^ (felvetelTervDatuma >>> 32));
        result = 31 * result + (lerakasiHely != null ? lerakasiHely.hashCode() : 0);
        result = 31 * result + (cimzett != null ? cimzett.hashCode() : 0);
        result = 31 * result + (int) (lerakasiHatarido ^ (lerakasiHatarido >>> 32));
        result = 31 * result + (felveteliRef != null ? felveteliRef.hashCode() : 0);
        result = 31 * result + (elofutasJarat != null ? elofutasJarat.hashCode() : 0);
        result = 31 * result + (elofutasTgk != null ? elofutasTgk.hashCode() : 0);
        result = 31 * result + (elofutasPotkocsi != null ? elofutasPotkocsi.hashCode() : 0);
        result = 31 * result + (elofutasGkv != null ? elofutasGkv.hashCode() : 0);
        result = 31 * result + (int) (felvetelTenylDatuma ^ (felvetelTenylDatuma >>> 32));
        result = 31 * result + (serulesFelvetelkor != null ? serulesFelvetelkor.hashCode() : 0);
        result = 31 * result + euPalettaHelyFelvetelkor;
        result = 31 * result + colliFelvetelkor;
        result = 31 * result + (kgFelvetelkor != null ? kgFelvetelkor.hashCode() : 0);
        result = 31 * result + (cbmFelvetelkor != null ? cbmFelvetelkor.hashCode() : 0);
        result = 31 * result + (int) (betarDatum ^ (betarDatum >>> 32));
        result = 31 * result + (serulesBetarolaskor != null ? serulesBetarolaskor.hashCode() : 0);
        result = 31 * result + (raktariMegjegyzes != null ? raktariMegjegyzes.hashCode() : 0);
        result = 31 * result + (int) (kitarDatum ^ (kitarDatum >>> 32));
        result = 31 * result + (kiszallTgk != null ? kiszallTgk.hashCode() : 0);
        result = 31 * result + (kiszallPotkocsi != null ? kiszallPotkocsi.hashCode() : 0);
        result = 31 * result + (kiszallJarat != null ? kiszallJarat.hashCode() : 0);
        result = 31 * result + (kiszallGkv != null ? kiszallGkv.hashCode() : 0);
        result = 31 * result + (int) (lerakasTenylDatuma ^ (lerakasTenylDatuma >>> 32));
        result = 31 * result + (serulesLerakaskor != null ? serulesLerakaskor.hashCode() : 0);
        result = 31 * result + (afaTipusa != null ? afaTipusa.hashCode() : 0);
        result = 31 * result + (vamkezelesKesz ? 1 : 0);
        result = 31 * result + euPalettaHely;
        result = 31 * result + colli;
        result = 31 * result + (kg != null ? kg.hashCode() : 0);
        result = 31 * result + (cbm != null ? cbm.hashCode() : 0);
        result = 31 * result + (marker != null ? marker.hashCode() : 0);
        result = 31 * result + (felvetelPozicio != null ? felvetelPozicio.hashCode() : 0);
        result = 31 * result + (lerakasPozicio != null ? lerakasPozicio.hashCode() : 0);
        return result;
    }
}
