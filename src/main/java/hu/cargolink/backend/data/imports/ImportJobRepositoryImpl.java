package hu.cargolink.backend.data.imports;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Az ImportJobRepository-n vegezheto sajat muveletek implementacioja.
 *
 * Created by Aron on 2014.08.21..
 */
public class ImportJobRepositoryImpl implements ImportJobRepositoryCustom {
    @Autowired
    private ImportJobRepository importJobRepository;


    @Override
    public <E extends ImportJob> E saveWithLastModified(E object) {
        object.lastModified = System.currentTimeMillis();
        return importJobRepository.save(object);
    }

    @Override
    public <E extends ImportJob> Iterable<E> saveWithLastModified(Iterable<E> objects) {
        for (ImportJob object : objects) {
            object.lastModified = System.currentTimeMillis();
        }
        return importJobRepository.save(objects);
    }
}
