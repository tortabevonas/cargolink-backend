package hu.cargolink.backend.data.imports;

import com.google.gdata.data.spreadsheet.WorksheetEntry;
import hu.cargolink.backend.soforservice.common.LatLng;
import hu.cargolink.backend.spreadsheets.DateUtils;
import hu.cargolink.backend.spreadsheets.core.*;
import hu.cargolink.backend.utils.Utils;
import hu.cargolink.backend.web.LinkGenerator;

import java.math.BigDecimal;

/**
 * ImportJob objektumokat konvertal Google Spreadsheets tablazat soraiva, vagy tablazatsorbol tolt be ImportJob objektumokat.
 * Minden egyes mezohoz definialja, hogyan lehet azt irni- es olvasni.
 *
 * Created by Aron on 2014.08.18..
 */
public class ImportJobEntityMapping extends EntityMapping<ImportJob> {
    public ImportJobEntityMapping(final LinkGenerator linkGenerator) {
        addField(new EnumFieldMapping<ImportJob, ImportJobStatus>() {
            private final String[] STATUS_NAMES = new String[]{
                    "Megbízás felvéve",
                    "Kiadva felvételre IMP LKWnak",
                    "IMP LKW felvette",
                    "IMP LKW Full",
                    "Raktárba Beérkezett",
                    "Kiadva KIszállításra Saját LKW",
                    "Kiadva Kiszállításra Alvállalkozó",
                    "Megbízó szállíttatja Ki",
                    "Lerakva címzettnél / Kiadva megbízónak",
                    "Számlázható",
                    "Számlázva",
                    "Stornó a Megbízás",
            };

            private final ImportJobStatus[] STATUS_VALUES = new ImportJobStatus[]{
                    ImportJobStatus.MEGBIZAS_FELVEVE,
                    ImportJobStatus.KIADVA_FELVETELRE_IMP_LKWNAK,
                    ImportJobStatus.IMP_LKW_FELVETTE,
                    ImportJobStatus.IMP_LKW_FULL,
                    ImportJobStatus.RAKTARBA_BEERKEZETT,
                    ImportJobStatus.KIADVA_KISZALLITASRA_SAJAT_LKW,
                    ImportJobStatus.KIADVA_KISZALLITASRA_ALVALLAKOZO,
                    ImportJobStatus.KIADVA_KISZALLITASRA_MEGBIZO,
                    ImportJobStatus.LERAKVA_CIMZETTNEL_V_KIADVA_MEGBIZONAK,
                    ImportJobStatus.SZAMLAZHATO,
                    ImportJobStatus.SZAMLAZVA,
                    ImportJobStatus.STORNO,
            };


            @Override
            public void readEnum(ImportJob object, ImportJobStatus value) {
                object.status = value;
            }

            @Override
            public ImportJobStatus writeEnum(ImportJob object) {
                return object.status;
            }

            @Override
            public String[] names() {
                return STATUS_NAMES;
            }

            @Override
            public ImportJobStatus[] values() {
                return STATUS_VALUES;
            }
        }, "A", FIELD_STATUS);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.megbizo = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.megbizo;
            }
        }, "B", FIELD_MEGBIZO);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.felrakasiHely = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.felrakasiHely;
            }
        }, "C", FIELD_FELRAKASI_HELY);

        addField(new DefaultDateFieldMapping<ImportJob>() {
            @Override
            public void readDate(ImportJob object, long date) {
                object.felvetelTervDatuma = date;
            }

            @Override
            public long writeDate(ImportJob object) {
                return object.felvetelTervDatuma;
            }
        }, "D", FIELD_FELVETEL_TERV_DATUMA);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.lerakasiHely = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.lerakasiHely;
            }
        }, "E", FIELD_LERAKASI_HELY);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.cimzett = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.cimzett;
            }
        }, "F", FIELD_CIMZETT);

        addField(new DefaultDateFieldMapping<ImportJob>() {
            @Override
            public void readDate(ImportJob object, long date) {
                object.lerakasiHatarido = date;
            }

            @Override
            public long writeDate(ImportJob object) {
                return object.lerakasiHatarido;
            }
        }, "G", FIELD_LERAKASI_HATARIDO);

        addField(new IntegerFieldMapping<ImportJob>() {
            @Override
            public void readNumber(ImportJob object, int value) {
                object.euPalettaHely = value;
            }

            @Override
            public int writeNumber(ImportJob object) {
                return object.euPalettaHely;
            }
        }, "H", FIELD_EU_PALETTA_HELY);

        addField(new IntegerFieldMapping<ImportJob>() {
            @Override
            public void readNumber(ImportJob object, int value) {
                object.colli = value;
            }

            @Override
            public int writeNumber(ImportJob object) {
                return object.colli;
            }
        }, "I", FIELD_COLLI);

        addField(new DecimalFieldMapping<ImportJob>() {
            @Override
            public void readDecimal(ImportJob object, BigDecimal value) {
                object.kg = value;
            }

            @Override
            public BigDecimal writeDecimal(ImportJob object) {
                return object.kg;
            }
        }, "J", FIELD_KG);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.felveteliRef = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.felveteliRef;
            }
        }, "K", FIELD_FELVETELI_REF);

        addField(new DecimalFieldMapping<ImportJob>() {
            @Override
            public void readDecimal(ImportJob object, BigDecimal value) {
                object.cbm = value;
            }

            @Override
            public BigDecimal writeDecimal(ImportJob object) {
                return object.cbm;
            }
        }, "L", FIELD_CBM);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.elofutasJarat = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.elofutasJarat;
            }
        }, "M", FIELD_ELOFUTAS_JARAT);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.elofutasTgk = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.elofutasTgk;
            }
        }, "N", FIELD_ELOFUTAS_TGK);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.elofutasPotkocsi = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.elofutasPotkocsi;
            }
        }, "O", FIELD_ELOFUTAS_POTKOCSI);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.elofutasGkv = Utils.fixNameCase(value);
            }

            @Override
            public String write(ImportJob object) {
                return object.elofutasGkv;
            }
        }, "P", FIELD_ELOFUTAS_GKV);

        addField(new DefaultTimeFieldMapping<ImportJob>() {
            @Override
            public void readTime(ImportJob object, long date) {
                object.felvetelTenylDatuma = date;
            }

            @Override
            public long writeTime(ImportJob object) {
                return object.felvetelTenylDatuma;
            }
        }, "Q", FIELD_FELVETEL_TENYL_DATUMA);

        addField(new BooleanFieldMapping<ImportJob>() {
            @Override
            public void readBoolean(ImportJob object, boolean value) {
                //Nem olvassuk be, ez egy segéd mező az egységesség kedvéért
            }

            @Override
            public boolean writeBoolean(ImportJob object) {
                return Utils.stringValid(object.serulesFelvetelkor);
            }
        }, "R", FIELD_SERULES_FELVETELKOR);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.serulesFelvetelkor = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.serulesFelvetelkor;
            }
        }, "S", FIELD_SERULES_LEIRASA_FEVETELKOR);

        addField(new DefaultDateFieldMapping<ImportJob>() {
            @Override
            public void readDate(ImportJob object, long date) {
                object.betarDatum = date;
            }

            @Override
            public long writeDate(ImportJob object) {
                return object.betarDatum;
            }
        }, "T", FIELD_BETAR_DATUM);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.serulesBetarolaskor = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.serulesBetarolaskor;
            }
        }, "V", FIELD_SERULES_LEIRASA_BETAROLASKOR);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.raktariMegjegyzes = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.raktariMegjegyzes;
            }
        }, "W", FIELD_RAKTARI_MEGJEGYZES);

        addField(new DefaultDateFieldMapping<ImportJob>() {
            @Override
            public void readDate(ImportJob object, long date) {
                object.kitarDatum = date;
            }

            @Override
            public long writeDate(ImportJob object) {
                return object.kitarDatum;
            }
        }, "X", FIELD_KITAR_DATUM);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.kiszallTgk = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.kiszallGkv;
            }
        }, "Y", FIELD_KISZALL_TGK);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.kiszallPotkocsi = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.kiszallPotkocsi;
            }
        }, "Z", FIELD_KISZALL_POTKOCSI);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.kiszallJarat = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.kiszallJarat;
            }
        }, "AA", FIELD_KISZALL_JARAT);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.kiszallGkv = Utils.fixNameCase(value);
            }

            @Override
            public String write(ImportJob object) {
                return object.kiszallGkv;
            }
        }, "AB", FIELD_KISZALL_GKV);

        addField(new DefaultTimeFieldMapping<ImportJob>() {
            @Override
            public void readTime(ImportJob object, long date) {
                object.lerakasTenylDatuma = date;
            }

            @Override
            public long writeTime(ImportJob object) {
                return object.lerakasTenylDatuma;
            }
        }, "AC", FIELD_LERAKAS_TENYL_DATUMA);

        addField(new EnumFieldMapping<ImportJob, AfaType>() {
            @Override
            public void readEnum(ImportJob object, AfaType value) {
                object.afaTipusa = value;
            }

            @Override
            public AfaType writeEnum(ImportJob object) {
                return object.afaTipusa;
            }

            private final String[] NAMES = new String[]{
                    "Áruszállítás az EU területén",
                    "Import Fuvar",
                    "Raktározás",
                    "Kiállás",
                    "Belföldi Áruszállítás",
                    "Import Vámkezelés"
            };

            private final AfaType[] VALUES = new AfaType[]{
                    AfaType.ARUSZALLITAS_EU_TERULETEN,
                    AfaType.IMPORT_FUVAR,
                    AfaType.RAKTAROZAS,
                    AfaType.KIALLAS,
                    AfaType.BELFOLDI_ARUSZALLITAS,
                    AfaType.IMPORT_VAMKEZELES,
            };

            @Override
            public String[] names() {
                return NAMES;
            }

            @Override
            public AfaType[] values() {
                return VALUES;
            }
        }, "BA", FIELD_AFA_TIPUSA);

        ///////////////////////////////////

        addField(new LatLngFieldMapping<ImportJob>() {
            @Override
            public void readLatLng(ImportJob object, LatLng latLng) {
                object.felvetelPozicio = latLng;
            }

            @Override
            public LatLng writeLatLng(ImportJob object) {
                return object.felvetelPozicio;
            }
        }, "BC", FIELD_FELVETEL_POZICIO);

        addField(new BooleanFieldMapping<ImportJob>() {
            @Override
            public void readBoolean(ImportJob object, boolean value) {
                //Nem olvassuk vissza mert segédmező
            }

            @Override
            public boolean writeBoolean(ImportJob object) {
                return true
                        && object.euPalettaHely == object.euPalettaHelyFelvetelkor
                        && object.colli == object.colliFelvetelkor
                        && object.kg.compareTo(object.kgFelvetelkor) == 0
                        && object.cbm.compareTo(object.cbmFelvetelkor) == 0;
            }
        }, "BD", FIELD_FELVETT_MENNYISEG_EGYEZIK);

        addField(new IntegerFieldMapping<ImportJob>() {
            @Override
            public void readNumber(ImportJob object, int value) {
                object.euPalettaHelyFelvetelkor = value;
            }

            @Override
            public int writeNumber(ImportJob object) {
                return object.euPalettaHelyFelvetelkor;
            }
        }, "BE", FIELD_EU_PALETTA_HELY_FELVETELKOR);

        addField(new IntegerFieldMapping<ImportJob>() {
            @Override
            public void readNumber(ImportJob object, int value) {
                object.colliFelvetelkor = value;
            }

            @Override
            public int writeNumber(ImportJob object) {
                return object.colliFelvetelkor;
            }
        }, "BF", FIELD_COLLI_FELVETELKOR);

        addField(new DecimalFieldMapping<ImportJob>() {
            @Override
            public void readDecimal(ImportJob object, BigDecimal value) {
                object.kgFelvetelkor = value;
            }

            @Override
            public BigDecimal writeDecimal(ImportJob object) {
                return object.kgFelvetelkor;
            }
        }, "BG", FIELD_KG_FELVETELKOR);

        addField(new DecimalFieldMapping<ImportJob>() {
            @Override
            public void readDecimal(ImportJob object, BigDecimal value) {
                object.cbmFelvetelkor = value;
            }

            @Override
            public BigDecimal writeDecimal(ImportJob object) {
                return object.cbmFelvetelkor;
            }
        }, "BH", FIELD_CBM_FELVETELKOR);

        addField(new BooleanFieldMapping<ImportJob>() {
            @Override
            public void readBoolean(ImportJob object, boolean value) {
                object.vamkezelesKesz = value;
            }

            @Override
            public boolean writeBoolean(ImportJob object) {
                return object.vamkezelesKesz;
            }
        }, "BN", FIELD_VAMKEZELES_KESZ);

        addField(new LatLngFieldMapping<ImportJob>() {
            @Override
            public void readLatLng(ImportJob object, LatLng latLng) {
                object.lerakasPozicio = latLng;
            }

            @Override
            public LatLng writeLatLng(ImportJob object) {
                return object.lerakasPozicio;
            }
        }, "BK", FIELD_LERAKAS_POZICIO);

        addField(new BooleanFieldMapping<ImportJob>() {
            @Override
            public void readBoolean(ImportJob object, boolean value) {
                // Nem olvassuk be, segéd mező, az egységesség kedvéért
            }

            @Override
            public boolean writeBoolean(ImportJob object) {
                return Utils.stringValid(object.serulesLerakaskor);
            }
        }, "BL", FIELD_SERULES_LERAKASKOR);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.serulesLerakaskor = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.serulesLerakaskor;
            }
        }, "BM", FIELD_SERULES_LEIRASA_LERAKASKOR);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                // Természetesen nem olvassuk vissza a dokumentumokat
            }

            @Override
            public String write(ImportJob object) {
                return linkGenerator.linkToJobDocuments(object.id);
            }
        }, "BI", FIELD_DOCUMENTS);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.marker = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.marker;
            }
        }, "BJ", FIELD_MARKER);

        addField(new StringFieldMapping<ImportJob>() {
            @Override
            public void read(ImportJob object, String value) {
                object.id = value;
            }

            @Override
            public String write(ImportJob object) {
                return object.id;
            }
        }, "BO", FIELD_UUID);
    }

    @Override
    public boolean acceptWorksheet(WorksheetEntry worksheet) {
        return DateUtils.weekFromWorksheetName(worksheet.getTitle().getPlainText()) >= 0;
    }

    @Override
    public FieldMapping<ImportJob> idField() {
        return fieldForName(FIELD_UUID);
    }

    @Override
    public void readWorksheetInfo(ImportJob entity, WorksheetEntry worksheet) {
        entity.week = DateUtils.weekFromWorksheetName(worksheet.getTitle().getPlainText());
    }

    @Override
    protected ImportJob createNewEntity() {
        return new ImportJob();
    }

    public static final String FIELD_STATUS = "status";
    public static final String FIELD_MEGBIZO = "megbizo";
    public static final String FIELD_FELRAKASI_HELY = "felrakasiHely";
    public static final String FIELD_FELVETEL_TERV_DATUMA = "felvetelTervDatuma";
    public static final String FIELD_LERAKASI_HELY = "lerakasiHely";
    public static final String FIELD_CIMZETT = "cimzett";
    public static final String FIELD_LERAKASI_HATARIDO = "lerakasiHatarido";
    public static final String FIELD_EU_PALETTA_HELY = "euPalettaHely";
    public static final String FIELD_COLLI = "colli";
    public static final String FIELD_KG = "kg";
    public static final String FIELD_FELVETELI_REF = "felveteliRef";
    public static final String FIELD_CBM = "cbm";
    public static final String FIELD_ELOFUTAS_JARAT = "elofutasJarat";
    public static final String FIELD_ELOFUTAS_TGK = "elofutasTgk";
    public static final String FIELD_ELOFUTAS_POTKOCSI = "elofutasPotkocsi";
    public static final String FIELD_ELOFUTAS_GKV = "elofutasGkv";
    public static final String FIELD_FELVETEL_TENYL_DATUMA = "felvetelTenyegesDatuma";
    public static final String FIELD_FELVETEL_POZICIO = "felvetelPozicio";
    public static final String FIELD_SERULES_LEIRASA_FEVETELKOR = "serulesLeirasaFelvetelkor";
    public static final String FIELD_BETAR_DATUM = "betarDatum";
    public static final String FIELD_SERULES_LEIRASA_BETAROLASKOR = "serulesLeirasaBetarolaskor";
    public static final String FIELD_MARKER = "marker";
    public static final String FIELD_RAKTARI_MEGJEGYZES = "raktariMegjegyzes";
    public static final String FIELD_KITAR_DATUM = "kitarDatum";
    public static final String FIELD_KISZALL_TGK = "kiszallGkv";
    public static final String FIELD_KISZALL_POTKOCSI = "kiszallPotkocsi";
    public static final String FIELD_KISZALL_JARAT = "kiszallJarat";
    public static final String FIELD_KISZALL_GKV = "kiszallGkv";
    public static final String FIELD_LERAKAS_TENYL_DATUMA = "lerakasTenylDatuma";
    public static final String FIELD_SERULES_LEIRASA_LERAKASKOR = "serulesLeirasaLerakaskor";
    public static final String FIELD_AFA_TIPUSA = "afaTipusa";
    public static final String FIELD_UUID = "id";
    public static final String FIELD_LERAKAS_POZICIO = "lerakasPozicio";
    public static final String FIELD_VAMKEZELES_KESZ = "vamkezelesKesz";
    public static final String FIELD_DOCUMENTS = "documents";
    public static final String FIELD_FELVETT_MENNYISEG_EGYEZIK = "felvettMennyisegEgyezik";
    public static final String FIELD_EU_PALETTA_HELY_FELVETELKOR = "euPalettaHelyFelvetelkor";
    public static final String FIELD_COLLI_FELVETELKOR = "colliFelvetelkor";
    public static final String FIELD_KG_FELVETELKOR = "kgFelvetelkor";
    public static final String FIELD_CBM_FELVETELKOR = "cbmFelvetelkor";
    public static final String FIELD_SERULES_FELVETELKOR = "serulesFelvetelkor";
    public static final String FIELD_SERULES_LERAKASKOR = "serulesLerakaskor";
}
