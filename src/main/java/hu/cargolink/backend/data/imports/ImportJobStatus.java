package hu.cargolink.backend.data.imports;

import hu.cargolink.backend.data.JobStatus;

import java.io.Serializable;

/**
 * Az import munkak lehetseges statuszait tartalmazza
 *
 * Created by Aron on 2014.08.14..
 */
public enum ImportJobStatus implements JobStatus, Serializable {
    // Belf. szakasz
    MEGBIZAS_FELVEVE,
    KIADVA_FELVETELRE_IMP_LKWNAK,
    IMP_LKW_FELVETTE,
    IMP_LKW_FULL,
    RAKTARBA_BEERKEZETT,
    // NK szakasz
    KIADVA_KISZALLITASRA_SAJAT_LKW,
    KIADVA_KISZALLITASRA_ALVALLAKOZO,
    KIADVA_KISZALLITASRA_MEGBIZO,
    LERAKVA_CIMZETTNEL_V_KIADVA_MEGBIZONAK,
    SZAMLAZHATO,
    SZAMLAZVA,
    //
    STORNO;

    private ImportJobStatus() {
    }

    /**
     * @return true-val ter vissza, ha az ilyen allapotban levo munka mar tul van a belfoldi szakaszon
     */
    public boolean isDoneForBelf() {
        return this != KIADVA_KISZALLITASRA_SAJAT_LKW;
    }

    /**
     * @return true-val ter vissza, ha az ilyen allapotban levo munka nincs benne a nemzetkozi szakaszban
     */
    public boolean isDoneForNk() {
        return ordinal() < KIADVA_FELVETELRE_IMP_LKWNAK.ordinal() || ordinal() >= RAKTARBA_BEERKEZETT.ordinal();
    }
}
