package hu.cargolink.backend.data.imports;

/**
 * Sajat muveletek az ImportJobRepository-hoz
 *
 * Created by Aron on 2014.08.21..
 */
public interface ImportJobRepositoryCustom {
    public <E extends ImportJob> E saveWithLastModified(E object);

    public <E extends ImportJob> Iterable<E> saveWithLastModified(Iterable<E> objects);
}
