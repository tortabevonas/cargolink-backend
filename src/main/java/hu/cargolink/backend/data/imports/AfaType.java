package hu.cargolink.backend.data.imports;

/**
 * Az munkak lehetseges AFA tipusait tartalmazza.
 *
 * Created by Aron on 2014.08.18..
 */
public enum AfaType {
    ARUSZALLITAS_EU_TERULETEN,
    IMPORT_FUVAR,
    RAKTAROZAS,
    KIALLAS,
    BELFOLDI_ARUSZALLITAS,
    IMPORT_VAMKEZELES,
}
