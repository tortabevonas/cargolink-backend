package hu.cargolink.backend.data.documents.importexport;

import hu.cargolink.backend.utils.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * Created by aronlorincz on 14. 12. 02..
 *
 * Ez a szolgáltatás a rendszerben tárolt dokumentumok JSON-ból való
 * importálását végzi.
 * Csak speciális esetekben lehet szükséges, ha az adatbázis valami okból üríteni kell.
 */
@Component
public class DocumentImportService extends AsyncService<File, Throwable> {
    @Autowired
    private DocumentImportExportManager documentImportExportManager;

    @Override
    protected Throwable doJob(File file) {
        try {
            int importCount=documentImportExportManager.importDocuments(file);
            System.out.println("Dokumentumok importalva: " + file.getName()+" "+importCount+" darab munka.");
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return e;
        }
    }
}
