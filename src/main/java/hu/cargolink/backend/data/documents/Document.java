package hu.cargolink.backend.data.documents;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Adminisztratív dokumentum, amely különféle típusú lehet.
 *
 * Created by Aron on 2014.08.17..
 */
@Entity
public class Document {
    private DocumentType type = DocumentType.EGYEB;

    @Id
    private String id = UUID.randomUUID().toString();

    @OrderBy("position ASC")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Page> pages = new ArrayList<Page>();

    //public String jobId;

    private long dateOfCreation = System.currentTimeMillis();

    public DocumentType getType() {
        return type;
    }

    public void setType(DocumentType type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public List<Page> getPages() {
        return pages;
    }

    public Page newPage() {
        Page page = new Page();
        pages.add(page);
        return page;
    }

    public long getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(long dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }
}
