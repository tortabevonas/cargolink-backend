package hu.cargolink.backend.data.documents.importexport;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import hu.cargolink.backend.data.Job;
import hu.cargolink.backend.data.documents.Document;
import hu.cargolink.backend.data.documents.DocumentRepository;
import hu.cargolink.backend.data.documents.PageRepository;
import hu.cargolink.backend.data.exports.ExportJob;
import hu.cargolink.backend.data.exports.ExportJobRepository;
import hu.cargolink.backend.data.imports.ImportJob;
import hu.cargolink.backend.data.imports.ImportJobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by aronlorincz on 14. 12. 02..
 *
 * Dokumentumok importálását és exportálását összefogó szolgáltatás
 */
@Component
public class DocumentImportExportManager {
    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private PageRepository pageRepository;

    @Autowired
    private ImportJobRepository importJobRepository;

    @Autowired
    private ExportJobRepository exportJobRepository;


    public void exportDocuments(File file) throws IOException {
        List<PortableJob> portableJobs = new ArrayList<PortableJob>();
        for (ImportJob importJob : importJobRepository.findAll()) {
            if (!importJob.documents.isEmpty()) {
                portableJobs.add(createPortableJob(importJob, importJob.documents));
            }
        }
        for (ExportJob exportJob : exportJobRepository.findAll()) {
            if (!exportJob.documents.isEmpty()) {
                portableJobs.add(createPortableJob(exportJob, exportJob.documents));
            }
        }

        new ObjectMapper().writerWithDefaultPrettyPrinter().writeValue(file, portableJobs);
    }

    private PortableJob createPortableJob(Job job, Set<Document> documents) {
        PortableJob portableJob = new PortableJob();
        portableJob.documents = new ArrayList<Document>();
        portableJob.documents.addAll(documents);
        portableJob.id = job.getId();
        return portableJob;
    }

    public int importDocuments(File file) throws IOException {
        List<PortableJob> portableJobs = new ObjectMapper().readValue(file, new TypeReference<ArrayList<PortableJob>>() {
        });
        int counter = 0;
        System.out.println("Dokumentum importalas: " + portableJobs.size() + " darab portableJob");
        for (PortableJob portableJob : portableJobs) {
            ImportJob importJob = importJobRepository.findOne(portableJob.id);
            ExportJob exportJob = exportJobRepository.findOne(portableJob.id);
            if (importJob != null) {
                importJob.documents.addAll(portableJob.documents);
                importJobRepository.saveWithLastModified(importJob);
                counter++;
            } else if (exportJob != null) {
                exportJob.documents.addAll(portableJob.documents);
                exportJobRepository.saveWithLastModified(exportJob);
                counter++;
            }
        }
        System.out.println("Dokumentum importalas befejezve");
        return counter;
    }
}
