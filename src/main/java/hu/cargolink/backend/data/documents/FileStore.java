package hu.cargolink.backend.data.documents;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * Kulcs alapú fájltároló, amely elrejti a fájlrendszer részleteit.
 *
 * Created by Aron on 2014.08.17..
 */
public class FileStore implements Store {
    public FileStore(File baseDirectory) {
        this.baseDirectory = baseDirectory;
        if (baseDirectory.exists()) {
            if (baseDirectory.isFile()) {
                throw new RuntimeException("Supplied directory is a file!");
            }
        } else {
            if (!baseDirectory.mkdirs()) {
                throw new RuntimeException("Can't create directory for FileStore!");
            }
        }
    }

    private File baseDirectory;

    public File file(String key) {
        return new File(baseDirectory, key);
    }

    @Override
    public void put(String key, WriteCallback writeCallback) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(file(key));
        writeCallback.write(outputStream);
        outputStream.close();
    }

    @Override
    public void get(String key, ReadCallback readCallback) throws IOException {
        FileInputStream inputStream = new FileInputStream(file(key));
        readCallback.read(inputStream);
        inputStream.close();
    }

    @Override
    public void delete(String key) throws Exception {
        if (!file(key).delete()) {
            throw new Exception("Unable to remove page!");
        }
    }
}
