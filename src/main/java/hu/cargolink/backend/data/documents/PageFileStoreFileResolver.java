package hu.cargolink.backend.data.documents;

import hu.cargolink.backend.soforservice.FileResolver;

import java.io.File;

/**
 * Page objektumok fájlokká alakítására szolgáló FileResolver.
 *
 * Created by Aron on 2014.08.25..
 */
public class PageFileStoreFileResolver implements FileResolver<Page> {
    private final FileStore fileStore;

    public PageFileStoreFileResolver(FileStore fileStore) {
        this.fileStore = fileStore;
    }

    @Override
    public File fileFor(Page page) {
        return fileStore.file(page.getId());
    }
}
