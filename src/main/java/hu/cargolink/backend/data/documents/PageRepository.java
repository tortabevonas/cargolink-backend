package hu.cargolink.backend.data.documents;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Spring Data repository dokumentumoldalakhoz
 *
 * Created by Aron on 2014.08.17..
 */
public interface PageRepository extends CrudRepository<Page, String> {
    @Override
    @Query("from Page order by position, dateOfCreation")
    Iterable<Page> findAll();
}
