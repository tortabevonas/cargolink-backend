package hu.cargolink.backend.data.documents.importexport;

/**
 * Created by aronlorincz on 14. 12. 02..
 *
 * Egy dokumentumoldal JSON-ba szerializálható reprezentációja
 */
public class PortablePage {
    public String id;
    public long dateOfCreation;
    public int position;
}
