package hu.cargolink.backend.data.documents;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

/**
 * Egy dokumentum egyetlen oldala, amely egy kamerával készített képből és metaadatokból áll.
 *
 * Created by Aron on 2014.08.17..
 */
@Entity
public class Page {
    @Id
    private String id = UUID.randomUUID().toString();
    private long dateOfCreation = System.currentTimeMillis();
    private int position;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getDateOfCreation() {
        return dateOfCreation;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setDateOfCreation(long dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }
}
