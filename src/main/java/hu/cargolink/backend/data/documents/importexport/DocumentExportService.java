package hu.cargolink.backend.data.documents.importexport;

import hu.cargolink.backend.utils.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * Created by aronlorincz on 14. 12. 02..
 *
 * Ez a szolgáltatás a rendszerben tárolt dokumentumok JSON-ba való
 * kiexportálását végzi.
 * Csak speciális esetekben lehet szükséges, ha az adatbázis valami okból üríteni kell.
 */
@Component
public class DocumentExportService extends AsyncService<File, Throwable> {
    @Autowired
    private DocumentImportExportManager documentImportExportManager;

    @Override
    protected Throwable doJob(File file) {
        try {
            documentImportExportManager.exportDocuments(file);
            System.out.println("Dokumentumok kiexportalva: " + file.getName());
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return e;
        }
    }
}
