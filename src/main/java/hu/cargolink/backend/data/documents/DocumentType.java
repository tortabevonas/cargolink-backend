package hu.cargolink.backend.data.documents;

/**
 *
 * A rendszerben használható dokumentumtípusok.
 *
 * Created by Aron on 2014.08.12..
 */
public enum DocumentType {
    ATVETELI_JEGY,
    VAMOKVANY,
    SERULES_JEGYZOKONYV,
    CMR,
    EGYEB,
    SERULES_FENYKEPEK;

    private DocumentType() {
    }
}
