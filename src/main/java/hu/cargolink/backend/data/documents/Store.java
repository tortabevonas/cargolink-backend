package hu.cargolink.backend.data.documents;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Absztrakció, amely elfedi hogy hogyan tárolunk egy String kulccsal ellátott adatfolyamot.
 *
 * Created by Aron on 2014.08.17..
 */
public interface Store {
    void put(String key, WriteCallback writeCallback) throws IOException;

    void get(String key, ReadCallback readCallback) throws IOException;

    void delete(String key) throws Exception;

    public interface WriteCallback {
        public void write(OutputStream outputStream) throws IOException;
    }

    public interface ReadCallback {
        public void read(InputStream inputStream) throws IOException;
    }
}
