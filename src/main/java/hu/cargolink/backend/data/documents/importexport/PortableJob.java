package hu.cargolink.backend.data.documents.importexport;

import hu.cargolink.backend.data.documents.Document;

import java.util.List;

/**
 * Created by aronlorincz on 14. 12. 02..
 *
 * Egy munka JSON-ba szerializálható reprezentációja
 */
public class PortableJob {
    public String id;
    public List<Document> documents;
}
