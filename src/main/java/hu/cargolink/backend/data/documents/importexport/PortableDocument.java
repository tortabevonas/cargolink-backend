package hu.cargolink.backend.data.documents.importexport;

import hu.cargolink.backend.data.documents.DocumentType;

import java.util.List;

/**
 * Created by aronlorincz on 14. 12. 02..
 *
 * Egy dokumentum JSON-ba szerializálható reprezentációja
 */
public class PortableDocument {
    public List<PortablePage> pages;
    public DocumentType type;
    public long dateOfCreation;
}
