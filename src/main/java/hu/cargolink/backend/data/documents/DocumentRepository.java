package hu.cargolink.backend.data.documents;

import org.springframework.data.repository.CrudRepository;

/**
 * Spring Data repository dokumentumokhoz.
 *
 * Created by Aron on 2014.08.17..
 */
public interface DocumentRepository extends CrudRepository<Document, String> {
}
