package hu.cargolink.backend.data.authentication;

import hu.cargolink.backend.data.sofor.Sofor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

/**
 * Created by Aron on 2014.08.23..
 *
 * Egy adott felhasználó (sofőr) munkamenetét azonosítja,
 * amelyet a mobil appban való bejelentkezéssel kezdeményez.
 */
@Entity
public class AuthenticationToken {
    public AuthenticationToken() {
        value = randomValue();
    }

    public AuthenticationToken(Sofor sofor) {
        this();
        this.soforId = sofor.getId();
    }

    @Id
    private String value;

    private String soforId;

    private static final String randomValue() {
        return UUID.randomUUID().toString();
    }

    public String getValue() {
        return value;
    }

    public String getSoforId() {
        return soforId;
    }
}