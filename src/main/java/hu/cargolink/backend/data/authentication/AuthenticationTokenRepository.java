package hu.cargolink.backend.data.authentication;

import org.springframework.data.repository.CrudRepository;

/**
 * Spring Data repository AuthenticationToken-ek tárolására. (Igen, adatbázisban tároljuk őket.)
 *
 * Created by Aron on 2014.08.23..
 */
public interface AuthenticationTokenRepository extends CrudRepository<AuthenticationToken, String> {
    public AuthenticationToken findByValue(String value);
}
