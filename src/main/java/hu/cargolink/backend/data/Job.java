package hu.cargolink.backend.data;

import java.io.Serializable;

/**
 * A munkak (export es import) kozos tulajdonsagait tartalmazza.
 *
 * Created by Aron on 2014.08.14..
 */
public interface Job extends Serializable {
    public String getId();
}
