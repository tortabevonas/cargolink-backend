package hu.cargolink.backend.data.exports;

/**
 * Sajat muveletek amelyeket az ExportJobRepository-n lehet vegrehajtani
 *
 * Created by Aron on 2014.08.21..
 */
public interface ExportJobRepositoryCustom {
    public <E extends ExportJob> E saveWithLastModified(E object);

    public <E extends ExportJob> Iterable<E> saveWithLastModified(Iterable<E> objects);
}
