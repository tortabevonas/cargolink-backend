package hu.cargolink.backend.data.exports;

import hu.cargolink.backend.spreadsheets.core.EntitySynchronizer;
import hu.cargolink.backend.spreadsheets.core.SyncRange;
import hu.cargolink.backend.web.LinkGenerator;

/**
 * Export munkak Google tablazatokbol valo betolteset menedzseli.
 * Alosztalyai dontik el, hogy mikor es mit toltsenek be, ez az osztaly csak azt definialja, ami az importalando
 * adatok tipusara vonatkozik.
 *
 * Created by Aron on 2014.08.20..
 */
public abstract class ExportJobSynchronizer extends EntitySynchronizer<ExportJob> {
    protected ExportJobSynchronizer(SyncRange maxSyncRange, LinkGenerator linkGenerator) {
        super(maxSyncRange, new ExportJobEntityMapping(linkGenerator));
    }

    @Override
    public String entityUuid(ExportJob entity) {
        return entity.id;
    }

    @Override
    protected void setPosition(ExportJob entity, int position) {
        entity.position = position;
    }

    @Override
    protected int getPosition(ExportJob entity) {
        return entity.position;
    }
}