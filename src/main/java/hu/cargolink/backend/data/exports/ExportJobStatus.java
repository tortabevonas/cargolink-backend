package hu.cargolink.backend.data.exports;

import hu.cargolink.backend.data.JobStatus;

import java.io.Serializable;

/**
 * Export munkak allapotai
 *
 * Created by Aron on 2014.08.11..
 */
public enum ExportJobStatus implements JobStatus, Serializable {
    // Belf. szakasz:
    MEGBIZAS_FELVEVE,
    KIADVA_BESSZALLITASRA_SAJAT_LKW,
    KIADVA_BESZALLITASRA_ALVALLAKOZO,
    KIADVA_BESZALLITASRA_MEGBIZO,
    BESZALLITASRA_FELVEVE,
    RAKTARBA_LEADVA,
    RAKTARBA_BEVETELEZVE,
    // NK szakasz:
    FELRAKVA_POTKOCSIRA_LKWRA,
    EXPORT_NK_SZALLITAS_ALATT,
    LERAKVA_CIMZETTNEL,
    SZAMLAZHATO,
    SZAMLAZVA,
    //
    STORNO;

    private ExportJobStatus() {
    }

    /**
     * Akkor ter vissza true-val, ha az ilyen allapotban levo munka belfoldi szakasza veget ert
     * @return
     */
    public boolean isDoneForBelf() {
        return ordinal() >= RAKTARBA_LEADVA.ordinal();
    }

    /**
     * Akkor ter vissza true-val, ha az ilyen allapotban levo munka kulfoldi szakasza veget ert
     * @return
     */
    public boolean isDoneForNk() {
        return ordinal() >= LERAKVA_CIMZETTNEL.ordinal();
    }
}
