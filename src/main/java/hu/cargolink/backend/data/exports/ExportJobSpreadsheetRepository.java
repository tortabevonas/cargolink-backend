package hu.cargolink.backend.data.exports;

import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import hu.cargolink.backend.spreadsheets.DateUtils;
import hu.cargolink.backend.spreadsheets.core.EntityMapping;
import hu.cargolink.backend.spreadsheets.core.SpreadsheetRepository;
import hu.cargolink.backend.spreadsheets.core.SyncRange;

/**
 * Az export munkakat tartalmazo Google Spreadsheets tablazatot szemelyesiti meg, mint adatbazist amelyet irni/olvasni lehet
 *
 * Created by Aron on 2014.08.21..
 */
public class ExportJobSpreadsheetRepository extends SpreadsheetRepository<ExportJob> {
    public ExportJobSpreadsheetRepository(SpreadsheetService service, SpreadsheetEntry spreadsheet, EntityMapping<ExportJob> entityMapping, SyncRange syncRange) {
        super(service, spreadsheet, entityMapping, syncRange);
    }

    @Override
    protected Object getSpreadsheetIdentifierInfo(ExportJob entity) {
        return entity.week;
    }

    @Override
    protected boolean acceptWorksheet(WorksheetEntry worksheet, Object spreadsheetIdentifierInfo) {
        return DateUtils.weekFromWorksheetName(worksheet.getTitle().getPlainText()) == ((Integer) spreadsheetIdentifierInfo);
    }

    @Override
    protected boolean acceptRow(ExportJob rowEntity, ExportJob entityToSave) {
        return rowEntity.id != null && rowEntity.id.equals(entityToSave.id);
    }
}
