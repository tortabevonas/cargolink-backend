package hu.cargolink.backend.data.exports;

import com.google.gdata.data.spreadsheet.WorksheetEntry;
import hu.cargolink.backend.soforservice.common.LatLng;
import hu.cargolink.backend.spreadsheets.DateUtils;
import hu.cargolink.backend.spreadsheets.core.*;
import hu.cargolink.backend.utils.Utils;
import hu.cargolink.backend.web.LinkGenerator;

import java.math.BigDecimal;

/**
 * ExportJob objektumokat konvertal Google Spreadsheets tablazat soraiva, vagy tablazatsorbol tolt be ExportJob objektumokat.
 * Minden egyes mezohoz definialja, hogyan lehet azt irni- es olvasni.
 *
 * Created by Aron on 2014.08.18..
 */
public class ExportJobEntityMapping extends EntityMapping<ExportJob> {
    public ExportJobEntityMapping(final LinkGenerator linkGenerator) {
        addField(new EnumFieldMapping<ExportJob, ExportJobStatus>() {
            private final String[] STATUS_NAMES = new String[]{
                    "Megbízás felvéve",
                    "Kiadva Beszállításra Saját LKW",
                    "Kiadva Beszállításra Alvállalkozó",
                    "Beszállításra felvéve",
                    "Megbízó szállíttatja be",
                    "Raktárba leadva (GKV)",
                    "Raktárba Bevételezve",
                    "Felrakva Pótkocsira/LKWra",
                    "Lerakva címzettnél",
                    "Számlázható",
                    "Stornó a Megbízás",
                    "Számlázva",
                    "Export NK szállítás alatt",
            };

            private final ExportJobStatus[] STATUS_VALUES = new ExportJobStatus[]{
                    ExportJobStatus.MEGBIZAS_FELVEVE,
                    ExportJobStatus.KIADVA_BESSZALLITASRA_SAJAT_LKW,
                    ExportJobStatus.KIADVA_BESZALLITASRA_ALVALLAKOZO,
                    ExportJobStatus.BESZALLITASRA_FELVEVE,
                    ExportJobStatus.KIADVA_BESZALLITASRA_MEGBIZO,
                    ExportJobStatus.RAKTARBA_LEADVA,
                    ExportJobStatus.RAKTARBA_BEVETELEZVE,
                    ExportJobStatus.FELRAKVA_POTKOCSIRA_LKWRA,
                    ExportJobStatus.LERAKVA_CIMZETTNEL,
                    ExportJobStatus.SZAMLAZHATO,
                    ExportJobStatus.STORNO,
                    ExportJobStatus.SZAMLAZVA,
                    ExportJobStatus.EXPORT_NK_SZALLITAS_ALATT,
            };

            @Override
            public void readEnum(ExportJob object, ExportJobStatus value) {
                object.status = value;
            }

            @Override
            public ExportJobStatus writeEnum(ExportJob object) {
                return object.status;
            }

            @Override
            public String[] names() {
                return STATUS_NAMES;
            }

            @Override
            public ExportJobStatus[] values() {
                return STATUS_VALUES;
            }
        }, "A", FIELD_STATUS);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.megbizo = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.megbizo;
            }
        }, "B", FIELD_MEGBIZO);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.feladoCeg = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.feladoCeg;
            }
        }, "C", FIELD_FELADO_CEG);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.felrakasiHely = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.felrakasiHely;
            }
        }, "D", FIELD_FELRAKASI_HELY);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.felveteliRef = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.felveteliRef;
            }
        }, "E", FIELD_FELVETELI_REF);

        addField(new DefaultDateFieldMapping<ExportJob>() {
            @Override
            public void readDate(ExportJob object, long date) {
                object.felvetelTervDatuma = date;
            }

            @Override
            public long writeDate(ExportJob object) {
                return object.felvetelTervDatuma;
            }
        }, "F", FIELD_FELVETEL_TERV_DATUMA);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.cimzettCeg = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.cimzettCeg;
            }
        }, "G", FIELD_CIMZETT_CEG);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.lerakasiHely = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.lerakasiHely;
            }
        }, "H", FIELD_LERAKASI_HELY);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.leadasiRef = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.leadasiRef;
            }
        }, "I", FIELD_LEADASI_REF);

        addField(new DefaultDateFieldMapping<ExportJob>() {
            @Override
            public void readDate(ExportJob object, long date) {
                object.lerakasTervDatuma = date;
            }

            @Override
            public long writeDate(ExportJob object) {
                return object.lerakasTervDatuma;
            }
        }, "J", FIELD_LERAKAS_TERV_DATUMA);

        addField(new IntegerFieldMapping<ExportJob>() {
            @Override
            public void readNumber(ExportJob object, int value) {
                object.euPalettaHely = value;
            }

            @Override
            public int writeNumber(ExportJob object) {
                return object.euPalettaHely;
            }
        }, "K", FIELD_EU_PAL_HELY);

        addField(new IntegerFieldMapping<ExportJob>() {
            @Override
            public void readNumber(ExportJob object, int value) {
                object.colli = value;
            }

            @Override
            public int writeNumber(ExportJob object) {
                return object.colli;
            }
        }, "L", FIELD_COLLI);

        addField(new DecimalFieldMapping<ExportJob>() {
            @Override
            public void readDecimal(ExportJob object, BigDecimal value) {
                object.kg = value;
            }

            @Override
            public BigDecimal writeDecimal(ExportJob object) {
                return object.kg;
            }
        }, "M", FIELD_KG);

        addField(new DecimalFieldMapping<ExportJob>() {
            @Override
            public void readDecimal(ExportJob object, BigDecimal value) {
                object.cbm = value;
            }

            @Override
            public BigDecimal writeDecimal(ExportJob object) {
                return object.cbm;
            }
        }, "N", FIELD_CBM);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.beszallCegVagyJarat = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.beszallCegVagyJarat;
            }
        }, "O", FIELD_BESZALLITAS_CEG_VAGY_JARAT);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.beszallGkv = Utils.fixNameCase(value);
            }

            @Override
            public String write(ExportJob object) {
                return object.beszallGkv;
            }
        }, "P", FIELD_BESZALL_GKV);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.beszallTgk = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.beszallTgk;
            }
        }, "Q", FIELD_BESZALL_TGK);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.beszallPotkocsi = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.beszallPotkocsi;
            }
        }, "R", FIELD_BESZALL_POTKOCSI);

        addField(new DefaultTimeFieldMapping<ExportJob>() {
            @Override
            public void readTime(ExportJob object, long date) {
                object.felvetelTenylDatuma = date;
            }

            @Override
            public long writeTime(ExportJob object) {
                return object.felvetelTenylDatuma;
            }
        }, "S", FIELD_FELVETEL_TENYL_DATUMA);

        addField(new BooleanFieldMapping<ExportJob>() {
            @Override
            public void readBoolean(ExportJob object, boolean value) {
                // Nem olvassuk be mert segédmező
            }

            @Override
            public boolean writeBoolean(ExportJob object) {
                return Utils.stringValid(object.serulesFelvetelkor);
            }
        }, "T", FIELD_SERULES_FELVETELKOR);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.serulesFelvetelkor = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.serulesFelvetelkor;
            }
        }, "U", FIELD_SERULES_LEIRASA_FELVETELKOR);

        addField(new DefaultDateFieldMapping<ExportJob>() {
            @Override
            public void readDate(ExportJob object, long date) {
                object.betarDatum = date;
            }

            @Override
            public long writeDate(ExportJob object) {
                return object.betarDatum;
            }
        }, "V", FIELD_BETAR_DATUM);

        addField(new IntegerFieldMapping<ExportJob>() {
            @Override
            public void readNumber(ExportJob object, int value) {
                object.betarColli = value;
            }

            @Override
            public int writeNumber(ExportJob object) {
                return object.betarColli;
            }
        }, "W", FIELD_BETAR_COLLI);

        addField(new DecimalFieldMapping<ExportJob>() {
            @Override
            public void readDecimal(ExportJob object, BigDecimal value) {
                object.betarKg = value;
            }

            @Override
            public BigDecimal writeDecimal(ExportJob object) {
                return object.betarKg;
            }
        }, "X", FIELD_BETAR_KG);

        addField(new DecimalFieldMapping<ExportJob>() {
            @Override
            public void readDecimal(ExportJob object, BigDecimal value) {
                object.betarCbm = value;
            }

            @Override
            public BigDecimal writeDecimal(ExportJob object) {
                return object.betarCbm;
            }
        }, "Y", FIELD_BETAR_CBM);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.marker = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.marker;
            }
        }, "Z", FIELD_MARKER);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.serulesBetarolaskor = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.serulesBetarolaskor;
            }
        }, "AB", FIELD_SERULES_LEIRASA_BETAROLASKOR);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.raktariMegjegyzes = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.raktariMegjegyzes;
            }
        }, "AC", FIELD_RAKTARI_MEGJEGYZES);

        addField(new DefaultDateFieldMapping<ExportJob>() {
            @Override
            public void readDate(ExportJob object, long date) {
                object.kitarDatum = date;
            }

            @Override
            public long writeDate(ExportJob object) {
                return object.kitarDatum;
            }
        }, "AD", FIELD_KITAR_DATUM);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.nkTgk = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.nkTgk;
            }
        }, "AE", FIELD_NK_TGK);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.nkPotkocsi = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.nkPotkocsi;
            }
        }, "AF", FIELD_NK_POTKOCSI);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.nkJarat = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.nkJarat;
            }
        }, "AG", FIELD_NK_JARAT);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.nkGkv = Utils.fixNameCase(value);
            }

            @Override
            public String write(ExportJob object) {
                return object.nkGkv;
            }
        }, "AH", FIELD_NK_GKV);

        addField(new DefaultTimeFieldMapping<ExportJob>() {
            @Override
            public void readTime(ExportJob object, long date) {
                object.lerakasTenylDatuma = date;
            }

            @Override
            public long writeTime(ExportJob object) {
                return object.lerakasTenylDatuma;
            }
        }, "AJ", FIELD_LERAKAS_TENYL_DATUMA);

        addField(new BooleanFieldMapping<ExportJob>() {
            @Override
            public void readBoolean(ExportJob object, boolean value) {
                //Nem olvassuk be mert segédmező
            }

            @Override
            public boolean writeBoolean(ExportJob object) {
                return Utils.stringValid(object.serulesLerakaskor);
            }
        }, "AK", FIELD_SERULES_LERAKASKOR);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.serulesLerakaskor = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.serulesLerakaskor;
            }
        }, "AL", FIELD_SERULES_LEIRASA_LERAKASKOR);

        ////////////////////////////////

        addField(new LatLngFieldMapping<ExportJob>() {
            @Override
            public void readLatLng(ExportJob object, LatLng latLng) {
                object.felvetelPozicio = latLng;
            }

            @Override
            public LatLng writeLatLng(ExportJob object) {
                return object.felvetelPozicio;
            }
        }, "BR", FIELD_FELVETEL_POZICIO);

        addField(new BooleanFieldMapping<ExportJob>() {
            @Override
            public void readBoolean(ExportJob object, boolean value) {
                //Nem olvassuk vissza
            }

            @Override
            public boolean writeBoolean(ExportJob object) {
                return true
                        && object.euPalettaHely == object.euPalettaHelyFelvetelkor
                        && object.colli == object.colliFelvetelkor
                        && object.kg.compareTo(object.kgFelvetelkor) == 0
                        && object.cbm.compareTo(object.cbmFelvetelkor) == 0;
            }
        }, "BS", FIELD_FELVETT_MENNYISEG_EGYEZIK);

        addField(new IntegerFieldMapping<ExportJob>() {
            @Override
            public void readNumber(ExportJob object, int value) {
                object.euPalettaHelyFelvetelkor = value;
            }

            @Override
            public int writeNumber(ExportJob object) {
                return object.euPalettaHelyFelvetelkor;
            }
        }, "BT", FIELD_EU_PAL_HELY_FELVETELKOR);

        addField(new IntegerFieldMapping<ExportJob>() {
            @Override
            public void readNumber(ExportJob object, int value) {
                object.colliFelvetelkor = value;
            }

            @Override
            public int writeNumber(ExportJob object) {
                return object.colliFelvetelkor;
            }
        }, "BU", FIELD_COLLI_FELVETELKOR);

        addField(new DecimalFieldMapping<ExportJob>() {
            @Override
            public void readDecimal(ExportJob object, BigDecimal value) {
                object.kgFelvetelkor = value;
            }

            @Override
            public BigDecimal writeDecimal(ExportJob object) {
                return object.kgFelvetelkor;
            }
        }, "BV", FIELD_KG_FELVETELKOR);

        addField(new DecimalFieldMapping<ExportJob>() {
            @Override
            public void readDecimal(ExportJob object, BigDecimal value) {
                object.cbmFelvetelkor = value;
            }

            @Override
            public BigDecimal writeDecimal(ExportJob object) {
                return object.cbmFelvetelkor;
            }
        }, "BW", FIELD_CBM_FELVETELKOR);

        addField(new LatLngFieldMapping<ExportJob>() {
            @Override
            public void readLatLng(ExportJob object, LatLng latLng) {
                object.lerakasPozicio = latLng;
            }

            @Override
            public LatLng writeLatLng(ExportJob object) {
                return object.lerakasPozicio;
            }
        }, "BX", FIELD_LERAKAS_POZICIO);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                // Nem olvassuk vissza a dokumentumokat
            }

            @Override
            public String write(ExportJob object) {
                return linkGenerator.linkToJobDocuments(object.id);
            }
        }, "BY", FIELD_DOCUMENTS);

        addField(new StringFieldMapping<ExportJob>() {
            @Override
            public void read(ExportJob object, String value) {
                object.id = value;
            }

            @Override
            public String write(ExportJob object) {
                return object.id;
            }
        }, "BZ", FIELD_UUID);
    }

    @Override
    public boolean acceptWorksheet(WorksheetEntry worksheet) {
        return DateUtils.weekFromWorksheetName(worksheet.getTitle().getPlainText()) >= 0;
    }

    @Override
    public FieldMapping<ExportJob> idField() {
        return fieldForName(FIELD_UUID);
    }

    @Override
    public void readWorksheetInfo(ExportJob entity, WorksheetEntry worksheet) {
        entity.week = DateUtils.weekFromWorksheetName(worksheet.getTitle().getPlainText());
    }

    @Override
    protected ExportJob createNewEntity() {
        return new ExportJob();
    }

    public static final String FIELD_STATUS = "status";
    public static final String FIELD_MEGBIZO = "megbizo";
    public static final String FIELD_FELADO_CEG = "feladoCeg";
    public static final String FIELD_FELRAKASI_HELY = "felrakasiHely";
    public static final String FIELD_FELVETELI_REF = "felveteliRef";
    public static final String FIELD_FELVETEL_TERV_DATUMA = "felvetelTervDatuma";
    public static final String FIELD_CIMZETT_CEG = "cimzettCeg";
    public static final String FIELD_LERAKASI_HELY = "lerakasiHely";
    public static final String FIELD_LEADASI_REF = "leadasiRef";
    public static final String FIELD_LERAKAS_TERV_DATUMA = "lerakasTervDatuma";
    public static final String FIELD_EU_PAL_HELY = "euPalHely";
    public static final String FIELD_COLLI = "colli";
    public static final String FIELD_KG = "kg";
    public static final String FIELD_CBM = "cbm";
    public static final String FIELD_BESZALLITAS_CEG_VAGY_JARAT = "beszallitasCegVagyJarat";
    public static final String FIELD_BESZALL_GKV = "beszallGkv";
    public static final String FIELD_BESZALL_TGK = "beszallTgk";
    public static final String FIELD_BESZALL_POTKOCSI = "beszallPotkocsi";
    public static final String FIELD_FELVETEL_TENYL_DATUMA = "felvetelTenylDatuma";
    public static final String FIELD_EU_PAL_HELY_FELVETELKOR = "euPalHelyFelvetelkor";
    public static final String FIELD_COLLI_FELVETELKOR = "colliFelvetelkor";
    public static final String FIELD_KG_FELVETELKOR = "kgFelvetelkor";
    public static final String FIELD_CBM_FELVETELKOR = "cbmFelvetelkor";
    public static final String FIELD_FELVETT_MENNYISEG_EGYEZIK = "felvettMennyisegEgyezik";
    public static final String FIELD_SERULES_FELVETELKOR = "serulesFelvetelkor";
    public static final String FIELD_SERULES_LEIRASA_FELVETELKOR = "serulesLeirasaFelvetelkor";
    public static final String FIELD_BETAR_DATUM = "betarDatum";
    public static final String FIELD_BETAR_COLLI = "betarColli";
    public static final String FIELD_BETAR_KG = "betarKg";
    public static final String FIELD_BETAR_CBM = "betarCbm";
    public static final String FIELD_MARKER = "marker";
    public static final String FIELD_SERULES_LEIRASA_BETAROLASKOR = "serulesBetarolaskor";
    public static final String FIELD_RAKTARI_MEGJEGYZES = "raktariMegjegyzes";
    public static final String FIELD_KITAR_DATUM = "kitarDatum";
    public static final String FIELD_NK_TGK = "nkTgk";
    public static final String FIELD_NK_POTKOCSI = "nkPotkocsi";
    public static final String FIELD_NK_JARAT = "nkJarat";
    public static final String FIELD_NK_GKV = "nkGkv";
    public static final String FIELD_LERAKAS_TENYL_DATUMA = "lerakasTenylDatuma";
    public static final String FIELD_SERULES_LERAKASKOR = "serulesLerakaskor";
    public static final String FIELD_SERULES_LEIRASA_LERAKASKOR = "serulesLeirasaLerakaskor";
    public static final String FIELD_UUID = "id";
    public static final String FIELD_FELVETEL_POZICIO = "felvetelPozicio";
    public static final String FIELD_LERAKAS_POZICIO = "lerakasPozicio";
    public static final String FIELD_DOCUMENTS = "documents";
}
