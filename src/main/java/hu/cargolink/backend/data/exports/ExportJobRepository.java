package hu.cargolink.backend.data.exports;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

/**
 * Spring Data repository export munkaknak
 *
 * Created by Aron on 2014.08.17..
 */
public interface ExportJobRepository extends JpaRepository<ExportJob, String>, ExportJobRepositoryCustom {
    //@Query("from ExportJob j where j.lastModified > ?2 and (j.beszallGkv = ?1 or j.nkGkv = ?1)")
    @Query("from ExportJob j where j.lastModified > ?1")
    public Set<ExportJob> findSoforUpdates(long lastModified);

    public Set<ExportJob> findByWeek(int week);
}
