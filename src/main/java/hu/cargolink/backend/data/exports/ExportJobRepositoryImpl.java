package hu.cargolink.backend.data.exports;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * ExportJobRepository-n vegrehajthato sajat muveletek implementacioja
 *
 * Created by Aron on 2014.08.21..
 */
public class ExportJobRepositoryImpl implements ExportJobRepositoryCustom {
    @Autowired
    private ExportJobRepository exportJobRepository;

    @Override
    public <E extends ExportJob> E saveWithLastModified(E object) {
        object.lastModified = System.currentTimeMillis();
        return exportJobRepository.save(object);
    }

    @Override
    public <E extends ExportJob> Iterable<E> saveWithLastModified(Iterable<E> objects) {
        for (ExportJob object : objects) {
            object.lastModified = System.currentTimeMillis();
        }
        return exportJobRepository.save(objects);
    }
}
