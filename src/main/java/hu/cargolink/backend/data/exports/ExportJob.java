package hu.cargolink.backend.data.exports;

import hu.cargolink.backend.data.Job;
import hu.cargolink.backend.data.documents.Document;
import hu.cargolink.backend.soforservice.common.LatLng;
import hu.cargolink.backend.utils.Utils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * A cég egy export megrendelését jelképezi.
 * Az ilyen megrendelések két szakaszból állnak, ebben a sorrendben:
 *  - Belföldi: belföldről összegyűjtik és a raktárukba szállítják az árut
 *  - Nemzetközi: az elraktározott árut külföldre, annak rendeltetési helyére szállítják
 *
 * Created by Aron on 2014.08.11..
 */
@Entity
public class ExportJob implements Job, Serializable {
    @Id
    public String id = UUID.randomUUID().toString();

    public long lastModified;

    public int position;

    public int week;

    public ExportJobStatus status;

    @Column(length = 8000)
    public String megbizo;

    @Column(length = 8000)
    public String feladoCeg;

    @Column(length = 8000)
    public String felrakasiHely;

    @Column(length = 8000)
    public String felveteliRef;
    public long felvetelTervDatuma;

    @Column(length = 8000)
    public String cimzettCeg;

    @Column(length = 8000)
    public String lerakasiHely;

    @Column(length = 8000)
    public String leadasiRef;

    public long lerakasTervDatuma;

    @Column(length = 8000)
    public String beszallCegVagyJarat;

    /**
     * Belfoldi szakasz gepkocsivezetojenek az ID-je
     */
    @Column(length = 8000)
    public String beszallGkv;

    /**
     * Belfoldi szakasz gepjarmu rendszama
     */
    @Column(length = 8000)
    public String beszallTgk;

    /**
     * Belföldi szakasz pótkocsi rendszáma (ha van)
     */
    @Column(length = 8000)
    public String beszallPotkocsi;

    /**
     * Felvétel tényleges időpontja
     */
    public long felvetelTenylDatuma;

    @Column(length = 8000)
    public String serulesFelvetelkor;

    public long betarDatum;

    public int betarColli = 0;

    public BigDecimal betarKg = BigDecimal.ZERO;

    public BigDecimal betarCbm = BigDecimal.ZERO;

    @Column(length = 8000)
    public String marker;

    @Column(length = 8000)
    public String serulesBetarolaskor;

    @Column(length = 8000)
    public String raktariMegjegyzes;

    public long kitarDatum;

    /**
     * Nemzetkozi szakasz tehergepjarmu rendszama
     */
    @Column(length = 8000)
    public String nkTgk;

    @Column(length = 8000)
    public String nkPotkocsi;

    @Column(length = 8000)
    public String nkJarat;

    /**
     * Gepkocsivezeto a nemzetkozi szakaszban
     */
    @Column(length = 8000)
    public String nkGkv;

    public long lerakasTenylDatuma;

    @Column(length = 8000)
    public String serulesLerakaskor;

    public int euPalettaHely;

    public int colli = 0;

    public BigDecimal kg = BigDecimal.ZERO;

    public BigDecimal cbm = BigDecimal.ZERO;

    @OrderBy("dateOfCreation ASC")
    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    public Set<Document> documents = new HashSet<Document>();
    /*public void addDocument(Document document) {
        document.jobId = id;
    }*/

    //Koordináták

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "lat", column = @Column(name = "felvetel_pozicio_lat")),
            @AttributeOverride(name = "lng", column = @Column(name = "felvetel_pozicio_lng")),
    })
    public LatLng felvetelPozicio;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "lat", column = @Column(name = "lerakas_pozicio_lat")),
            @AttributeOverride(name = "lng", column = @Column(name = "lerakas_pozicio_lng")),
    })
    public LatLng lerakasPozicio;

    /* Kell ez?: */
    public int euPalettaHelyFelvetelkor;
    public int colliFelvetelkor;
    public BigDecimal kgFelvetelkor = BigDecimal.ZERO;
    public BigDecimal cbmFelvetelkor = BigDecimal.ZERO;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExportJob)) return false;

        ExportJob exportJob = (ExportJob) o;

        if (betarColli != exportJob.betarColli) {
            //Utils.printNotEquals("betarColli");
            return false;
        }
        if (betarDatum != exportJob.betarDatum) {
            //Utils.printNotEquals("betarDatum");
            return false;
        }
        if (colli != exportJob.colli) {
            //Utils.printNotEquals("colli");
            return false;
        }
        if (colliFelvetelkor != exportJob.colliFelvetelkor) {
            //Utils.printNotEquals("colliFelvetelkor");
            return false;
        }
        if (euPalettaHely != exportJob.euPalettaHely) {
            //Utils.printNotEquals("euPalettaHely");
            return false;
        }
        if (euPalettaHelyFelvetelkor != exportJob.euPalettaHelyFelvetelkor) {
            //Utils.printNotEquals("euPalettaHelyFelvetelkor");
            return false;
        }
        if (felvetelTenylDatuma != exportJob.felvetelTenylDatuma) {
            //Utils.printNotEquals("felvetelTenylDatuma");
            return false;
        }
        if (felvetelTervDatuma != exportJob.felvetelTervDatuma) {
            //Utils.printNotEquals("felvetelTervDatuma");
            return false;
        }
        if (kitarDatum != exportJob.kitarDatum) {
            //Utils.printNotEquals("kitarDatum");
            return false;
        }
        if (lerakasTenylDatuma != exportJob.lerakasTenylDatuma) {
            //Utils.printNotEquals("lerakasTenylDatuma");
            return false;
        }
        if (lerakasTervDatuma != exportJob.lerakasTervDatuma) {
            //Utils.printNotEquals("lerakasTervDatuma");
            return false;
        }
        if (week != exportJob.week) {
            //Utils.printNotEquals("week");
            return false;
        }
        if (beszallCegVagyJarat != null ? !beszallCegVagyJarat.equals(exportJob.beszallCegVagyJarat) : exportJob.beszallCegVagyJarat != null) {
            //Utils.printNotEquals("beszallCegVagyJarat");
            return false;
        }
        if (beszallGkv != null ? !beszallGkv.equals(exportJob.beszallGkv) : exportJob.beszallGkv != null) {
            //Utils.printNotEquals("beszallGkv");
            return false;
        }
        if (beszallPotkocsi != null ? !beszallPotkocsi.equals(exportJob.beszallPotkocsi) : exportJob.beszallPotkocsi != null) {
            //Utils.printNotEquals("beszallPotkocsi");
            return false;
        }
        if (beszallTgk != null ? !beszallTgk.equals(exportJob.beszallTgk) : exportJob.beszallTgk != null) {
            //Utils.printNotEquals("beszallTgk");
            return false;
        }
        if (!Utils.same(betarCbm, exportJob.betarCbm)) {
            //Utils.printNotEquals("betarCbm");
            return false;
        }
        if (!Utils.same(betarKg, exportJob.betarKg)) {
            //Utils.printNotEquals("betarKg");
            return false;
        }
        if (!Utils.same(cbm, exportJob.cbm)) {
            //Utils.printNotEquals("cbm");
            return false;
        }
        if (!Utils.same(cbmFelvetelkor, exportJob.cbmFelvetelkor)) {
            //Utils.printNotEquals("cbmFelvetelkor");
            return false;
        }
        if (cimzettCeg != null ? !cimzettCeg.equals(exportJob.cimzettCeg) : exportJob.cimzettCeg != null) {
            //Utils.printNotEquals("cimzettCeg");
            return false;
        }
        if (feladoCeg != null ? !feladoCeg.equals(exportJob.feladoCeg) : exportJob.feladoCeg != null) {
            //Utils.printNotEquals("feladoCeg");
            return false;
        }
        if (felrakasiHely != null ? !felrakasiHely.equals(exportJob.felrakasiHely) : exportJob.felrakasiHely != null) {
            //Utils.printNotEquals("felrakasiHely");
            return false;
        }
        if (felvetelPozicio != null ? !felvetelPozicio.equals(exportJob.felvetelPozicio) : exportJob.felvetelPozicio != null) {
            //Utils.printNotEquals("felvetelPozicio");
            return false;
        }
        if (felveteliRef != null ? !felveteliRef.equals(exportJob.felveteliRef) : exportJob.felveteliRef != null) {
            //Utils.printNotEquals("felveteliRef");
            return false;
        }
        if (id != null ? !id.equals(exportJob.id) : exportJob.id != null) {
            //Utils.printNotEquals("id");
            return false;
        }
        if (!Utils.same(kg, exportJob.kg)) {
            //Utils.printNotEquals("kg");
            return false;
        }
        if (!Utils.same(kgFelvetelkor, exportJob.kgFelvetelkor)) {
            //Utils.printNotEquals("kgFelvetelkor");
            return false;
        }
        if (leadasiRef != null ? !leadasiRef.equals(exportJob.leadasiRef) : exportJob.leadasiRef != null) {
            //Utils.printNotEquals("leadasiRef");
            return false;
        }
        if (lerakasPozicio != null ? !lerakasPozicio.equals(exportJob.lerakasPozicio) : exportJob.lerakasPozicio != null) {
            //Utils.printNotEquals("lerakasPozicio");
            return false;
        }
        if (lerakasiHely != null ? !lerakasiHely.equals(exportJob.lerakasiHely) : exportJob.lerakasiHely != null) {
            //Utils.printNotEquals("lerakasiHely");
            return false;
        }
        if (marker != null ? !marker.equals(exportJob.marker) : exportJob.marker != null) {
            //Utils.printNotEquals("marker");
            return false;
        }
        if (megbizo != null ? !megbizo.equals(exportJob.megbizo) : exportJob.megbizo != null) {
            //Utils.printNotEquals("megbizo");
            return false;
        }
        if (nkGkv != null ? !nkGkv.equals(exportJob.nkGkv) : exportJob.nkGkv != null) {
            //Utils.printNotEquals("nkGkv");
            return false;
        }
        if (nkJarat != null ? !nkJarat.equals(exportJob.nkJarat) : exportJob.nkJarat != null) {
            //Utils.printNotEquals("nkJarat");
            return false;
        }
        if (nkPotkocsi != null ? !nkPotkocsi.equals(exportJob.nkPotkocsi) : exportJob.nkPotkocsi != null) {
            //Utils.printNotEquals("nkPotkocsi");
            return false;
        }
        if (nkTgk != null ? !nkTgk.equals(exportJob.nkTgk) : exportJob.nkTgk != null) {
            //Utils.printNotEquals("nkTgk");
            return false;
        }
        if (raktariMegjegyzes != null ? !raktariMegjegyzes.equals(exportJob.raktariMegjegyzes) : exportJob.raktariMegjegyzes != null) {
            //Utils.printNotEquals("raktariMegjegyzes");
            return false;
        }
        if (serulesBetarolaskor != null ? !serulesBetarolaskor.equals(exportJob.serulesBetarolaskor) : exportJob.serulesBetarolaskor != null) {
            //Utils.printNotEquals("serulesBetarolaskor");
            return false;
        }
        if (serulesFelvetelkor != null ? !serulesFelvetelkor.equals(exportJob.serulesFelvetelkor) : exportJob.serulesFelvetelkor != null) {
            //Utils.printNotEquals("serulesFelvetelkor");
            return false;
        }
        if (serulesLerakaskor != null ? !serulesLerakaskor.equals(exportJob.serulesLerakaskor) : exportJob.serulesLerakaskor != null) {
            //Utils.printNotEquals("serulesLerakaskor");
            return false;
        }
        if (status != exportJob.status) {
            //Utils.printNotEquals("status");
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + week;
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (megbizo != null ? megbizo.hashCode() : 0);
        result = 31 * result + (feladoCeg != null ? feladoCeg.hashCode() : 0);
        result = 31 * result + (felrakasiHely != null ? felrakasiHely.hashCode() : 0);
        result = 31 * result + (felveteliRef != null ? felveteliRef.hashCode() : 0);
        result = 31 * result + (int) (felvetelTervDatuma ^ (felvetelTervDatuma >>> 32));
        result = 31 * result + (cimzettCeg != null ? cimzettCeg.hashCode() : 0);
        result = 31 * result + (lerakasiHely != null ? lerakasiHely.hashCode() : 0);
        result = 31 * result + (leadasiRef != null ? leadasiRef.hashCode() : 0);
        result = 31 * result + (int) (lerakasTervDatuma ^ (lerakasTervDatuma >>> 32));
        result = 31 * result + (beszallCegVagyJarat != null ? beszallCegVagyJarat.hashCode() : 0);
        result = 31 * result + (beszallGkv != null ? beszallGkv.hashCode() : 0);
        result = 31 * result + (beszallTgk != null ? beszallTgk.hashCode() : 0);
        result = 31 * result + (beszallPotkocsi != null ? beszallPotkocsi.hashCode() : 0);
        result = 31 * result + (int) (felvetelTenylDatuma ^ (felvetelTenylDatuma >>> 32));
        result = 31 * result + (serulesFelvetelkor != null ? serulesFelvetelkor.hashCode() : 0);
        result = 31 * result + (int) (betarDatum ^ (betarDatum >>> 32));
        result = 31 * result + betarColli;
        result = 31 * result + (betarKg != null ? betarKg.hashCode() : 0);
        result = 31 * result + (betarCbm != null ? betarCbm.hashCode() : 0);
        result = 31 * result + (marker != null ? marker.hashCode() : 0);
        result = 31 * result + (serulesBetarolaskor != null ? serulesBetarolaskor.hashCode() : 0);
        result = 31 * result + (raktariMegjegyzes != null ? raktariMegjegyzes.hashCode() : 0);
        result = 31 * result + (int) (kitarDatum ^ (kitarDatum >>> 32));
        result = 31 * result + (nkTgk != null ? nkTgk.hashCode() : 0);
        result = 31 * result + (nkPotkocsi != null ? nkPotkocsi.hashCode() : 0);
        result = 31 * result + (nkJarat != null ? nkJarat.hashCode() : 0);
        result = 31 * result + (nkGkv != null ? nkGkv.hashCode() : 0);
        result = 31 * result + (int) (lerakasTenylDatuma ^ (lerakasTenylDatuma >>> 32));
        result = 31 * result + (serulesLerakaskor != null ? serulesLerakaskor.hashCode() : 0);
        result = 31 * result + euPalettaHely;
        result = 31 * result + colli;
        result = 31 * result + (kg != null ? kg.hashCode() : 0);
        result = 31 * result + (cbm != null ? cbm.hashCode() : 0);
        result = 31 * result + (felvetelPozicio != null ? felvetelPozicio.hashCode() : 0);
        result = 31 * result + (lerakasPozicio != null ? lerakasPozicio.hashCode() : 0);
        result = 31 * result + euPalettaHelyFelvetelkor;
        result = 31 * result + colliFelvetelkor;
        result = 31 * result + (kgFelvetelkor != null ? kgFelvetelkor.hashCode() : 0);
        result = 31 * result + (cbmFelvetelkor != null ? cbmFelvetelkor.hashCode() : 0);
        return result;
    }
}