package hu.cargolink.backend.data.sofor;

import hu.cargolink.backend.data.exports.ExportJob;
import hu.cargolink.backend.data.imports.ImportJob;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

/**
 * Egy sofort jelkepez, ugy mint a mobil app felhasznalojat is.
 *
 * Created by Aron on 2014.08.17..
 */
@Entity
public class Sofor {
    @Id
    public String id = UUID.randomUUID().toString();

    public String name;

    public String password;

    public String getId() {
        return id;
    }

    public boolean responsibleForBelf(ExportJob job) {
        return name.trim().equalsIgnoreCase(job.beszallGkv.trim());
    }

    public boolean responsibleForNk(ExportJob job) {
        return name.trim().equalsIgnoreCase(job.nkGkv.trim());
    }

    public boolean responsibleForBelf(ImportJob job) {
        return name.trim().equalsIgnoreCase(job.kiszallGkv.trim());
    }

    public boolean responsibleForNk(ImportJob job) {
        return name.trim().equalsIgnoreCase(job.elofutasGkv.trim());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sofor)) return false;

        Sofor sofor = (Sofor) o;

        if (id != null ? !id.equals(sofor.id) : sofor.id != null) return false;
        if (name != null ? !name.equals(sofor.name) : sofor.name != null) return false;
        if (password != null ? !password.equals(sofor.password) : sofor.password != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
