package hu.cargolink.backend.data.sofor;

import org.springframework.data.repository.CrudRepository;

/**
 * Spring Data repository sofor objektumoknak
 *
 * Created by Aron on 2014.08.17..
 */
public interface SoforRepository extends CrudRepository<Sofor, String> {
    public Sofor findByName(String name);
}
