package hu.cargolink.backend.data.sofor;

import com.google.gdata.data.spreadsheet.WorksheetEntry;
import hu.cargolink.backend.spreadsheets.core.EntityMapping;
import hu.cargolink.backend.spreadsheets.core.FieldMapping;
import hu.cargolink.backend.spreadsheets.core.StringFieldMapping;
import hu.cargolink.backend.utils.Utils;

/**
 * Definialja, hogyan lehet egy google tablazatsort Sofor objektumma vagy egy Sofor objektumot tablazatsorra alakitani.
 * Created by Aron on 2014.08.20..
 */
public class SoforEntityMapping extends EntityMapping<Sofor> {
    public SoforEntityMapping() {
        addField(new StringFieldMapping<Sofor>() {
            @Override
            public void read(Sofor object, String value) {
                object.name = Utils.fixNameCase(value);
            }

            @Override
            public String write(Sofor object) {
                return object.name;
            }
        }, "C", FIELD_NAME);

        addField(new StringFieldMapping<Sofor>() {
            @Override
            public void read(Sofor object, String value) {
                object.password = value;
            }

            @Override
            public String write(Sofor object) {
                return object.password;
            }
        }, "D", FIELD_PASSWORD);

        addField(new StringFieldMapping<Sofor>() {
            @Override
            public void read(Sofor object, String value) {
                object.id = value;
            }

            @Override
            public String write(Sofor object) {
                return object.id;
            }
        }, "E", FIELD_UUID);
    }

    @Override
    protected boolean acceptWorksheet(WorksheetEntry worksheet) {
        return worksheet.getTitle().getPlainText().equalsIgnoreCase("Kódok");
    }

    @Override
    public FieldMapping<Sofor> idField() {
        return fieldForName(FIELD_UUID);
    }

    @Override
    protected Sofor createNewEntity() {
        return new Sofor();
    }

    public static final String FIELD_NAME = "name";
    public static final String FIELD_PASSWORD = "password";
    public static final String FIELD_UUID = "id";
}
