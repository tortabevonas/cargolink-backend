package hu.cargolink.backend.data.sofor;

import hu.cargolink.backend.spreadsheets.core.EntitySynchronizer;
import hu.cargolink.backend.spreadsheets.core.SyncRange;

/**
 * Definialja, hogyan lehet beolvasni egy sofort a tablazatbol.
 * Az alosztalyok dontik el, mikor es milyen adatokat olvasnak.
 *
 * Created by Aron on 2014.08.20..
 */
public abstract class SoforSynchronizer extends EntitySynchronizer<Sofor> {
    protected SoforSynchronizer(SyncRange maxSyncRange) {
        super(maxSyncRange, new SoforEntityMapping());
    }


    @Override
    protected String entityUuid(Sofor entity) {
        return entity.getId();
    }
}
