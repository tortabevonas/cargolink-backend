package hu.cargolink.backend.data;

import hu.cargolink.backend.data.exports.ExportJob;
import hu.cargolink.backend.data.exports.ExportJobRepository;
import hu.cargolink.backend.data.imports.ImportJob;
import hu.cargolink.backend.data.imports.ImportJobRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * ID alapjan megkeres egy munkat, legyen az import vagy export.
 *
 * Created by Aron on 14. 10. 24..
 */
public class JobRepository {
    @Autowired
    private ImportJobRepository importJobRepository;

    @Autowired
    private ExportJobRepository exportJobRepository;

    public Job getJob(String uuid) {
        ImportJob importJob = importJobRepository.findOne(uuid);
        ExportJob exportJob = exportJobRepository.findOne(uuid);
        if (importJob != null) {
            return importJob;
        } else {
            return exportJob;
        }
    }
}
