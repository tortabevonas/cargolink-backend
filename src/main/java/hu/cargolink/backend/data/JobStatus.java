package hu.cargolink.backend.data;

/**
 * Az import- es export munkak allapotainak kozos jellemzoi
 *
 * Created by Aron on 2014.08.11..
 */
public interface JobStatus {
    public boolean isDoneForBelf();

    public boolean isDoneForNk();
}
