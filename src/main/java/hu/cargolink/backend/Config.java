package hu.cargolink.backend;

import hu.cargolink.backend.data.JobRepository;
import hu.cargolink.backend.data.documents.FileStore;
import hu.cargolink.backend.data.exports.ExportJobEntityMapping;
import hu.cargolink.backend.data.exports.ExportJobRepository;
import hu.cargolink.backend.data.exports.ExportJobSpreadsheetRepository;
import hu.cargolink.backend.data.imports.ImportJobEntityMapping;
import hu.cargolink.backend.data.imports.ImportJobRepository;
import hu.cargolink.backend.data.imports.ImportJobSpreadsheetRepository;
import hu.cargolink.backend.data.sofor.SoforRepository;
import hu.cargolink.backend.spreadsheets.PeriodicSpreadsheetImportService;
import hu.cargolink.backend.spreadsheets.SpreadsheetImportService;
import hu.cargolink.backend.spreadsheets.SpreadsheetIntegrationService;
import hu.cargolink.backend.spreadsheets.sync.ExportSheetSyncRequestService;
import hu.cargolink.backend.spreadsheets.sync.ImportSheetSyncRequestService;
import hu.cargolink.backend.spreadsheets.sync.SoforSheetSyncRequestService;
import hu.cargolink.backend.utils.Utils;
import hu.cargolink.backend.web.DocumentController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;

/**
 * Belső konfiguráció, producerek
 *
 * Created by Aron on 2014.08.17..
 */
@Configuration
@EnableJpaRepositories
@EnableTransactionManagement
@EnableAutoConfiguration
public class Config {

    @Autowired
    private DocumentController documentController;

    @Bean
    public DataSource dataSource(
            @Value("${mdb.url}") String url,
            @Value("${mdb.username}") String username,
            @Value("${mdb.password}") String password
    ) {
        DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUsername(username);
        ds.setPassword(password);
        ds.setUrl(url);
        return ds;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setGenerateDdl(true);
        hibernateJpaVendorAdapter.setDatabase(Database.MYSQL);
        return hibernateJpaVendorAdapter;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        return new JpaTransactionManager();
    }

    @Bean
    public FileStore pageFileStore(@Value("${filestore.path}") String path) {
        return new FileStore(new File(path));
    }

    @Bean
    public HandlerAdapter servletHandlerAdapter() {
        AnnotationMethodHandlerAdapter adapter = new AnnotationMethodHandlerAdapter();
        return adapter;
    }

    @Bean
    public MessageSource messageSource(SpringTemplateEngine templateEngine) {
        ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setBasename("templates.messages");
        source.setDefaultEncoding("UTF8");
        source.setFallbackToSystemLocale(true);
        templateEngine.setMessageSource(source);
        return source;
    }

    @Bean
    public SpreadsheetIntegrationService spreadsheetIntegrationService() throws IOException {
        SpreadsheetIntegrationService service = new SpreadsheetIntegrationService();
        return service;
    }


    @Bean
    @Qualifier("exportSheetSyncRequestService")
    public ExportSheetSyncRequestService exportSheetSyncRequestService(SpreadsheetImportService s, SpreadsheetIntegrationService sis, ExportJobRepository exportJobRepository) {
        ExportSheetSyncRequestService service = new ExportSheetSyncRequestService(
                sis.getSpreadsheetService(),
                sis.getExportSheetIntegration().getSpreadsheet(),
                exportJobRepository,
                sis.getExportSheetIntegration().getSyncRange(),
                documentController
        );
        s.exportSheetSyncRequestService = service;
        return service;
    }

    @Bean
    @Qualifier("importSheetSyncRequestService")
    public ImportSheetSyncRequestService importSheetSyncRequestService(SpreadsheetImportService s, SpreadsheetIntegrationService sis, ImportJobRepository importJobRepository) {
        ImportSheetSyncRequestService service = new ImportSheetSyncRequestService(
                sis.getSpreadsheetService(),
                sis.getImportSheetIntegration().getSpreadsheet(),
                importJobRepository,
                sis.getImportSheetIntegration().getSyncRange(),
                documentController
        );
        s.importSheetSyncRequestService = service;
        return service;
    }

    @Bean
    @Qualifier("soforSheetSyncRequestService")
    public SoforSheetSyncRequestService soforSheetSyncRequestService(SpreadsheetImportService s, SpreadsheetIntegrationService sis, SoforRepository soforRepository) {
        SoforSheetSyncRequestService service = new SoforSheetSyncRequestService(
                sis.getSpreadsheetService(),
                sis.getSoforSheetIntegration().getSpreadsheet(),
                soforRepository,
                sis.getSoforSheetIntegration().getSyncRange()
        );
        s.soforSheetSyncRequestService = service;
        return service;
    }

    @Bean
    public SpreadsheetImportService spreadsheetImportService() {
        return new SpreadsheetImportService();
    }

    @Bean
    public PeriodicSpreadsheetImportService periodicSpreadsheetImportService(SpreadsheetImportService spreadsheetImportService) {
        return new PeriodicSpreadsheetImportService(
                spreadsheetImportService,
                Utils.minutesToMillis(4f),
                Utils.hoursToMillis(2f)
        );
    }

    @Bean
    public ExportJobSpreadsheetRepository exportJobSpreadsheetRepository(SpreadsheetIntegrationService integrationService) {
        return new ExportJobSpreadsheetRepository(
                integrationService.getSpreadsheetService(),
                integrationService.getExportSheetIntegration().getSpreadsheet(),
                new ExportJobEntityMapping(documentController),
                integrationService.getExportSheetIntegration().getSyncRange()
        );
    }

    @Bean
    public ImportJobSpreadsheetRepository importJobSpreadsheetRepository(SpreadsheetIntegrationService integrationService) {
        return new ImportJobSpreadsheetRepository(
                integrationService.getSpreadsheetService(),
                integrationService.getImportSheetIntegration().getSpreadsheet(),
                new ImportJobEntityMapping(documentController),
                integrationService.getImportSheetIntegration().getSyncRange()
        );
    }

    @Bean
    public StartupListener startupListener() {
        return new StartupListener();
    }

    @Bean
    public JobRepository jobRepository() {
        return new JobRepository();
    }
}