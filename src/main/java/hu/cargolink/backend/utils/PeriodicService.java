package hu.cargolink.backend.utils;

/**
 * Olyan szolgáltatások szülőosztálya, amelyek egy, az alosztályok által definiált feladatot meghatározott időközönként végrehajtanak.
 *
 * Created by aronlorincz on 14. 12. 02..
 */
public abstract class PeriodicService {
    private long period;
    private SchedulerThread thread;
    private boolean doingJob = false;
    private boolean doJobNow = false;

    public PeriodicService(long period) {
        this.period = period;
    }

    public void start() {
        if (thread == null) {
            thread = new SchedulerThread();
            thread.start();
        }
    }

    public void stop() {
        if (thread != null) {
            thread.interrupt();
            thread = null;
        }
    }

    public void setPeriod(long period) {
        this.period = period;
    }

    protected abstract void doJob();

    private class SchedulerThread extends Thread {
        public SchedulerThread() {
            super("PeriodicService");
        }

        @Override
        public void run() {
            while (true) {
                try {
                    doingJob = true;
                    doJob();
                    doingJob = false;
                    sleep(period);
                } catch (InterruptedException e) {
                    if (doJobNow) {
                        doJobNow = false;
                        continue;
                    } else {
                        return;
                    }
                } finally {
                    doingJob = false;
                }
            }
        }
    }

    public boolean isRunning() {
        return thread != null;
    }

    public void skipWait() {
        if (isRunning() && !doingJob) {
            doJobNow = true;
            thread.interrupt();
        }
    }
}
