package hu.cargolink.backend.utils;

/**
 * Egyszerű háttérfolyamat, amelynek lekérdezhető az állapota és legutóbbi végrehajtásának az eredménye.
 *
 * Created by Aron on 2014.08.19..
 */
public abstract class AsyncService<Input, Output> {
    private Thread thread;
    private Output lastResult;

    public void start(final Input input) {
        if (isRunning()) {
            return;
        }
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                lastResult = doJob(input);
                thread = null;
            }
        });
        thread.start();
    }

    public final boolean isRunning() {
        return thread != null;
    }

    protected abstract Output doJob(Input input);

    public void await() {
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Output getLastResult() {
        return lastResult;
    }
}

