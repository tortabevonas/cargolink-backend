package hu.cargolink.backend.utils;

/**
 * Hibakeresésre és teljesítménymérésre használt időmérő
 *
 * Created by Aron on 2014.08.21..
 */
public class Stopwatch {
    private final long startTime;

    public Stopwatch() {
        startTime = System.currentTimeMillis();
    }

    public long getElapsedTime() {
        return System.currentTimeMillis() - startTime;
    }

    public float getElapsedSeconds() {
        return getElapsedTime() / 1000f;
    }
}
