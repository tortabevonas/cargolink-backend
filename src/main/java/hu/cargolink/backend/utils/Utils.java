package hu.cargolink.backend.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Különféle segéd függvények
 *
 * Created by Aron on 2014.08.18..
 */
public class Utils {
    public static boolean stringValid(String string) {
        if (string == null) {
            return false;
        }
        return string.trim().length() > 0;
    }

    public static <T> List<T> toList(Iterable<T> iterable) {
        List<T> result = new ArrayList<T>();
        for (T t : iterable) {
            result.add(t);
        }
        return result;
    }

    public static boolean equal(Object a, Object b) {
        if (a != null && b != null) {
            return a.equals(b);
        }
        return false;
    }

    public static <T> Set<T> toSet(Iterable<T> all) {
        Set<T> set = new HashSet<T>();
        for (T t : all) {
            set.add(t);
        }
        return set;
    }

    public static String fixNameCase(String name) {
        if (name != null) {
            String[] nameParts = name.split("\\s+");
            name = "";
            for (String namePart : nameParts) {
                name += Character.toUpperCase(namePart.charAt(0)) + namePart.substring(1).toLowerCase();
                name += " ";
            }
            return name.trim();
        } else {
            return null;
        }
    }

    public static long secondsToMillis(float seconds) {
        return (long) (seconds * 1000f);
    }

    public static long minutesToMillis(float minutes) {
        return (long) (minutes * 60f * 1000f);
    }

    public static long hoursToMillis(float hours) {
        return (long) (hours * 60f * 60f * 1000f);
    }

    public static boolean same(BigDecimal a, BigDecimal b) {
        if (a == null) {
            return b == null || b.compareTo(BigDecimal.ZERO) == 0;
        } else if (b == null) {
            return a == null || a.compareTo(BigDecimal.ZERO) == 0;
        }
        boolean same = a.setScale(2, RoundingMode.HALF_UP).compareTo(b.setScale(2, RoundingMode.HALF_UP)) == 0;
        return same;
    }

    public static void printNotEquals(String fieldName) {
        System.out.println("Not equals:" + fieldName);
    }
}
